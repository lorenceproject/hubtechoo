<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 20/07/2020
 * Time: 15:22
 */

namespace App\Http\Middleware;

use Closure;
use App;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LanguageSwitcher
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->header('Cache-Control', 'no-cache, must-revalidate, max-age=31536000');

        if (!Session::has('locale'))
        {
            Session::put('locale',Config::get('app.locale'));
        }
        App::setLocale(session('locale'));
        return $next($request);
    }
}
