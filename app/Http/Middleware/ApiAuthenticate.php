<?php
/**
 * Created by PhpStorm.
 * User: vuongluis
 * Date: 9/18/2020
 * Time: 11:55 AM
 */

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (JWTAuth::getToken() && JWTAuth::parseToken()->authenticate()) {
            $user = JWTAuth::parseToken()->authenticate();
            
        } else {
            return response()->json(['error' => 'user_not_found'], 404);
        }
        return $next($request);
    }
}