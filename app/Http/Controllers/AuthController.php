<?php
namespace App\Http\Controllers;

use App\Jobs\SendEmailVerified;
use App\Model\User;
use App\Repositories\User\UserRepository;
use App\Repositories\Verification\VerificationRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{

    use HasTimestamps;

    protected $userRepository;
    protected $verificationRepository;

    public function __construct(UserRepository $userRepository, VerificationRepository $verificationRepository) {
        $this->userRepository = $userRepository;
        $this->verificationRepository = $verificationRepository;
    }

    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials, ['exp' => Carbon::now()->addDays(1)->timestamp])) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        if (Auth::guard('web')->attempt([
            'email' => $request->email,
            'password' => $request->password])) {
            // check user if email_verify_at
            $user = $this->userRepository->findByField('email', $request->email);
            $permissions = json_encode(Auth::guard('web')->user()->allPermissions, true);
            $roles = json_encode(Auth::guard('web')->user()->allRoles, true);
            if (!is_null($user) && sizeof($user) > 0 && is_null($user[0]['email_verified_at'])) {
                $verify = is_null($user[0]['email_verified_at']);
                return response()->json(compact('token', 'verify', 'permissions', 'roles'));
            }
            if (!is_null($user) && sizeof($user) > 0) {
                $status = $user[0]['status'];
                return response()->json(compact('token', 'status', 'permissions', 'roles'));
            }
            if (!is_null($user) && sizeof($user) > 0 && strcmp('external', $user[0]['type_of_user']) == 0) {
                $type_of_user = $user[0]['type_of_user'];
                return response()->json(compact('token', 'type_of_user', 'permissions', 'roles'));
            }
        }
    }

    public function fauthenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials, ['exp' => Carbon::now()->addDays(1)->timestamp])) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        if (Auth::guard('web')->attempt([
            'email' => $request->email,
            'password' => $request->password])) {
            // check user if email_verify_at
            $user = $this->userRepository->findByField('email', $request->email);
            if (!is_null($user) && sizeof($user) > 0 && is_null($user[0]['email_verified_at'])) {
                $verify = is_null($user[0]['email_verified_at']);
                return response()->json(compact('token', 'verify'));
            }
            if (!is_null($user) && sizeof($user) > 0) {
                $status = $user[0]['status'];
                $user = $user[0];
                return response()->json(compact('token','status', 'user'));
            }
        }
    }

    public function register(Request $request) {
        $credentials = $request->only('email', 'password', 'confirm_password');
        $rules = [
            'email' => 'regex:/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/|unique:users',
            'password' => 'required',
            'confirm_password' => 'required',
        ];
        if (strcmp('vi', $request->header('locale')) == 0) {
            $customMessages = [
                'email.regex' => 'Địa chỉ hộp thư không đúng định dạng.',
                'email.unique' => 'Địa chỉ hộp thư đã tồn tại trong hệ thống.',
                'password.required' => 'Trường mật khẩu không được để trống.',
                'confirm_password.required' => 'Trường xác nhận mật khẩu không được để trống.'
            ];
        } else {
            $customMessages = [
                'email.regex' => 'The mail address is not properly formatted.',
                'email.unique' => 'This email address is already being used.',
                'password.required' => 'The password cannot be left blank.',
                'confirm_password.required' => 'The confirm password cannot be left blank.'
            ];
        }
        $validator = Validator::make($credentials, $rules, $customMessages);
        if ($validator->fails()) {
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => $validator->errors()->first()
                ],
            ]));
        } else {
            $request->merge([
                'password' => Hash::make($request->password),
                'type_of_user' => 'internal',
                'status' => 0,
                'is_editable' => 0,
                'lang' => $request->header('locale'),
                'created_at' => $this->freshTimestamp(),
                'updated_at' => $this->freshTimestamp()
            ]);
            $this->userRepository->create($request->all());

            $user = $this->userRepository->findByField('email', $request->email);
            if (!is_null($user) && sizeof($user) > 0) {
                $name = strcmp('vi', $request->header('locale')) == 0 ? $user[0]['name_vi'] : $user[0]['name_en'];
                $data = [
                    'id' => $user[0]['id'],
                    'email' => $user[0]['email'],
                    'name' => $name,
                    'subject' => 'ACCOUNT VERIFICATION'
                ];
                SendEmailVerified::dispatch($data);
            }
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'email' => $request->email
                ],
                'user' => $user[0]
            ]));

        }
    }

    public function fregister(Request $request) {
        $credentials = $request->only('email', 'password', 'confirm_password');
        $rules = [
            'email' => 'regex:/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/|unique:users',
            'password' => 'required',
            'confirm_password' => 'required',
        ];
        if (strcmp('vi', $request->header('locale')) == 0) {
            $customMessages = [
                'email.regex' => 'Địa chỉ hộp thư không đúng định dạng.',
                'email.unique' => 'Địa chỉ hộp thư đã tồn tại trong hệ thống.',
                'password.required' => 'Trường mật khẩu không được để trống.',
                'confirm_password.required' => 'Trường xác nhận mật khẩu không được để trống.'
            ];
        } else {
            $customMessages = [
                'email.regex' => 'The mail address is not properly formatted.',
                'email.unique' => 'This email address is already being used.',
                'password.required' => 'The password cannot be left blank.',
                'confirm_password.required' => 'The confirm password cannot be left blank.'
            ];
        }
        $validator = Validator::make($credentials, $rules, $customMessages);
        if ($validator->fails()) {
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => $validator->errors()->first()
                ],
            ]));
        } else {
            $request->merge([
                'password' => Hash::make($request->password),
                'name_vi' => explode("@", $request->email)[0],
                'name_en' => explode("@", $request->email)[0],
                'avatar' => url('/').'/images/user.svg',
                'type_of_user' => 'external',
                'status' => 0,
                'is_editable' => 0,
                'lang' => $request->header('locale'),
                'created_at' => $this->freshTimestamp(),
                'updated_at' => $this->freshTimestamp()
            ]);
            $this->userRepository->create($request->all());

            $user = $this->userRepository->findByField('email', $request->email);
            if (!is_null($user) && sizeof($user) > 0) {
                $name = strcmp('vi', $request->header('locale')) == 0 ? $user[0]['name_vi'] : $user[0]['name_en'];
                $data = [
                    'id' => $user[0]['id'],
                    'email' => $user[0]['email'],
                    'name' => $name,
                    'subject' => 'ACCOUNT VERIFICATION'
                ];
                SendEmailVerified::dispatch($data);
            }
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'email' => $request->email
                ],
                'user' => $user[0]
            ]));

        }
    }

    public function verify($token) {
        $verifies = $this->verificationRepository->findByField('token', $token);
        $user_id = 0;
        if (!is_null($verifies) && sizeof($verifies) > 0) {
            foreach ($verifies as $verify) {
                $user_id = $verify['user_id'];
                $this->verificationRepository->delete($verify['id']);
            }
        }
        $this->userRepository->update(array(
            'email_verified_at' => $this->freshTimestamp()
        ), $user_id);
        $user = $this->userRepository->findByField('id', $user_id);
        if (!is_null($user) && sizeof($user) > 0 && !is_null($user[0]['email_verified_at'])) {
            if (strcmp('internal', $user[0]['type_of_user']) == 0) {
                return redirect('admin/authenticate/login');
            } else {
                return redirect('home');
            }
        }
        return redirect('home');
    }

    public function check()
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response(['authenticated' => false]);
        }
        return response([
            'authenticated' => true,
            'permissions' => json_encode(Auth::guard('web')->user()->allPermissions, true)
        ]);
    }

    public function avatar() {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            response(['authenticated' => false]);
        }
        response(['authenticated' => true]);
    }

    public function logout()
    {
        try {
            $token = JWTAuth::getToken();

            if ($token) {
                JWTAuth::invalidate($token);
            }
        } catch (JWTException $e) {
            return response()->json($e->getMessage(), 401);
        }
        return response()->json(['message' => 'Log out success'], 200);
    }

    public function flogout()
    {
        try {
            $token = JWTAuth::getToken();

            if ($token) {
                JWTAuth::invalidate($token);
            }
        } catch (JWTException $e) {
            return response()->json($e->getMessage(), 401);
        }
        return response()->json(['message' => 'Log out success'], 200);
    }

    public function social($provider) {
        $auth = Socialite::driver($provider)
            ->stateless()->user();
        $user = $this->userRepository->findByField('email', $auth->getEmail());
        $password = self::generatePassword(8);
        if (strcmp('google', $provider) == 0) {
            $avatar = '/images/logo_google.png';
        } else {
            $avatar = '/images/logo_facebook.png';
        }
        if (!isset($avatar)) {
            $avatar = '/images/user.svg';
        }
        // all good so return the token
        if (!is_null($user) && sizeof($user) > 0) {
            $this->userRepository->update(array(
                'email' => $auth->getEmail() != NULL ? $auth->getEmail() : $user[0]['email'],
                'avatar' => $avatar,
                'name_vi' => $auth->getName() != NULL ? $auth->getName() : $user[0]['name_vi'],
                'name_en' => $auth->getName() != NULL ? $auth->getName() : $user[0]['name_en'],
                'created_at' => $this->freshTimestamp(),
                'updated_at' => $this->freshTimestamp()
            ), $user[0]['id']);
        } else {
            $this->userRepository->create(array(
                'email' => $auth->getEmail(),
                'password' => Hash::make($password),
                'name_vi' => $auth->getName(),
                'name_en' => $auth->getName(),
                'avatar' => $avatar,
                'created_at' => $this->freshTimestamp(),
                'updated_at' => $this->freshTimestamp()
            ));
        }

        if (!is_null($user) && sizeof($user) > 0) {
            $user = User::where('email', '=', $auth->getEmail())->first();
            $token = JWTAuth::fromUser($user, ['exp' => Carbon::now()->addDays(1)->timestamp]);
        } else {
            $request = new Request();
            $request->replace(
                [
                    'email' => $auth->getEmail(),
                    'password' => $password
                ]
            );
            // Grab credentials from the request
            $credentials = $request->only('email', 'password');
            if (! $token = JWTAuth::attempt($credentials, ['exp' => Carbon::now()->addDays(1)->timestamp])) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        }
        return response()->json(compact('token', 'avatar'));
    }

    private function generatePassword($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
