<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Model\Role;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Role\RoleRepository;
use App\Traits\FlagUtitlites;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;

class RoleController extends Controller
{
    use FlagUtitlites;
    use HasTimestamps;

    protected $requestGuzzle;
    protected $repository;
    protected $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository) {
        $this->requestGuzzle = new Client();
        $this->repository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function index()
    {
        if ($this->preventCrawlingData('security')) {
            if (strcmp('all', request()->header('type')) == 0) {
                return $this->repository->getAll();
            } else if (strcmp('detail', request()->header('type')) == 0) {
                return $this->repository->findWhere([
                    ['name','LIKE','%' . request()->header('keyword') . '%']
                ]);
            } else {
                $length = request()->input('length');
                $sortBy = request()->input('column');
                $orderBy = request()->input('dir');
                $searchValue = request()->input('search');

                $query = Role::eloquentQuery($sortBy, $orderBy, $searchValue);

                $data = $query->paginate($length);

                return new DataTableCollectionResource($data);
            }
        } else {
            return response()->json(
                [
                    'error' => [
                        'code' => 401,
                        'message' => 'Token is not provided'
                    ],
                    'data' => null,
                ]
            );
        }
    }

    public function store(Request $request) {
        $credentials = $request->only('name', 'guard_name');

        $rules = [
            'name' => 'required',
            'guard_name' => 'required'
        ];

        $customMessages = (strcmp("vi", $request->lang) == 0) ? [
            'name.required' => 'Tên vai trò là bắt buộc.',
            'guard_name.required' => 'Thiết lập Guard là bắt buộc.'
        ] : [
            'name.required' => 'Role name is required.',
            'guard_name.required' => 'Guard name is required.'
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        } else {
            $role = $this->repository->findByField('name', $request->name);
            if (!is_null($role) && sizeof($role) > 0) {
                return json_encode(([
                    'message' => [
                        'status' => "error",
                        'description' => "The role has been existed in our system"
                    ],
                    'role' => $role[0]
                ]));
            } else {
                \Spatie\Permission\Models\Role::create(
                    [
                        'name' => $request->name,
                        'guard_name' => $request->guard_name,
                        'created_at' => $this->freshTimestamp(),
                        'updated_at' => $this->freshTimestamp()
                    ]
                );
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "Add new role successfully!"
                    ],
                    'role' => $this->repository->findByField('name', $request->name)[0]
                ]));
            }
        }
    }

    public function update(Request $request, $id) {
        $credentials = $request->only('name', 'guard_name');

        $rules = [
            'name' => 'required',
            'guard_name' => 'required'
        ];

        $customMessages = (strcmp("vi", $request->lang) == 0) ? [
            'name.required' => 'Tên vai trò là bắt buộc.',
            'guard_name.required' => 'Thiết lập Guard là bắt buộc.'
        ] : [
            'name.required' => 'Role name is required.',
            'guard_name.required' => 'Guard name is required.'
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        } else {
            $role = $this->repository->findWhere([
                ['name','=',$request->name],
                ['id','<>',$id]
            ]);
            if (!is_null($role) && sizeof($role) > 0) {
                return json_encode(([
                    'message' => [
                        'status' => "error",
                        'description' => "The role has been existed in our system"
                    ],
                    'role' => $role[0]
                ]));
            } else {
                $this->repository->update(array(
                    'name' => $request->name,
                    'guard_name' => $request->guard_name,
                    'created_at' => $this->freshTimestamp(),
                    'updated_at' => $this->freshTimestamp()
                ), $id);
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "Role updated successfully"
                    ]
                ]));
            }
        }
    }

    public function show($roleId) {
        $temps = DB::table('role_has_permissions')
            ->where('role_id', $roleId)->get();
        $stack = array();
        foreach ($temps as $temp) {
            $permission = $this->permissionRepository->findByField('id', $temp->permission_id);
            if (!is_null($permission) && sizeof($permission) > 0) {
                array_push($stack, ($permission[0]['name']));
            }
        };
        return $stack;
    }

    public function destroy($id)
    {
        if ($this->repository->findByField('id', $id, ['*']) != null) {
            $this->repository->delete($id);
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Role deleted"
                ],
                'id' => $id
            ]));
        } else {
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => "Role delete failure"
                ]
            ]));
        }
    }
}
