<?php

namespace App\Http\Controllers\API\V1;

use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use App\Repositories\BlogTag\BlogTagRepository;
use App\Repositories\Tag\TagRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Config;
use Illuminate\Http\Request;
use Storage;

class TagController extends Controller
{

    protected $repository;
    protected $blogTagRepository;

    public function __construct(TagRepository $repository, BlogTagRepository $blogTagRepository) {
        $this->repository = $repository;
        $this->blogTagRepository = $blogTagRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index()
    {
        if (request()->header('blog_id') == -1) {
            return $this->repository->getAll();
        } else if (request()->header('blog_id') != 0) {
            $blogTags = $this->blogTagRepository->findByField('blog_id', request()->header('blog_id'));
            $stack = [];
            foreach ($blogTags as $blogTag) {
                $tag = $this->repository->findByField('id',  $blogTag['tag_id']);
                if (!is_null($tag) && sizeof($tag) > 0) {
                    if (strcmp('vi', request()->header('locale')) == 0) {
                        array_push($stack, [
                            "id" => $tag[0]['id'],
                            "name_vi" => $tag[0]['name_vi'],
                            "name_en" => $tag[0]['name_en']
                        ]);
                    } else {
                        array_push($stack, [
                            "id" => $tag[0]['id'],
                            "name_en" => $tag[0]['name_en'],
                            "name_vi" => $tag[0]['name_vi']
                        ]);
                    }
                }
            }
            return [
                'all' => $this->repository->getAll(),
                'case' => $stack
            ];
        } else {
            return $this->repository->lastestPaginate(20);
        }
    }

    public function store(Request $request) {
        $credentials = $request->only('name', 'description');
        $rules = [
            'name' => 'required',
            'description' => 'required|max:160'
        ];

        $customMessages = (strcmp("vi", $request->header('locale')) == 0) ? [
            'name.required' => 'Trường thẻ là bắt buộc.',
            'description.required' => 'Trường mô tả là bắt buộc.'
        ] : [
            'name.required' => 'The tag is required.',
            'description.required' => 'The description is required.'
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            if (strcmp('vi', $request->lang) == 0) {
                $request->merge([
                    'name_vi' => $request->name,
                    'slug_vi' => $request->slug,
                    'description_vi' => $request->description
                ]);
            } else {
                $request->merge([
                    'name_en' => $request->name,
                    'slug_en' => $request->slug,
                    'description_en' => $request->description
                ]);
            }
            $request->merge([
                'user_id' => Auth::user()->id,
                'status' => $request->status ? 1 : 0,
                'is_editable' => $request->is_editable ? 1 : 0
            ]);
            $this->repository->create($request->all());
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Tag created successfully"
                ],
                'tag' => $request->all()
            ]));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = $this->repository->findByField('id', $id);
        if (!is_null($tag) && (sizeof($tag) > 0)) {
            $credentials = $request->only('name', 'description');
            $rules = [
                'name' => 'required',
                'description' => 'required|max:160'
            ];

            $customMessages = (strcmp("vi", $request->header('locale')) == 0) ? [
                'name.required' => 'Trường thẻ là bắt buộc.',
                'description.required' => 'Trường mô tả là bắt buộc.'
            ] : [
                'name.required' => 'The tag is required.',
                'description.required' => 'The description is required.'
            ];
            $validator = Validator::make($credentials, $rules, $customMessages);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            } else {
                if (strcmp('vi', $request->lang) == 0) {
                    $request->merge([
                        'name_vi' => ($request->lang == "vi") ? $request->name : ((!is_null($tag) && sizeof($tag) > 0) ? $tag[0]['name_vi'] : ""),
                        'slug_vi' => ($request->lang == "vi") ? $request->slug : ((!is_null($tag) && sizeof($tag) > 0) ? $tag[0]['slug_vi'] : ""),
                        'description_vi' => ($request->lang == "vi") ? $request->description : ((!is_null($tag) && sizeof($tag) > 0) ? $tag[0]['description_vi'] : "")
                    ]);
                } else {
                    $request->merge([
                        'name_en' => ($request->lang == "en") ? $request->name : ((!is_null($tag) && sizeof($tag) > 0) ? $tag[0]['name_en'] : ""),
                        'slug_en' => ($request->lang == "en") ? $request->slug : ((!is_null($tag) && sizeof($tag) > 0) ? $tag[0]['slug_en'] : ""),
                        'description_en' => ($request->lang == "en") ? $request->description : ((!is_null($tag) && sizeof($tag) > 0) ? $tag[0]['description_en'] : "")
                    ]);
                }
                $request->merge([
                    'user_id' => Auth::user()->id,
                    'status' => $request->status ? 1 : 0,
                    'is_editable' => $request->is_editable ? 1 : 0
                ]);
                $this->repository->update($request->all(), $id);
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "Tag updated"
                    ],
                    'id' => $id
                ]));
            }
        } else {
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Tag updated failure"
                ]
            ]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->repository->findByField('id', $id, ['*']) != null) {

            $blogtags = $this->blogTagRepository->findByField('tag_id', $id);
            if (!is_null($blogtags) && sizeof($blogtags) > 0) {
                foreach ($blogtags as $blogtag) {
                    $this->blogTagRepository->delete($blogtag['id']);
                }
            }

            $this->repository->delete($id);
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Tag deleted"
                ],
                'id' => $id
            ]));
        } else {
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => "Tag delete failure"
                ]
            ]));
        }
    }
}
