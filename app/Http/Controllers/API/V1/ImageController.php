<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Thumbnail\ThumbnailRepository;
use App\Traits\AmazonS3Trait;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{

    use AmazonS3Trait;
    use HasTimestamps;

    protected $thumbnailRepository;

    public function __construct(ThumbnailRepository $thumbnailRepository) {
        $this->thumbnailRepository = $thumbnailRepository;
    }

    public function index()
    {
        return $this->thumbnailRepository->lastestPaginate(10, request()->header('project_id'));
    }

    public function store(Request $request) {
        $this->saveThumbnail($request, 'file');
        if (self::getType(getimagesize($request->thumbnail)[0], getimagesize($request->thumbnail)[1]) > 0) {
            $this->thumbnailRepository->create(array(
                'project_id' => $request->project_id,
                'thumbnail' => $request->thumbnail,
                'width' => getimagesize($request->thumbnail)[0],
                'height' => getimagesize($request->thumbnail)[1],
                'type' => self::getType(getimagesize($request->thumbnail)[0], getimagesize($request->thumbnail)[1]),
                'created_at' => $this->freshTimestamp(),
                'updated_at' => $this->freshTimestamp()
            ));
        } else {
            $pathinfo = pathinfo($request->thumbnail);
            Storage::disk(S3)->delete('/leearchitects/thumbnail/'.$pathinfo['filename'].'.'.$pathinfo['extension']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return false|Response|string
     */
    public function destroy($id)
    {
        $thumbnail = $this->thumbnailRepository->findByField('id', $id, ['*']);
        if (!is_null($thumbnail) && sizeof($thumbnail) > 0) {
            $pathinfo = pathinfo($thumbnail[0]['thumbnail']);
            Storage::disk(S3)->delete('/leearchitects/thumbnail/'.$pathinfo['filename'].'.'.$pathinfo['extension']);
            $this->thumbnailRepository->delete($id);
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Thumbnail deleted"
                ],
                'id' => $id
            ]));
        } else {
            return json_encode([
                'message' => [
                    'status' => "error",
                    'description' => "Thumbnail delete failure",
                ]
            ]);
        }
    }

    public function blog(Request $request) {
        $this->saveBlog($request, 'file');
        return $request->image;
    }

    public function getType($width, $height) {
        if (920 == $width || 770 == $height) {
            return 1; // Slider
        } else if (1304 == $width || 868 == $height) {
            return 2; // Thumbnail
        } else if (362 == $width || 167 == $height) {
            return 3; // Small Thumbnail
        } else if (1920 == $width || 326 == $height) {
            return 4; // Project Banner
        }
        return 0;
    }
}
