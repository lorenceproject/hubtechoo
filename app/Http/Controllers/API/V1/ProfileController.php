<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Profile\ProfileRepository;
use App\Repositories\User\UserRepository;
use App\Traits\AmazonS3Trait;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class ProfileController extends Controller
{

    use AmazonS3Trait;
    use HasTimestamps;

    protected $repository;
    protected $profileRepository;
    protected $tokenRepository;

    public function __construct(UserRepository $repository, ProfileRepository $profileRepository) {
        $this->repository = $repository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return false|\Illuminate\Http\Response|string
     */
    public function index()
    {
        $profile = $this->profileRepository->findByField('user_id', Auth::user()->id);
        if (!is_null($profile) && sizeof($profile) > 0) {
            return json_encode(([
                'profile' => $profile[0]
            ]));
        }
        return null;
    }

    public function store(Request $request) {
        $credentials = $request->only('name', 'phone_number', 'job','address', 'description', 'username');
        $profile = $this->profileRepository->findByField('id', $request->id);
        $rules = [
            'name' => 'required',
            'phone_number' => 'required',
            'job' => 'required',
            'username' => 'required',
            'address' => 'required',
            'description' => 'required'
        ];
        $customMessages = (strcmp("vi", $request->header('locale')) == 0) ? [
            'name.required' => 'Trường họ và tên là bắt buộc.',
            'phone_number.required' => 'Trường số điện thoại là bắt buộc.',
            'job.required' => 'Trường nghề nghiệp là bắt buộc.',
            'username.required' => 'Trường tên đăng nhập là bắt buộc.',
            'address.required' => 'Trường địa chỉ là bắt buộc.',
            'description.required' => 'Trường mô tả là bắt buộc.'
        ] : [
            'name.required' => 'The full name field is required.',
            'phone_number.required' => 'The phone number field is required.',
            'job.required' => 'The job is required.',
            'username.required' => 'The Username is required.',
            'address.required' => 'The address is required.',
            'description.required' => 'The description is required.'
        ];
        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $request->merge([
                'name_en' => ($request->lang == "en") ? $request->name : ((!is_null($profile) && sizeof($profile) > 0) ? $profile[0]['name_en'] : ""),
                'name_vi' => ($request->lang == "vi") ? $request->name : ((!is_null($profile) && sizeof($profile) > 0) ? $profile[0]['name_vi'] : ""),
                'job_en' => ($request->lang == "en") ? $request->job : ((!is_null($profile) && sizeof($profile) > 0) ? $profile[0]['job_en'] : ""),
                'job_vi' => ($request->lang == "vi") ? $request->job : ((!is_null($profile) && sizeof($profile) > 0) ? $profile[0]['job_vi'] : ""),
                'address_en' => ($request->lang == "en") ? $request->address : ((!is_null($profile) && sizeof($profile) > 0) ? $profile[0]['address_en'] : ""),
                'address_vi' => ($request->lang == "vi") ? $request->address : ((!is_null($profile) && sizeof($profile) > 0) ? $profile[0]['address_vi'] : ""),
                'description_en' => ($request->lang == "en") ? $request->description : ((!is_null($profile) && sizeof($profile) > 0) ? $profile[0]['description_en'] : ""),
                'description_vi' => ($request->lang == "vi") ? $request->description : ((!is_null($profile) && sizeof($profile) > 0) ? $profile[0]['description_vi'] : ""),
                'date_of_birth' => Carbon::parse($request->date_of_birth),
                'status' => strcmp('false', $request->status) == 0 ? 0 : 1,
                'is_editable' => strcmp('false', $request->is_editable) == 0 ? 0 : 1,
                'created_at' => $this->freshTimestamp(),
                'updated_at' => $this->freshTimestamp()
            ]);
            $this->profileRepository->update($request->all(), $request->id);
            $this->repository->update(array(
                'name_en' => ($request->lang == "en") ? $request->name : ((!is_null($profile) && sizeof($profile) > 0) ? $profile[0]['name_en'] : ""),
                'name_vi' => ($request->lang == "vi") ? $request->name : ((!is_null($profile) && sizeof($profile) > 0) ? $profile[0]['name_vi'] : ""),
                'username' => $request->username
            ), Auth::user()->id);
            return json_encode([
                'result' => true
            ]);
        }
    }
}
