<?php

namespace App\Http\Controllers\API\V1\Search;

use App\Helpers\AppHelper;
use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Repositories\Blog\BlogRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Tag\TagRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    protected $repository;
    protected $tagRepository;
    protected $userRepository;
    protected $categoryRepository;

    public function __construct(UserRepository $userRepository, BlogRepository $blogRepository, CategoryRepository $categoryRepository, TagRepository $tagRepository) {
        $this->repository = $blogRepository;
        $this->tagRepository = $tagRepository;
        $this->userRepository = $userRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function search(Request $request) {
        if (strcmp(Constants::BLOG, $request->type) == 0) {
            if (strcmp(Constants::LANGUAGE_VI, AppHelper::getLanguageRequest()) == 0) {
                return $this->repository->search([
                    ['title_vi','LIKE','%' . $request->keyword . '%'],
                    ['status', '=', $request->status]
                ], $request->date, 20);
            } else {
                return $this->repository->search([
                    ['title_en','LIKE','%' . $request->keyword . '%'],
                    ['status', '=', $request->status]
                ], $request->date, 20);
            }
        } else if (strcmp(Constants::CATEGORY, $request->type) == 0) {
            if (strcmp(Constants::LANGUAGE_VI, AppHelper::getLanguageRequest()) == 0) {
                return $this->categoryRepository->search([
                    ['name_vi','LIKE','%' . $request->keyword . '%'],
                    ['status', '=', $request->status]
                ], $request->date, 20);
            } else {
                return $this->categoryRepository->search([
                    ['name_en','LIKE','%' . $request->keyword . '%'],
                    ['status', '=', $request->status]
                ], $request->date, 20);
            }
        } else if (strcmp(Constants::TAG, $request->type) == 0) {
            if (strcmp(Constants::LANGUAGE_VI, AppHelper::getLanguageRequest()) == 0) {
                return $this->tagRepository->search([
                    ['name_vi','LIKE','%' . $request->keyword . '%'],
                    ['status', '=', $request->status]
                ], $request->date, 20);
            } else {
                return $this->tagRepository->search([
                    ['name_en','LIKE','%' . $request->keyword . '%'],
                    ['status', '=', $request->status]
                ], $request->date, 20);
            }
        } else if (strcmp(Constants::USER, $request->type) == 0) {
            if (strcmp(Constants::LANGUAGE_VI, AppHelper::getLanguageRequest()) == 0) {
                return $this->userRepository->search([
                    ['name_vi','LIKE','%' . $request->keyword . '%'],
                    ['status', '=', $request->status]
                ], $request->date, 20, $request->header('_case'));
            } else {
                return $this->userRepository->search([
                    ['name_en','LIKE','%' . $request->keyword . '%'],
                    ['status', '=', $request->status]
                ], $request->date, 20, $request->header('_case'));
            }
        }
    }
}
