<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryThumbnail\CategoryThumbnailRepository;
use App\Traits\AmazonS3Trait;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Http\Request;
use Storage;

class CategoryImageController extends Controller
{

    use AmazonS3Trait;
    use HasTimestamps;

    protected $categoryThumbnailRepository;

    public function __construct(CategoryThumbnailRepository $categoryThumbnailRepository) {
        $this->categoryThumbnailRepository = $categoryThumbnailRepository;
    }

    public function index()
    {
        return $this->categoryThumbnailRepository->lastestPaginate(10, request()->header('category_id'));
    }

    public function store(Request $request) {
        $this->saveCategory($request, 'file');
        $this->categoryThumbnailRepository->create(array(
            'category_id' => $request->category_id,
            'thumbnail' => $request->image,
            'width' => getimagesize($request->image)[0],
            'height' => getimagesize($request->image)[1],
            'type' => self::getType(getimagesize($request->image)[0], getimagesize($request->image)[1]),
            'created_at' => $this->freshTimestamp(),
            'updated_at' => $this->freshTimestamp()
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $thumbnail = $this->categoryThumbnailRepository->findByField('id', $id, ['*']);
        if (!is_null($thumbnail) && sizeof($thumbnail) > 0) {
            $pathinfo = pathinfo($thumbnail[0]['thumbnail']);
            Storage::disk(S3)->delete('/leearchitects/category/'.$pathinfo['filename'].'.'.$pathinfo['extension']);
            $this->categoryThumbnailRepository->delete($id);
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Category Thumbnail deleted"
                ],
                'id' => $id
            ]));
        } else {
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => "Category Thumbnail delete failure",
                ]
            ]));
        }
    }

    public function getType($width, $height) {
        if (208 == $width || 258 == $height) {
            return 1; // Thumbnail Vertical
        } else if (355 == $width || 237 == $height) {
            return 2; // Thumbnail Horizontal
        } else if (600 == $width || 315 == $height) {
            return 4; // Facebook Minimum Size
        } else if (1200 == $width && 630 == $height) {
            return 5; // Facebook Recommended Size
        } else if (1200 == $width && 800 == $height) {
            return 6; // Twitter Recommended Size
        }
        return 0;
    }
}
