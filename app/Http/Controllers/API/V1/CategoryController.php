<?php

namespace App\Http\Controllers\API\V1;

use App\Helpers\AppHelper;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\CategoryThumbnail\CategoryThumbnailRepository;
use App\Traits\AmazonS3Trait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Config;
use Storage;

class CategoryController extends Controller
{

    protected $repository;
    protected $categoryThumbnailRepository;

    use AmazonS3Trait;

    public function __construct(CategoryRepository $categoryRepository, CategoryThumbnailRepository $categoryThumbnailRepository) {
        $this->repository = $categoryRepository;
        $this->categoryThumbnailRepository = $categoryThumbnailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return false|Response|string
     */
    public function index()
    {
        if (strcmp('all', request()->header('type')) == 0) {
            if (empty(request()->header('category_id'))) {
                $temps = $this->repository->getAll();
                foreach ($temps as $temp) {
                    $temp['label'] = AppHelper::getLanguageRequest() == 'vi' ? $temp->name_vi : $temp->name_en;
                    $temp['children'] = $this->repository->findWhere([
                        'parent_id' => $temp->id,
                        'level' => $temp->level + 1
                    ]);
                    self::removeUnnecessary($temp);
                    // Level 1:
                    if (!is_null($temp->children)) {
                        foreach ($temp->children as $temp1) {
                            $temp1['label'] = AppHelper::getLanguageRequest() == 'vi' ? $temp1->name_vi : $temp1->name_en;
                            $temp1['children'] = $this->repository->findWhere([
                                'parent_id' => $temp1->id,
                                'level' => $temp1->level + 1
                            ]);
                            self::removeUnnecessary($temp1);

                            // Level 2
                            if (!is_null($temp1->children)) {
                                foreach ($temp1->children as $temp2) {
                                    $temp2['label'] = AppHelper::getLanguageRequest() == 'vi' ? $temp2->name_vi : $temp2->name_en;
                                    $temp2['children'] = $this->repository->findWhere([
                                        'parent_id' => $temp2->id,
                                        'level' => $temp2->level + 1
                                    ]);
                                    self::removeUnnecessary($temp2);

                                    // Level 3
                                    if (!is_null($temp2->children)) {
                                        foreach ($temp2->children as $temp3) {
                                            $temp3['label'] = AppHelper::getLanguageRequest() == 'vi' ? $temp3->name_vi : $temp3->name_en;
                                            $temp3['children'] = $this->repository->findWhere([
                                                'parent_id' => $temp3->id,
                                                'level' => $temp3->level + 1
                                            ]);
                                            self::removeUnnecessary($temp3);

                                            // Level 4
                                            if (!is_null($temp3->children)) {
                                                foreach ($temp3->children as $temp4) {
                                                    $temp4['label'] = AppHelper::getLanguageRequest() == 'vi' ? $temp4->name_vi : $temp4->name_en;
                                                    $temp4['children'] = $this->repository->findWhere([
                                                        'parent_id' => $temp4->id,
                                                        'level' => $temp4->level + 1
                                                    ]);
                                                    self::removeUnnecessary($temp4);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return $temps;
            } else {
                $category = $this->repository->findByField('id', request()->header('category_id'));
                $temps = $this->repository->getAll();
                foreach ($temps as $temp) {
                    $temp['label'] = AppHelper::getLanguageRequest() == 'vi' ? $temp->name_vi : $temp->name_en;
                    $temp['children'] = $this->repository->findWhere([
                        'parent_id' => $temp->id,
                        'level' => $temp->level + 1
                    ]);
                    self::removeUnnecessary($temp);
                    // Level 1:
                    if (!is_null($temp->children)) {
                        foreach ($temp->children as $temp1) {
                            $temp1['label'] = AppHelper::getLanguageRequest() == 'vi' ? $temp1->name_vi : $temp1->name_en;
                            $temp1['children'] = $this->repository->findWhere([
                                'parent_id' => $temp1->id,
                                'level' => $temp1->level + 1
                            ]);
                            self::removeUnnecessary($temp1);

                            // Level 2
                            if (!is_null($temp1->children)) {
                                foreach ($temp1->children as $temp2) {
                                    $temp2['label'] = AppHelper::getLanguageRequest() == 'vi' ? $temp2->name_vi : $temp2->name_en;
                                    $temp2['children'] = $this->repository->findWhere([
                                        'parent_id' => $temp2->id,
                                        'level' => $temp2->level + 1
                                    ]);
                                    self::removeUnnecessary($temp2);

                                    // Level 3
                                    if (!is_null($temp2->children)) {
                                        foreach ($temp2->children as $temp3) {
                                            $temp3['label'] = AppHelper::getLanguageRequest() == 'vi' ? $temp3->name_vi : $temp3->name_en;
                                            $temp3['children'] = $this->repository->findWhere([
                                                'parent_id' => $temp3->id,
                                                'level' => $temp3->level + 1
                                            ]);
                                            self::removeUnnecessary($temp3);

                                            // Level 4
                                            if (!is_null($temp3->children)) {
                                                foreach ($temp3->children as $temp4) {
                                                    $temp4['label'] = AppHelper::getLanguageRequest() == 'vi' ? $temp4->name_vi : $temp4->name_en;
                                                    $temp4['children'] = $this->repository->findWhere([
                                                        'parent_id' => $temp4->id,
                                                        'level' => $temp4->level + 1
                                                    ]);
                                                    self::removeUnnecessary($temp4);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return json_encode(([
                    'temps' => $temps,
                    'category' => array([
                        $category[0]['id']
                    ])
                ]));
            }
        } else {
            return $this->repository->lastestPaginate(20);
        }
    }

    private function removeUnnecessary($temp) {
        unset($temp->level);
        if (AppHelper::getLanguageRequest() == 'vi') {
            unset($temp->name_vi);
        } else {
            unset($temp->name_en);
        }
        if (is_null($temp->children) || 0 == sizeof($temp->children)) {
            unset($temp->children);
        }
        if (is_null($temp->thumbnail) || 0 == sizeof($temp->thumbnail)) {
            unset($temp->thumbnail);
        }
    }

    public function show($level) {
        if(empty(request()->header('category_id'))) {
            return $this->repository->findWhere([
                'level' => $level-1,
                'status' => '1'
            ]);
        } else {
            $category = $this->repository->findByField("id", request()->header('category_id'));
            if (!is_null($category) && sizeof($category) > 0) {
                return $this->repository->findWhere(
                    [
                        'level' => $category[0]['level'] + ('down' == request()->header('action') ? 1 : -1),
                        'status' => '1'
                    ]
                );
            }
        }
        return array();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return false|Response|string
     * @reference: https://docs.spatie.be/laravel-permission/v3/basic-usage/role-permissions/
     */
    public function store(Request $request)
    {
        $credentials = $request->only('name', 'description');
        $rules = [
            'name' => 'required',
            'description' => 'required|min:6'
        ];

        $customMessages = (strcmp("vi", $request->header('locale')) == 0) ? [
            'name.required' => 'Trường chủ đề là bắt buộc.',
            'description.required' => 'Trường mô tả là bắt buộc.',
            'description.min' => 'Trường mô tả có độ dài ít nhất 6 ký tự.'
        ] : [
            'name.required' => 'The topic is required.',
            'description.required' => 'The description is required.',
            'description.min' => 'The description field should be at least 6 characters.'
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $request->merge([
                'user_id' => Auth::user()->id,
                'name_en' => ($request->lang == "en") ? $request->name : "",
                'name_vi' => ($request->lang == "vi") ? $request->name : "",
                'slug_vi' => ($request->lang == "vi") ? $request->slug : "",
                'slug_en' => ($request->lang == "en") ? $request->slug : "",
                'description_en' => ($request->lang == "en") ? $request->description : "",
                'description_vi' => ($request->lang == "vi") ? $request->description : ""
            ]);
            $this->repository->create($request->all());
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Category created successfully"
                ],
                'user' => $request->all()
            ]));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return false|\Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request, $id)
    {
        $category = $this->repository->findByField('id', $id);
        if (!is_null($category) && (sizeof($category) > 0)) {
            $credentials = $request->only('name', 'description');
            $rules = [
                'name' => 'required',
                'description' => 'required|min:32'
            ];

            $customMessages = (strcmp("vi", $request->header('locale')) == 0) ? [
                'name.required' => 'Trường chủ đề là bắt buộc.',
                'description.required' => 'Trường mô tả là bắt buộc.',
                'description.min' => 'Trường mô tả có độ dài ít nhất 32 ký tự.'
            ] : [
                'name.required' => 'The topic is required.',
                'description.required' => 'The description is required.',
                'description.min' => 'The description field should be at least 32 characters.'
            ];
            $validator = Validator::make($credentials, $rules, $customMessages);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            } else {
                $request->merge([
                    'user_id' => Auth::user()->id,
                    'name_en' => ($request->lang == "en") ? $request->name : ((!is_null($category) && sizeof($category) > 0) ? $category[0]['name_en'] : ""),
                    'name_vi' => ($request->lang == "vi") ? $request->name : ((!is_null($category) && sizeof($category) > 0) ? $category[0]['name_vi'] : ""),
                    'slug_en' => ($request->lang == "en") ? $request->slug : ((!is_null($category) && sizeof($category) > 0) ? $category[0]['slug_en'] : ""),
                    'slug_vi' => ($request->lang == "vi") ? $request->slug : ((!is_null($category) && sizeof($category) > 0) ? $category[0]['slug_vi'] : ""),
                    'description_en' => ($request->lang == "en") ? $request->description : ((!is_null($category) && sizeof($category) > 0) ? $category[0]['description_en'] : ""),
                    'description_vi' => ($request->lang == "vi") ? $request->description : ((!is_null($category) && sizeof($category) > 0) ? $category[0]['description_vi'] : "")
                ]);
                $this->repository->update($request->all(), $id);
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "Category updated"
                    ],
                    'id' => $id
                ]));
            }
        } else {
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "User updated failure"
                ]
            ]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return false|Response|string
     */
    public function destroy($id)
    {
        if ($this->repository->findByField('id', $id, ['*']) != null) {

            $thumbnails = $this->categoryThumbnailRepository->findByField('category_id', $id);
            if (!is_null($thumbnails) && sizeof($thumbnails) > 0) {
                foreach ($thumbnails as $thumbnail) {
                    $this->categoryThumbnailRepository->delete($thumbnail['id']);

                    $pathinfo = pathinfo($thumbnail['thumbnail']);
                    Storage::disk(S3)->delete('/leearchitects/category/'.$pathinfo['filename'].'.'.$pathinfo['extension']);
                }
            }

            $this->repository->delete($id);
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Category deleted"
                ],
                'id' => $id
            ]));
        } else {
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => "Category delete failure",
                ]
            ]));
        }
    }
}
