<?php

namespace App\Http\Controllers\API\V1;

use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Profile\ProfileRepository;
use App\Repositories\Role\RoleRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\Verification\VerificationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    protected $repository;
    protected $roleRepository;
    protected $profileRepository;
    protected $permissionRepository;
    protected $verificationRepository;

    public function __construct(VerificationRepository $verificationRepository, PermissionRepository $permissionRepository, RoleRepository $roleRepository, UserRepository $repository, ProfileRepository $profileRepository) {
        $this->repository = $repository;
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
        $this->profileRepository = $profileRepository;
        $this->verificationRepository = $verificationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (strcmp('all', request()->header('type')) == 0) {
            return $this->repository->getAll();
        } else if (strcmp('detail', request()->header('type')) == 0) {
            if ('vi' == request()->header('lang')) {
                return $this->repository->findWhere([
                    ['name_vi','LIKE','%' . request()->header('keyword') . '%']
                ]);
            } else {
                return $this->repository->findWhere([
                    ['name_en','LIKE','%' . request()->header('keyword') . '%']
                ]);
            }
        } else {
            return $this->repository->lastestPaginate(10, request()->header('_case'));
        }
    }

    public function show($userId) {
        $stack = array();
        if (strcmp('role', request()->header('type')) == 0) {
            $temps = DB::table('model_has_roles')
                ->where('model_id', $userId)->get();
            foreach ($temps as $temp) {
                $role = $this->roleRepository->findByField('id', $temp->role_id);
                if (!is_null($role) && sizeof($role) > 0) {
                    array_push($stack,$role[0]['name']);
                }
            };
        } else {
            $temps = DB::table('model_has_permissions')
                ->where('model_id', $userId)->get();
            foreach ($temps as $temp) {
                $permission = $this->permissionRepository->findByField('id', $temp->permission_id);
                if (!is_null($permission) && sizeof($permission) > 0) {
                    array_push($stack,$permission[0]['name']);
                }
            };
        }
        return $stack;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @reference: https://docs.spatie.be/laravel-permission/v3/basic-usage/role-permissions/
     */
    public function store(Request $request)
    {
        $credentials = $request->only('name', 'password', 'username', 'email');
        $rules = [
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required|min:6'
        ];

        $customMessages = (strcmp("vi", $request->header('locale')) == 0) ? [
            'name.required' => 'Trường họ và tên là bắt buộc.',
            'name.regex' => 'Trường tên là bảng chữ cái bắt buộc.',
            'email.required' => 'Trường địa chỉ hộp thư là bắt buộc.',
            'email.unique' => 'Trường địa chỉ hộp thư đã tồn tại trong hệ thống.',
            'username.required' => 'Trường tài khoản người dùng là bắt buộc.',
            'username.unique' => 'Trường tài khoản người dùng đã tồn tại trong hệ thống.',
            'password.required' => 'Trường mật khẩu là bắt buộc.'
        ] : [
            'name.required' => 'The full name field is required.',
            'name.regex' => 'The full name field is required alphabets.',
            'email.required' => 'The mail address field is required.',
            'email.unique' => 'The mail address already exists on the system.',
            'username.required' => 'The username field is required.',
            'username.unique' => 'The user field already exists in the system.',
            'password.required' => 'The password field is required.',
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $request->merge([
                'name_en' => ($request->lang == "en") ? $request->name : "",
                'name_vi' => ($request->lang == "vi") ? $request->name : "",
                'type_of_user' => $request->header('type_of_user'), /** Only user in internal */
                'password' => Hash::make($request->password)
            ]);
            $this->repository->create($request->all());
            $user = $this->repository->findByField('email', $request->email)[0];

            if (strcmp("vi", $request->header('locale')) == 0) {
                $this->profileRepository->create(array(
                    'user_id' => $user['id'],
                    'name_vi' => $user['name_vi']
                ));
            } else {
                $this->profileRepository->create(array(
                    'user_id' => $user['id'],
                    'name_en' => $user['name_en']
                ));
            }
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "User created successfully"
                ],
                'user' => $request->all()
            ]));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->repository->findByField('id', $id, ['*']);
        if (!is_null($user) && sizeof($user) > 0) {
            $credentials = $request->only('name', 'password', 'username', 'email');
            $rules = [
                'name' => 'required',
                'username' => 'required|unique:users,username,'.$id,
                'email' => 'required|unique:users,email,'.$id,
                'password' => 'required|min:6'
            ];

            $customMessages = (strcmp("vi", $request->header('locale')) == 0) ? [
                'name.required' => 'The full name field is required.',
                'name.regex' => 'Trường tên là bảng chữ cái bắt buộc.',
                'email.required' => 'Trường địa chỉ hộp thư là bắt buộc.',
                'email.unique' => 'Trường địa chỉ hộp thư đã tồn tại trong hệ thống.',
                'username.required' => 'Trường tài khoản người dùng là bắt buộc.',
                'username.unique' => 'Trường tài khoản người dùng đã tồn tại trong hệ thống.',
                'password.required' => 'Trường mật khẩu là bắt buộc.'
            ] : [
                'name.required' => 'Trường họ và tên là bắt buộc.',
                'name.regex' => 'The full name field is required alphabets.',
                'email.required' => 'The mail address field is required.',
                'email.unique' => 'The mail address already exists on the system.',
                'username.required' => 'The username field is required.',
                'username.unique' => 'The user field already exists in the system.',
                'password.required' => 'The password field is required.'
            ];

            $validator = Validator::make($credentials, $rules, $customMessages);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            } else {
                $request->merge([
                    'name_en' => ($request->lang == "en") ? $request->name : ((!is_null($user) && sizeof($user) > 0) ? $user[0]['name_en'] : ""),
                    'name_vi' => ($request->lang == "vi") ? $request->name : ((!is_null($user) && sizeof($user) > 0) ? $user[0]['name_vi'] : ""),
                    'password' => Hash::make($request->password),
                ]);
                $this->repository->update($request->all(), $id);
                if (strcmp("vi", $request->header('locale')) == 0) {
                    $this->profileRepository->create(array(
                        'user_id' => $user[0]['id'],
                        'name_vi' => $request->name
                    ));
                } else {
                    $this->profileRepository->create(array(
                        'user_id' => $user[0]['id'],
                        'name_en' => $request->name
                    ));
                }
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "User updated successfully"
                    ],
                    'id' => $id
                ]));
            }
        } else {
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "User updated failure"
                ]
            ]));
        }
    }

    public function modelassigntorole(Request $request) {
        $credentials = $request->only('user_id');

        $rules = [
            'user_id' => 'regex:/^\d+$/'
        ];

        $customMessages = (strcmp("vi", $request->lang) == 0) ? [
            'user_id.regex' => 'Vui lòng không đặt quyền tên hoặc vai trò theo cú pháp sau: Tạo Công việc và Công việc vì Công việc bị trùng lặp và phức tạp hơn để hiểu.'
        ] : [
            'user_id.regex' => 'Please dont\' put name permission or role follow this syntax: Create Job and Job because Job is duplicated and more complexity to understand.'
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        } else {
            $user = $this->repository->findByField('id', $request->user_id);
            if (!is_null($user) && sizeof($user) > 0 && !is_null($request->selections)) {
                // Reset and add new
                DB::table('model_has_roles')
                    ->where('model_id', $request->user_id)->delete();

                $role_ids = explode (",", $request->selections);
                DB::table('model_has_roles')
                    ->where('model_id', $request->user_id)->delete();
                foreach ($role_ids as $role_id) {
                    $obj = DB::table('model_has_roles')
                        ->where('model_id', '=', $request->user_id)
                        ->where('role_id', '=', $role_id)->first();
                    if ($obj === null) {
                        DB::table('model_has_roles')->insert(
                            array(
                                'role_id'     =>   $role_id,
                                'model_type' => 'App\Model\User',
                                'model_id'   =>   $request->user_id
                            )
                        );
                    }
                }
            }
            if (null == $request->selections) {
                DB::table('model_has_roles')
                    ->where('model_id', $request->user_id)->delete();
            }
        }
        return null;
    }

    public function modelassigntopermission(Request $request) {
        $credentials = $request->only('user_id');

        $rules = [
            'user_id' => 'regex:/^\d+$/'
        ];

        $customMessages = (strcmp("vi", $request->lang) == 0) ? [
            'user_id.regex' => 'Vui lòng không đặt quyền tên hoặc vai trò theo cú pháp sau: Tạo Công việc và Công việc vì Công việc bị trùng lặp và phức tạp hơn để hiểu.'
        ] : [
            'user_id.regex' => 'Please dont\' put name permission or role follow this syntax: Create Job and Job because Job is duplicated and more complexity to understand.'
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        } else {
            $user = $this->repository->findByField('id', $request->user_id);
            if (!is_null($user) && sizeof($user) > 0 && !is_null($request->selections)) {
                // Reset and add new
                DB::table('model_has_permissions')
                    ->where('model_id', $request->user_id)->delete();

                $permission_ids = explode (",", $request->selections);
                DB::table('model_has_permissions')
                    ->where('model_id', $request->user_id)->delete();
                foreach ($permission_ids as $permission_id) {
                    $obj = DB::table('model_has_permissions')
                        ->where('model_id', '=', $request->user_id)
                        ->where('permission_id', '=', $permission_id)->first();
                    if ($obj === null) {
                        DB::table('model_has_permissions')->insert(
                            array(
                                'permission_id'     =>   $permission_id,
                                'model_type' => 'App\Model\User',
                                'model_id'   =>   $request->user_id
                            )
                        );
                    }
                }
            }
            if (null == $request->selections) {
                DB::table('model_has_permissions')
                    ->where('model_id', $request->user_id)->delete();
            }
        }
        return null;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->repository->findByField('id', $id, ['*']) != null) {
            $profile = $this->profileRepository->findByField('user_id', $id);
            $verifications = $this->verificationRepository->findByField('user_id', $id);
            if (!is_null($verifications) && sizeof($verifications) > 0) {
                foreach ($verifications as $verification) {
                    $this->verificationRepository->delete($verification['id']);
                }
            }
            if (!is_null($profile) && sizeof($profile) > 0) {
                foreach ($profile as $_profile) {
                    $this->profileRepository->delete($_profile['id']);
                }
            }
            $this->repository->delete($id);
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "User deleted"
                ],
                'id' => $id
            ]));
        } else {
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => "User delete failure",
                ]
            ]));
        }
    }
}
