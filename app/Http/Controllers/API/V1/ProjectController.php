<?php

namespace App\Http\Controllers\API\V1;

use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Thumbnail\ThumbnailRepository;
use App\Traits\AmazonS3Trait;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class ProjectController extends Controller
{

    use AmazonS3Trait;
    use HasTimestamps;

    protected $projectRepository;
    protected $thumbnailRepository;
    protected $categoryRepository;

    public function __construct(ProjectRepository $projectRepository, ThumbnailRepository $thumbnailRepository, CategoryRepository $categoryRepository) {
        $this->categoryRepository = $categoryRepository;
        $this->projectRepository = $projectRepository;
        $this->thumbnailRepository = $thumbnailRepository;
    }

    public function index()
    {
        return $this->projectRepository->lastestPaginate(10, request()->header('category_id'));
    }

    public function store(Request $request) {
        $credentials = $request->only('name', 'owner','location', 'status', 'design', 'construction', 'content');
        $rules = [
            'name' => 'required',
            'owner' => 'required',
            'location' => 'required',
            'status' => 'required',
            'design' => 'required',
            'construction' => 'required',
            'content' => 'required'
        ];

        $customMessages = (strcmp("vi", AppHelper::getLanguageRequest()) == 0) ? [
            'name.required' => 'Trường tên dự án là bắt buộc.',
            'name.unique' => 'Trường tên đã tồn tại trong hệ thống.',
            'owner.required' => 'Trường chủ đầu tư là bắt buộc.',
            'location.required' => 'Trường vị trí địa lý là bắt buộc.',
            'status.required' => 'Trường trạng thái là bắt buộc.',
            'design.required' => 'Trường đơn vị thiết kế là bắt buộc',
            'construction.required' => 'Trường đơn vị thi công là bắt buộc',
            'content.required' => 'Trường mô tả tổng quan dự án là bắt buộc'
        ] : [
            'name.required' => 'Project name field is required.',
            'name.unique' => 'Name field already exists in the system.',
            'owner.required' => 'Investor field is required.',
            'location.required' => 'Geolocation field is required.',
            'status.required' => 'Status field is required.',
            'design.required' => 'Design unit field is required.',
            'construction.required' => 'Construction unit field is required.',
            'content.required' => 'Project overview description field is required.'
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $request->merge([
                'name_vi' => (AppHelper::getLanguageRequest() == "vi") ? $request->name : "",
                'name_en' => (AppHelper::getLanguageRequest() == "en") ? $request->name : "",
                'owner_vi' => (AppHelper::getLanguageRequest() == "vi") ? $request->owner : "",
                'owner_en' => (AppHelper::getLanguageRequest() == "en") ? $request->owner : "",
                'description_vi' => (AppHelper::getLanguageRequest() == "vi") ? $request->description : "",
                'description_en' => (AppHelper::getLanguageRequest() == "en") ? $request->description : "",
                'title_vi' => (AppHelper::getLanguageRequest() == "vi") ? $request->title : "",
                'title_en' => (AppHelper::getLanguageRequest() == "en") ? $request->title : "",
                'timeline' => Carbon::parse($request->timeline),
                'is_home' => $request->is_home ? 1 : 0,
                'location_vi' => (AppHelper::getLanguageRequest() == "vi") ? $request->location : "",
                'location_en' => (AppHelper::getLanguageRequest() == "en") ? $request->location : "",
                'status_vi' => (AppHelper::getLanguageRequest() == "vi") ? $request->status : "",
                'status_en' => (AppHelper::getLanguageRequest() == "en") ? $request->status : "",
                'category_id' => $request->type,
                'design_vi' => (AppHelper::getLanguageRequest() == "vi") ? $request->design : "",
                'design_en' => (AppHelper::getLanguageRequest() == "en") ? $request->design : "",
                'construction_vi' => (AppHelper::getLanguageRequest() == "vi") ? $request->construction : "",
                'construction_en' => (AppHelper::getLanguageRequest() == "en") ? $request->construction : "",
                'content_vi' => (AppHelper::getLanguageRequest() == "vi") ? $request->input('content') : "",
                'content_en' => (AppHelper::getLanguageRequest() == "en") ? $request->input('content') : "",
                'user_id' => Auth::user()->id,
                'lang' => AppHelper::getLanguageRequest(),
                'created_at' => $this->freshTimestamp(),
                'updated_at' => $this->freshTimestamp()
            ]);
            $this->projectRepository->create($request->all());
            return ([
                'error' => [
                    'code' => 0,
                    'message' => "Success"
                ]
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return false|JsonResponse|string
     */
    public function update(Request $request, $id)
    {
        $project = $this->projectRepository->findByField('id', $request->id, ['*']);
        if (null != $project) {
            $credentials = $request->only('name', 'owner','location', 'status', 'design', 'construction', 'content');
            if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                $rules = [
                    'name' => 'required|unique:projects,name_vi,'.$id,
                    'owner' => 'required',
                    'location' => 'required',
                    'status' => 'required',
                    'design' => 'required',
                    'construction' => 'required',
                    'content' => 'required'
                ];
            } else {
                $rules = [
                    'name' => 'required|unique:projects,name_en,'.$id,
                    'owner' => 'required',
                    'location' => 'required',
                    'status' => 'required',
                    'design' => 'required',
                    'construction' => 'required',
                    'content' => 'required'
                ];
            }
            $customMessages = [
                'name.required' => 'Trường tên dự án là bắt buộc.',
                'name.unique' => 'Trường tên đã tồn tại trong hệ thống.',
                'owner.required' => 'Trường chủ đầu tư là bắt buộc.',
                'location.required' => 'Trường vị trí địa lý là bắt buộc.',
                'status.required' => 'Trường trạng thái là bắt buộc.',
                'design.required' => 'Trường đơn vị thiết kế là bắt buộc',
                'construction.required' => 'Trường đơn vị thi công là bắt buộc',
                'content.required' => 'Trường mô tả tổng quan dự án là bắt buộc'
            ];
            $validator = Validator::make($credentials, $rules, $customMessages);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            } else {
                $request->merge([
                    'name_vi' => ($request->lang == "vi") ? $request->name : ((sizeof($project) > 0) ? $project[0]['name_vi'] : ""),
                    'name_en' => ($request->lang == "en") ? $request->name : ((sizeof($project) > 0) ? $project[0]['name_en'] : ""),
                    'owner_vi' => ($request->lang == "vi") ? $request->owner : ((sizeof($project) > 0) ? $project[0]['owner_vi'] : ""),
                    'owner_en' => ($request->lang == "en") ? $request->owner : ((sizeof($project) > 0) ? $project[0]['owner_en'] : ""),
                    'description_vi' => ($request->lang == "vi") ? $request->description : ((sizeof($project) > 0) ? $project[0]['description_vi'] : ""),
                    'description_en' => ($request->lang == "en") ? $request->description : ((sizeof($project) > 0) ? $project[0]['description_en'] : ""),
                    'title_vi' => ($request->lang == "vi") ? $request->title : ((sizeof($project) > 0) ? $project[0]['title_vi'] : ""),
                    'title_en' => ($request->lang == "en") ? $request->title : ((sizeof($project) > 0) ? $project[0]['title_en'] : ""),
                    'location_vi' => ($request->lang == "vi") ? $request->location : ((sizeof($project) > 0) ? $project[0]['location_vi'] : ""),
                    'location_en' => ($request->lang == "en") ? $request->location : ((sizeof($project) > 0) ? $project[0]['location_en'] : ""),
                    'status_vi' => ($request->lang == "vi") ? $request->status : ((sizeof($project) > 0) ? $project[0]['status_vi'] : ""),
                    'status_en' => ($request->lang == "en") ? $request->status : ((sizeof($project) > 0) ? $project[0]['status_en'] : ""),
                    'design_vi' => ($request->lang == "vi") ? $request->design : ((sizeof($project) > 0) ? $project[0]['design_vi'] : ""),
                    'design_en' => ($request->lang == "en") ? $request->design : ((sizeof($project) > 0) ? $project[0]['design_en'] : ""),
                    'construction_vi' => ($request->lang == "vi") ? $request->construction : ((sizeof($project) > 0) ? $project[0]['construction_vi'] : ""),
                    'construction_en' => ($request->lang == "en") ? $request->construction : ((sizeof($project) > 0) ? $project[0]['construction_en'] : ""),
                    'content_vi' => ($request->lang == "vi") ? $request->input('content') : ((sizeof($project) > 0) ? $project[0]['content_vi'] : ""),
                    'content_en' => ($request->lang == "en") ? $request->input('content') : ((sizeof($project) > 0) ? $project[0]['content_en'] : ""),
                    'timeline' => Carbon::parse($request->timeline),
                    'category_id' => $request->type,
                    'user_id' => Auth::user()->id,
                    'is_home' => $request->is_home ? 1 : 0,
                    'created_at' => $this->freshTimestamp(),
                    'updated_at' => $this->freshTimestamp()
                ]);
                $this->projectRepository->update($request->all(), $id);
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "Project updated"
                    ],
                    'id' => $id
                ]));
            }
        } else {
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Project updated failure"
                ]
            ]));
        }
    }

    public function show($id) {
        $project = $this->projectRepository->findByField('id', $id);
        if (!is_null($project) && sizeof($project) > 0) {
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Project has existed in our system"
                ],
                'project' => $project
            ]));
        }
        return json_encode(([
            'message' => [
                'status' => "error",
                'description' => "Project has existed in our system"
            ]
        ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return false|Response|string
     */
    public function destroy($id)
    {
        $project = $this->projectRepository->findByField('id', $id, ['*']);
        if (!is_null($project) && sizeof($project) > 0) {
            $thumbnail = $this->thumbnailRepository->findByField('project_id', $project[0]['id']);
            if (!is_null($thumbnail) && sizeof($thumbnail) > 0) {
                $this->thumbnailRepository->delete($thumbnail[0]['id']);
            }
            $this->projectRepository->delete($project[0]['id']);
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "User deleted"
                ],
                'id' => $id
            ]));
        } else {
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => "User delete failure",
                ]
            ]));
        }
    }

    protected function getCategoryParentId($path) {
        switch (preg_split('~/~', $path)[2]) {
            case 'architecture':
                return 1;
                break;
            case 'furniture':
                return 2;
                break;
        }
        return 1;
    }
}
