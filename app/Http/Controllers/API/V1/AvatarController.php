<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Profile\ProfileRepository;
use App\Repositories\User\UserRepository;
use App\Traits\AmazonS3Trait;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AvatarController extends Controller
{

    use AmazonS3Trait;
    use HasTimestamps;

    protected $profileRepository;
    protected $userRepository;

    public function __construct(ProfileRepository $profileRepository, UserRepository $userRepository) {
        $this->userRepository = $userRepository;
        $this->profileRepository = $profileRepository;
    }

    public function store(Request $request) {
        $user = $this->userRepository->findByField('id', Auth::user()->id);
        if (!is_null($user) && sizeof($user) > 0 && !empty($user[0]['avatar'])) {
            $backup = $user[0]['avatar'];
        } else {
            $backup = url('/').'/images/user.webp';
        }
        $this->saveAvatar($request, $backup);
        $this->profileRepository->update(array(
            'user_id' => Auth::user()->id,
            'avatar' => $request->avatar,
            'created_at' => $this->freshTimestamp(),
            'updated_at' => $this->freshTimestamp()
        ), $request->id);
        $this->userRepository->update(array(
            'avatar' => $request->avatar
        ), Auth::user()->id);
        return json_encode([
            'result' => true,
            'avatar' => $request->avatar
        ]);
    }
}
