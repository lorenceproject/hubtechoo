<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Model\Permission;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Role\RoleRepository;
use App\Traits\FlagUtitlites;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;

class PermissionController extends Controller
{
    use FlagUtitlites;
    use HasTimestamps;

    protected $requestGuzzle;
    protected $repository;
    protected $roleRepository;

    public function __construct(PermissionRepository $permissionRepository, RoleRepository $roleRepository) {
        $this->requestGuzzle = new Client();
        $this->roleRepository = $roleRepository;
        $this->repository = $permissionRepository;
    }

    public function index()
    {
        if ($this->preventCrawlingData('security')) {
            if (strcmp('all', request()->header('type')) == 0) {
                return $this->repository->getAll();
            } else {
                $length = request()->input('length');
                $sortBy = request()->input('column');
                $orderBy = request()->input('dir');
                $searchValue = request()->input('search');

                $query = Permission::eloquentQuery($sortBy, $orderBy, $searchValue);

                $data = $query->paginate($length);

                return new DataTableCollectionResource($data);
            }
        } else {
            return response()->json(
                [
                    'error' => [
                        'code' => 401,
                        'message' => 'Token is not provided'
                    ],
                    'data' => null,
                ]
            );
        }
    }

    public function store(Request $request) {
        $credentials = $request->only('name', 'guard_name');

        $rules = [
            'name' => 'required',
            'guard_name' => 'required'
        ];

        $customMessages = (strcmp("vi", $request->lang) == 0) ? [
            'name.required' => 'Permission là bắt buộc.',
            'guard_name.required' => 'Thiết lập Guard là bắt buộc.'
        ] : [
            'name.required' => 'Permission name is required.',
            'guard_name.required' => 'Guard name is required.'
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        } else {
            $permission = $this->repository->findByField('name', $request->name);
            if (!is_null($permission) && sizeof($permission) > 0) {
                return json_encode(([
                    'message' => [
                        'status' => "error",
                        'description' => "The permission has been existed in our system"
                    ],
                    'role' => $permission[0]
                ]));
            } else {
                \Spatie\Permission\Models\Permission::create(
                    [
                        'name' => $request->name,
                        'guard_name' => $request->guard_name,
                        'created_at' => $this->freshTimestamp(),
                        'updated_at' => $this->freshTimestamp()
                    ]
                );
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "Add new permission successfully!"
                    ],
                    'permission' => $this->repository->findByField('name', $request->name)[0]
                ]));
            }
        }
    }

    public function update(Request $request, $id) {
        $credentials = $request->only('name', 'guard_name');

        $rules = [
            'name' => 'required',
            'guard_name' => 'required'
        ];

        $customMessages = (strcmp("vi", $request->lang) == 0) ? [
            'name.required' => 'Permission là bắt buộc.',
            'guard_name.required' => 'Thiết lập Guard là bắt buộc.'
        ] : [
            'name.required' => 'Permission name is required.',
            'guard_name.required' => 'Guard name is required.'
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        } else {
            $permission = $this->repository->findWhere([
                ['name','=',$request->name],
                ['id','<>',$id]
            ]);
            if (!is_null($permission) && sizeof($permission) > 0) {
                return json_encode(([
                    'message' => [
                        'status' => "error",
                        'description' => "The permission has been existed in our system"
                    ],
                    'permission' => $permission[0]
                ]));
            } else {
                $this->repository->update(array(
                    'name' => $request->name,
                    'guard_name' => $request->guard_name,
                    'created_at' => $this->freshTimestamp(),
                    'updated_at' => $this->freshTimestamp()
                ), $id);
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "Permission updated successfully"
                    ]
                ]));
            }
        }
    }

    public function roleassigntopermission(Request $request) {
        $credentials = $request->only('role_id');

        $rules = [
            'role_id' => 'regex:/^\d+$/'
        ];

        $customMessages = (strcmp("vi", $request->lang) == 0) ? [
            'role_id.regex' => 'Thực hiện tìm kiếm các vai trò trong hệ thống, và chọn vai trò để tiến hành thiết lập các quyền cho vai trò đó.'
        ] : [
            'role_id.regex' => 'Carry out of the search for the roles in the system, check the checkbox to get the role for assign suitable permissions to the role.'
        ];

        $validator = Validator::make($credentials, $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        } else {
            $role = $this->roleRepository->findByField('id', $request->role_id);
            if (!is_null($role) && sizeof($role) > 0 && !is_null($request->selections)) {
                // Reset and add new
                DB::table('role_has_permissions')
                    ->where('role_id', $request->role_id)->delete();

                $permission_ids = explode (",", $request->selections);
                DB::table('role_has_permissions')
                    ->where('role_id', $request->role_id)->delete();
                foreach ($permission_ids as $permission_id) {
                    $obj = DB::table('role_has_permissions')
                        ->where('role_id', '=', $request->role_id)
                        ->where('permission_id', '=', $permission_id)->first();
                    if ($obj === null) {
                        DB::table('role_has_permissions')->insert(
                            array(
                                'permission_id'     =>   $permission_id,
                                'role_id'   =>   $request->role_id
                            )
                        );
                    }
                }
            }
            if (null == $request->selections) {
                DB::table('role_has_permissions')
                    ->where('role_id', $request->role_id)->delete();
            }
        }
        return null;
    }

    public function destroy($id)
    {
        if ($this->repository->findByField('id', $id, ['*']) != null) {
            $this->repository->delete($id);
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Permission deleted"
                ],
                'id' => $id
            ]));
        } else {
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => "Permission delete failure"
                ]
            ]));
        }
    }

    public function show($permissionName) {
        $permission = $this->repository->findByField('name', $permissionName);
        if (!is_null($permission) && sizeof($permission) > 0) {
            $obj = DB::table('model_has_permissions')
                ->where('permission_id', '=', $permission[0]['id'])
                ->where('model_id', Auth::user()->id);
            if ($obj !== null) {
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "Permission Approved"
                    ],
                ]));
            } else {
                return json_encode(([
                    'message' => [
                        'status' => "rejected",
                        'description' => "Permission Denied"
                    ],
                ]));
            }
        }
        return json_encode(([
            'message' => [
                'status' => "rejected",
                'description' => "Permission Denied"
            ],
        ]));
    }
}
