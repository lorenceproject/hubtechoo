<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Contact\ContactRepository;
use App\Traits\FlagUtitlites;

class ContactController extends Controller
{

    use FlagUtitlites;

    protected $repository;

    public function __construct(ContactRepository $repository) {
        $this->repository = $repository;
    }

    public function index()
    {
        if ($this->preventCrawlingData('contact')) {
            return $this->repository->lastestPaginate(20);
        } else {
            return response()->json(
                [
                    'error' => [
                        'code' => 401,
                        'message' => 'Token is not provided'
                    ],
                    'data' => null,
                ]
            );
        }
    }

    public function destroy($id) {
        $this->repository->delete($id);
        return json_encode(([
            'message' => [
                'status' => "success",
                'description' => "Contact deleted"
            ],
            'id' => $id
        ]));
    }
}
