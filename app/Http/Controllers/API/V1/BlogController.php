<?php

namespace App\Http\Controllers\API\V1;

use App\Helpers\AppHelper;
use App\Repositories\BlogSeries\BlogSeriesRepository;
use App\Repositories\BlogTag\BlogTagRepository;
use App\Repositories\BlogThumbnail\BlogThumbnailRepository;
use App\Repositories\BlogView\BlogViewRepository;
use App\Repositories\Index\IndexRepository;
use App\Repositories\Tag\TagRepository;
use App\Traits\FlagUtitlites;
use Storage;
use App\Http\Controllers\Controller;
use App\Repositories\Blog\BlogRepository;
use App\Traits\AmazonS3Trait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{

    use AmazonS3Trait;
    use HasTimestamps;
    use FlagUtitlites;

    protected $blogRepository;
    protected $tagRepository;
    protected $blogViewRepository;
    protected $blogTagRepository;
    protected $indexRepository;
    protected $blogThumbnailRepository;
    protected $blogSeriesRepository;

    public function __construct(BlogSeriesRepository $blogSeriesRepository, IndexRepository $indexRepository, BlogRepository $blogRepository, BlogThumbnailRepository $blogThumbnailRepository,
                                BlogViewRepository $blogViewRepository, TagRepository $tagRepository, BlogTagRepository $blogTagRepository) {
        $this->tagRepository = $tagRepository;
        $this->indexRepository = $indexRepository;
        $this->blogRepository = $blogRepository;
        $this->blogTagRepository = $blogTagRepository;
        $this->blogViewRepository = $blogViewRepository;
        $this->blogSeriesRepository = $blogSeriesRepository;
        $this->blogThumbnailRepository = $blogThumbnailRepository;
    }

    public function index() {
        if (!empty(request()->header('type'))
            && strcmp('series', request()->header('type')) == 0) {
            if (!empty(request()->header('series_id'))) {
                return response()->json([
                    'blog_series' => $this->blogSeriesRepository->findRelative(request()->header('series_id')),
                    'all' => $this->blogRepository->getAll()
                ]);
            } else {
                return $this->blogRepository->getAll();
            }
        }
        if ($this->preventCrawlingData("blog")) {
            return $this->blogRepository->lastestPaginate(20);
        } else {
            return response()->json(
                [
                    'error' => [
                        'code' => 401,
                        'message' => 'Token is not provided'
                    ],
                    'data' => null,
                ]
            );

        }
    }

    public function store(Request $request) {
        if ($this->preventCrawlingData("create")) {
            $credentials = $request->only('title', 'description', 'content', 'category_id');

            $rules = [
                'title' => 'required',
                'description' => 'required',
                'content' => 'required',
                'category_id' => 'required'
            ];

            $customMessages = (strcmp("vi", $request->header('locale')) == 0) ? [
                'title.required' => 'Tiêu đề bài viết là bắt buộc.',
                'description.required' => 'Nội dung mô tả là bắt buộc.',
                'content.required' => 'Nội dung bài viết là bắt buộc.',
                'category_id.required' => 'Loại danh mục là bắt buộc.'
            ] : [
                'title.required' => 'Blog name is required.',
                'description.required' => 'Blog description is required.',
                'content.required' => 'Blog content is required.',
                'category_id.required' => 'Category type is required.'
            ];

            $validator = Validator::make($credentials, $rules, $customMessages);
            $time = $this->freshTimestamp();
            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            } else {
                $request->merge([
                    'slug_en' => ($request->lang == "en") ? $request->slug : "",
                    'slug_vi' => ($request->lang == "vi") ? $request->slug : "",
                    'title_en' => ($request->lang == "en") ? $request->title : "",
                    'title_vi' => ($request->lang == "vi") ? $request->title : "",
                    'description_en' => ($request->lang == "en") ? $request->description : "",
                    'description_vi' => ($request->lang == "vi") ? $request->description : "",
                    'content_en' => ($request->lang == "en") ? $request->input('content') : "",
                    'content_vi' => ($request->lang == "vi") ? $request->input('content') : "",
                    'date' => Carbon::parse($request->date),
                    'category_id' => $request->category_id,
                    'status' => $request->status ? 1 : 0,
                    'is_editable' => $request->is_editable ? 1 : 0,
                    'user_id' => Auth::user()->id,
                    'created_at' => $time,
                    'updated_at' => $time
                ]);
                $this->blogRepository->create($request->all());

                foreach ($request->tags as $tag) {

                    $item = $this->tagRepository->findByField('id', $tag['id']);
                    if (!is_null($item) && (sizeof($item) > 0)) {
                        // Exist tag and just update blogtag
                        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                            $existedTag = $this->tagRepository->findWhere([
                                'name_vi' => $tag['name_vi']
                            ]);
                        } else {
                            $existedTag = $this->tagRepository->findWhere([
                                'name_en' => $tag['name_en']
                            ]);
                        }

                        $existedBlogTag = $this->blogTagRepository->findWhere([
                            'tag_id' => $existedTag[0]['id'],
                            'blog_id' => self::getBlogId($time),
                        ]);

                        if (!is_null($existedBlogTag) && (sizeof($existedBlogTag) > 0)) {
                            // Exist link between tag and blog
                        } else {
                            $this->blogTagRepository->create(array(
                                'user_id' => Auth::user()->id,
                                'blog_id' => self::getBlogId($time),
                                'tag_id' => $existedTag[0]['id'],
                                'created_at' => $this->freshTimestamp(),
                                'updated_at' => $this->freshTimestamp()
                            ));
                        }

                    } else {
                        $newTag = $this->tagRepository->create(array(
                            'user_id' => Auth::user()->id,
                            'name_vi' => $tag['name_vi'],
                            'name_en' => $tag['name_en'],
                            'description_vi' => $tag['name_vi'],
                            'description_en' => $tag['name_en'],
                            'slug_vi' => $this->slugify($tag['name_vi']),
                            'slug_en' => $this->slugify($tag['name_en']),
                            'created_at' => $this->freshTimestamp(),
                            'updated_at' => $this->freshTimestamp()
                        ));

                        $newBlogTag = $this->blogTagRepository->findWhere([
                            'tag_id' => $newTag->id,
                            'blog_id' => self::getBlogId($time),
                        ]);

                        if (!is_null($newBlogTag) && (sizeof($newBlogTag) > 0)) {
                            // Exist link between tag and blog
                        } else {
                            $this->blogTagRepository->create(array(
                                'user_id' => Auth::user()->id,
                                'blog_id' => self::getBlogId($time),
                                'tag_id' => $newTag->id,
                                'created_at' => $this->freshTimestamp(),
                                'updated_at' => $this->freshTimestamp()
                            ));
                        }
                    }
                }

                return ([
                    'error' => [
                        'code' => 0,
                        'message' => "Success"
                    ]
                ]);
            }
        } else {
            return response()->json(
                [
                    'error' => [
                        'code' => 401,
                        'message' => 'Token is not provided'
                    ],
                    'data' => null,
                ]
            );
        }
    }

    protected function getBlogId($time) {
        $blogs = $this->blogRepository->findByField('created_at', $time);
        return $blogs[0]['id'];
    }

    public function update(Request $request)
    {
        if ($this->preventCrawlingData($request->id)) {
            self::handleIndexes($request->input('content'), $request->id, $request->lang);
            $blog = $this->blogRepository->findById('id', $request->id, ['*']);
            if (!is_null($blog) && sizeof($blog) > 0) {

                $credentials = $request->only('title', 'description', 'content');

                $rules = [
                    'title' => 'required',
                    'description' => 'required',
                    'content' => 'required'
                ];

                $customMessages = (strcmp("vi", $request->header('locale')) == 0) ? [
                    'title.required' => 'Tiêu đề bài viết là bắt buộc.',
                    'description.required' => 'Nội dung mô tả là bắt buộc.',
                    'content.required' => 'Nội dung bài viết là bắt buộc.'
                ] : [
                    'title.required' => 'Blog name is required.',
                    'description.required' => 'Blog description is required.',
                    'content.required' => 'Blog content is required.'
                ];

                $validator = Validator::make($credentials, $rules, $customMessages);

                if ($validator->fails()) {
                    return response()->json($validator->errors(), 422);
                } else {
                    $request->merge([
                        'slug_en' => ($request->lang == "en") ? $request->slug : ((!is_null($blog) && sizeof($blog) > 0) ? $blog[0]['slug_en'] : ""),
                        'slug_vi' => ($request->lang == "vi") ? $request->slug : ((!is_null($blog) && sizeof($blog) > 0) ? $blog[0]['slug_vi'] : ""),
                        'title_en' => ($request->lang == "en") ? $request->title : ((!is_null($blog) && sizeof($blog) > 0) ? $blog[0]['title_en'] : ""),
                        'title_vi' => ($request->lang == "vi") ? $request->title : ((!is_null($blog) && sizeof($blog) > 0) ? $blog[0]['title_vi'] : ""),
                        'description_en' => ($request->lang == "en") ? $request->description : ((!is_null($blog) && sizeof($blog) > 0) ? $blog[0]['description_en'] : ""),
                        'description_vi' => ($request->lang == "vi") ? $request->description : ((!is_null($blog) && sizeof($blog) > 0) ? $blog[0]['description_vi'] : ""),
                        'content_en' => ($request->lang == "en") ? $request->input('content') : ((!is_null($blog) && sizeof($blog) > 0) ? $blog[0]['content_en'] : ""),
                        'content_vi' => ($request->lang == "vi") ? $request->input('content') : ((!is_null($blog) && sizeof($blog) > 0) ? $blog[0]['content_vi'] : ""),
                        'date' => Carbon::parse($request->date),
                        'category_id' => $request->category_id,
                        'status' => $request->status ? 1 : 0,
                        'is_editable' => $request->is_editable ? 1 : 0,
                        'user_id' => Auth::user()->id,
                        'created_at' => $this->freshTimestamp(),
                        'updated_at' => $this->freshTimestamp()
                    ]);
                    $this->blogRepository->update($request->all(), $request->id);
                    // Delete tag_id from blog_id and conduct to add again. (For case: UPDATE)
                    $deleteBlogTag = $this->blogTagRepository->findWhere([
                        'blog_id' => $this->blogRepository->findWhere([
                            ['slug_vi','=',$request->slug_vi],
                            ['slug_en','=',$request->slug_en]
                        ])[0]['id']
                    ]);

                    foreach ($deleteBlogTag as $_delete) {
                        $this->blogTagRepository->delete($_delete['id']);
                    }

                    foreach ($request->tags as $tag) {

                        $item = $this->tagRepository->findByField('id', $tag['id']);
                        if (!is_null($item) && (sizeof($item) > 0)) {
                            // Exist tag and just update blogtag
                            if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                                $existedTag = $this->tagRepository->findWhere([
                                    'name_vi' => $tag['name_vi']
                                ]);
                            } else {
                                $existedTag = $this->tagRepository->findWhere([
                                    'name_en' => $tag['name_en']
                                ]);
                            }
                            $existedBlogTag = $this->blogTagRepository->findWhere([
                                'tag_id' => $existedTag[0]['id'],
                                'blog_id' => $this->blogRepository->findWhere([
                                    ['slug_vi','=',$request->slug_vi],
                                    ['slug_en','=',$request->slug_en]
                                ])[0]['id']
                            ]);

                            if (!is_null($existedBlogTag) && (sizeof($existedBlogTag) > 0)) {
                                // Exist link between tag and blog
                            } else {
                                $this->blogTagRepository->create(array(
                                    'user_id' => Auth::user()->id,
                                    'blog_id' => $this->blogRepository->findWhere([
                                        ['slug_vi','=',$request->slug_vi],
                                        ['slug_en','=',$request->slug_en]
                                    ])[0]['id'],
                                    'tag_id' => $existedTag[0]['id'],
                                    'created_at' => $this->freshTimestamp(),
                                    'updated_at' => $this->freshTimestamp()
                                ));
                            }

                        } else {
                            $newTag = $this->tagRepository->create(array(
                                'user_id' => Auth::user()->id,
                                'name_vi' => $tag['name_vi'],
                                'name_en' => $tag['name_en'],
                                'description_vi' => $tag['name_vi'],
                                'description_en' => $tag['name_en'],
                                'slug_vi' => $this->slugify($tag['name_vi']),
                                'slug_en' => $this->slugify($tag['name_en']),
                                'created_at' => $this->freshTimestamp(),
                                'updated_at' => $this->freshTimestamp()
                            ));

                            $newBlogTag = $this->blogTagRepository->findWhere([
                                'tag_id' => $newTag->id,
                                'blog_id' => $this->blogRepository->findWhere([
                                    ['slug_vi','=',$request->slug_vi],
                                    ['slug_en','=',$request->slug_en]
                                ])[0]['id'],
                            ]);

                            if (!is_null($newBlogTag) && (sizeof($newBlogTag) > 0)) {
                                // Exist link between tag and blog
                            } else {
                                $this->blogTagRepository->create(array(
                                    'user_id' => Auth::user()->id,
                                    'blog_id' => $this->blogRepository->findWhere([
                                        ['slug_vi','=',$request->slug_vi],
                                        ['slug_en','=',$request->slug_en]
                                    ])[0]['id'],
                                    'tag_id' => $newTag->id,
                                    'created_at' => $this->freshTimestamp(),
                                    'updated_at' => $this->freshTimestamp()
                                ));
                            }
                        }
                    }

                    return json_encode(([
                        'message' => [
                            'status' => "success",
                            'description' => "Blog updated"
                        ],
                        'id' => $request->id
                    ]));
                }
            }
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => "Blog updated failure"
                ]
            ]));
        } else {
            return response()->json(
                [
                    'error' => [
                        'code' => 401,
                        'message' => 'Token is not provided'
                    ],
                    'data' => null,
                ]
            );
        }
    }

    public function show($id) {
        if (!empty(request()->header('type'))) {
            if (is_numeric($id)) {
                return $this->blogRepository->findByField('id', $id);
            } else {
                if (strcmp('vi', request()->header('locale')) == 0) {
                    return $this->blogRepository->findWhere([
                        ['title_vi','LIKE','%' . request()->header('keyword') . '%']
                    ]);
                } else {
                    return $this->blogRepository->findWhere([
                        ['title_en','LIKE','%' . request()->header('keyword') . '%']
                    ]);
                }
            }
        }
        if ($this->preventCrawlingData($id)) {
            $blog = $this->blogRepository->findByField('id', $id);
            if (!is_null($blog) && sizeof($blog) > 0) {
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "Blog has existed in our system"
                    ],
                    'blog' => $blog
                ]));
            }
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => "Blog has existed in our system"
                ]
            ]));
        } else {
            return response()->json(
                [
                    'error' => [
                        'code' => 404,
                        'message' => 'Token is not provided'
                    ],
                    'data' => null,
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return false|\Illuminate\Http\Response|string
     */
    public function destroy($id)
    {
        if ($this->preventCrawlingData('blog')) {
            $blog = $this->blogRepository->findByField('id', $id, ['*']);
            if (!is_null($blog) && sizeof($blog) > 0) {

                $thumbnails = $this->blogThumbnailRepository->findByField('blog_id', $id);
                if (!is_null($thumbnails) && sizeof($thumbnails) > 0) {
                    foreach ($thumbnails as $thumbnail) {
                        $this->blogThumbnailRepository->delete($thumbnail['id']);

                        $pathinfo = pathinfo($thumbnail['thumbnail']);
                        Storage::disk('s3')->delete('/leearchitects/blog/'.$pathinfo['filename'].'.'.$pathinfo['extension']);
                    }
                }

                $blogtags = $this->blogTagRepository->findByField('blog_id', $id);
                if (!is_null($blogtags) && sizeof($blogtags) > 0) {
                    foreach ($blogtags as $blogtag) {
                        $this->blogTagRepository->delete($blogtag['id']);
                    }
                }

                $blog_views = $this->blogViewRepository->findByField('blog_id', $id);
                if (!is_null($blog_views) && sizeof($blog_views) > 0) {
                    foreach ($blog_views as $blog_view) {
                        $this->blogViewRepository->delete($blog_view['id']);
                    }
                }

                $this->blogRepository->delete($blog[0]['id']);
                return json_encode(([
                    'message' => [
                        'status' => "success",
                        'description' => "Blog deleted"
                    ],
                    'id' => $id
                ]));
            } else {
                return json_encode(([
                    'message' => [
                        'status' => "error",
                        'description' => "Blog delete failure",
                    ]
                ]));
            }
        } else {
            return response()->json(
                [
                    'error' => [
                        'code' => 401,
                        'message' => 'Token is not provided'
                    ],
                    'data' => null,
                ]
            );
        }
    }

    protected function handleIndexes($content, $blog_id, $lang) {
        $indexes = array();
        preg_match_all('/<h3><strong style=".*?">(.*?)<\/strong><\/h3>|<h3><strong>(.*?)<\/strong><\/h3>/',
            $content, $blogindexes, PREG_SET_ORDER);
        foreach ($blogindexes as $blogindex) {
            array_push($indexes, $blogindex[sizeof($blogindex)-1]);
        }
        foreach ($indexes as $_index) {
            $stringromannumerals = substr($_index, 0,  strpos($_index, '.'));
            $stringromannumerals = str_replace(' ', '', $stringromannumerals);
            $level = self::romanToInteger($stringromannumerals);
            if ($lang == "en") {
                $existedIndex = $this->indexRepository->findWhere([
                    'level' => $level,
                    'blog_id' => $blog_id
                ]);
                if (!is_null($existedIndex) && sizeof($existedIndex) > 0) {
                    $this->indexRepository->update(array(
                        'title_en' => $_index,
                        'level' => $level
                    ), $existedIndex[0]['id']);
                } else {
                    $this->indexRepository->create([
                        'title_en' => $_index,
                        'level' => $level,
                        'is_editable' => 1,
                        'status' => 1,
                        'blog_id' => $blog_id,
                        'created_at' => $this->freshTimestamp(),
                        'updated_at' => $this->freshTimestamp()
                    ]);
                }
            } else {
                $existedIndex = $this->indexRepository->findWhere([
                    'level' => $level,
                    'blog_id' => $blog_id
                ]);
                if (!is_null($existedIndex) && sizeof($existedIndex) > 0) {
                    $this->indexRepository->update(array(
                        'title_vi' => $_index,
                        'level' => $level
                    ), $existedIndex[0]['id']);
                } else {
                    $this->indexRepository->create([
                        'title_vi' => $_index,
                        'level' => $level,
                        'is_editable' => 1,
                        'status' => 1,
                        'blog_id' => $blog_id,
                        'created_at' => $this->freshTimestamp(),
                        'updated_at' => $this->freshTimestamp()
                    ]);
                }
            }
        }
    }

    protected function romanToInteger($key)
    {
        $romans = [
            'M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1,
        ];

        $roman = $key;
        $result = 0;

        foreach ($romans as $key => $value) {
            while (strpos($roman, $key) === 0) {
                $result += $value;
                $roman = substr($roman, strlen($key));
            }
        }
        return $result;
    }
}
