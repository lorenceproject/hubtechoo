<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Repositories\BlogThumbnail\BlogThumbnailRepository;
use App\Traits\AmazonS3Trait;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Http\Request;
use Storage;

class BlogImageController extends Controller
{

    use AmazonS3Trait;
    use HasTimestamps;

    protected $blogThumbnailRepository;

    public function __construct(BlogThumbnailRepository $blogThumbnailRepository) {
        $this->blogThumbnailRepository = $blogThumbnailRepository;
    }

    public function index()
    {
        return $this->blogThumbnailRepository->lastestPaginate(10, request()->header('blog_id'));
    }

    public function store(Request $request) {
        $this->saveBlog($request, 'file');
        $this->blogThumbnailRepository->create(array(
            'blog_id' => $request->blog_id,
            'thumbnail' => $request->image,
            'width' => getimagesize($request->image)[0],
            'height' => getimagesize($request->image)[1],
            'type' => self::getType(getimagesize($request->image)[0], getimagesize($request->image)[1]),
            'created_at' => $this->freshTimestamp(),
            'updated_at' => $this->freshTimestamp()
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $thumbnail = $this->blogThumbnailRepository->findByField('id', $id, ['*']);
        if (!is_null($thumbnail) && sizeof($thumbnail) > 0) {
            $pathinfo = pathinfo($thumbnail[0]['thumbnail']);
            Storage::disk(S3)->delete('/leearchitects/blog/'.$pathinfo['filename'].'.'.$pathinfo['extension']);
            $this->blogThumbnailRepository->delete($id);
            return json_encode(([
                'message' => [
                    'status' => "success",
                    'description' => "Blog Thumbnail deleted"
                ],
                'id' => $id
            ]));
        } else {
            return json_encode(([
                'message' => [
                    'status' => "error",
                    'description' => "Blog Thumbnail delete failure",
                ]
            ]));
        }
    }

    public function getType($width, $height) {
        if (208 == $width || 258 == $height) {
            return 1; // Thumbnail Vertical
        } else if (355 == $width || 237 == $height) {
            return 2; // Thumbnail Horizontal
        } else if (920 == $width || 614 == $height) {
            return 3; // Cover Image Blog
        } else if (600 == $width || 315 == $height) {
            return 4; // Facebook Minimum Size
        } else if (1200 == $width && 630 == $height) {
            return 5; // Facebook Recommended Size
        } else if (1200 == $width && 800 == $height) {
            return 6; // Twitter Recommended Size
        } else if (1110 == $width && 740 == $height) {
            return 7; // Cover Image Blog
        }
    }
}
