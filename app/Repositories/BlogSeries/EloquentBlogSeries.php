<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\BlogSeries;

use App\Helpers\AppHelper;
use Prettus\Repository\Eloquent\BaseRepository;

class EloquentBlogSeries extends BaseRepository implements BlogSeriesRepository {

    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\BlogSeries";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function lastestPaginate($num = 10) {
        return $this->model->latest()->paginate($num);
    }

    public function getAll()
    {
        return $this->model->get();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByField($field, $value = null, $columns = ['*']) {
        return $this->model->where($field, '=', $value)->get();
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }

    public function findWhere(array $where, $columns = ['*']) {
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($where);

        $model = $this->model->with(array('blog'))->get($columns);
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function findRelative($series_id) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['series_id', 'blog_id'];
        } else {
            $columns = ['series_id', 'blog_id'];
        }
        return $this->model
            ->select($columns)
            ->with(array('blog' => function($query) {
                if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                    $nested_columns = ['id', 'title_vi'];
                } else {
                    $nested_columns = ['id', 'title_en'];
                }
                $query
                    ->orderBy('view', 'asc')
                    ->select($nested_columns)
                    ->get();
                },
            ))
            ->where('series_id', '=', $series_id)
            ->get();
    }
}