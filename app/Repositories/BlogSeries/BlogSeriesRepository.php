<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:06
 */

namespace App\Repositories\BlogSeries;


interface BlogSeriesRepository
{
    public function getAll();

    public function create(array $attributes = []);

    public function lastestPaginate($num = 10);

    public function delete($id);

    public function findByField($field, $value = null, $columns = ['*']);

    public function update(array $attributes, $id);

    public function findWhere(array $where, $columns = ['*']);

    public function findRelative($series_id);
}