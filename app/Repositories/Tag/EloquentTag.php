<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\Tag;

use App\Helpers\AppHelper;
use Prettus\Repository\Eloquent\BaseRepository;

class EloquentTag extends BaseRepository implements TagRepository {

    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\Tag";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function getAll()
    {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'name_vi'];
        } else {
            $columns = ['id', 'name_en'];
        }
        return $this->model
            ->select($columns)
            ->get();
    }

    public function findSitemap() {
        return $this->model
            ->where('status','=', '1')
            ->get();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByField($field, $value = null, $columns = ['*']) {
        return parent::findByField($field, $value, $columns);
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }

    public function findWhere(array $where, $columns = ['*']) {
        return parent::findWhere($where, $columns);
    }

    public function lastestPaginate($num = 10) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'name_vi', 'slug_vi', 'description_vi', 'created_at', 'status', 'is_editable'];
        } else {
            $columns = ['id', 'name_en', 'slug_en', 'description_en', 'created_at', 'status', 'is_editable'];
        }
        return $this->model
            ->select($columns)
            ->latest()
            ->paginate($num);
    }

    public function search($where, $date, $num = 10) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'name_vi', 'slug_vi', 'description_vi', 'created_at', 'status', 'is_editable'];
        } else {
            $columns = ['id', 'name_en', 'slug_en', 'description_en', 'created_at', 'status', 'is_editable'];
        }
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($where);
        $model = $this->model
            ->select($columns)
            ->orderBy('created_at', $date)
            ->latest()
            ->paginate($num);
        $this->resetModel();
        return $this->parserResult($model);
    }
}
