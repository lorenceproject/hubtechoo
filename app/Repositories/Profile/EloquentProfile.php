<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\Profile;

use App\Helpers\AppHelper;
use Prettus\Repository\Eloquent\BaseRepository;

class EloquentProfile extends BaseRepository implements ProfileRepository {

    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\Profile";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function getAll()
    {
        return $this->model->with(array('role'))->all();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByField($field, $value = null, $columns = ['*']) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'user_id', 'name_vi', 'avatar', 'job_vi', 'status', 'is_editable', 'lang', 'address_vi', 'description_vi', 'phone_number', 'gender', 'date_of_birth'];
        } else {
            $columns = ['id', 'user_id', 'name_en', 'avatar', 'job_en', 'status', 'is_editable', 'lang', 'address_en', 'description_en', 'phone_number', 'gender', 'date_of_birth'];
        }
        return $this->model
            ->with(array('user' => function($query) {
                $query
                    ->select('id','username', 'email')
                    ->get();
            }))
            ->select($columns)
            ->where($field, '=', $value)->get($columns);
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }
}