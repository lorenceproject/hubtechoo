<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:06
 */

namespace App\Repositories\Project;


interface ProjectRepository
{
    public function getAll();

    public function getAllByType($id);

    public function getAllByCategoryID($category_id);

    public function lastestPaginate($num = 10, $category_id);

    public function findWhere(array $where, $columns = ['*']);

    public function latest($num = 4);

    public function getArticle($num = 6);

    public function getAllProject();

    public function getAllProjectByID($arrays_type);

    public function create(array $attributes = []);

    public function delete($id);

    public function findByField($field, $value = null, $columns = ['*']);

    public function update(array $attributes, $id);
}
