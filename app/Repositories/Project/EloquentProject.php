<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\Project;

use App\Helpers\AppHelper;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;

class EloquentProject extends BaseRepository implements ProjectRepository {

    protected $categoryRepository;

    public function __construct(Application $app, CategoryRepository $categoryRepository)
    {
        parent::__construct($app);
        $this->categoryRepository = $categoryRepository;
    }

    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\Project";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function findWhere(array $where, $columns = ['*']) {

        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'category_id', 'name_vi as name', 'owner_vi as owner', 'description_vi as description', 'title_vi as title', 'type', 'is_home'];
        } else {
            $columns = ['id', 'category_id', 'name_en as name', 'owner_en as owner', 'description_en as description', 'title_en as title', 'type', 'is_home'];
        }

        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($where);

        $model = $this
            ->model
            ->with(array('thumbnail' => function($query) {
                $query
                    ->where('width', '=', 920)
                    ->where('height', '=', 770)
                    ->get();
            } , 'category'))
            ->select($columns)
            ->get($columns);
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getAll()
    {
        return $this->model->with(array('category', 'thumbnail'))->get();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function lastestPaginate($num = 10, $category_id) {
        return $this->model->with(array('category'))->where('category_id', '=', $category_id)->latest()->paginate($num);
    }

    public function latest($num = 4) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'category_id', 'name_vi as name', 'owner_vi as owner', 'description_vi as description', 'title_vi as title', 'location_vi as location', 'type', 'is_home', 'timeline'];
        } else {
            $columns = ['id', 'category_id', 'name_en as name', 'owner_en as owner', 'description_en as description', 'title_en as title', 'location_en as location', 'type', 'is_home', 'timeline'];
        }
        return $this->model->with(array('thumbnail' => function($query) {
            $query
                ->where('width', '=', 1304)
                ->where('height', '=', 868)
                ->get();
        }))
            ->where('is_home', '=', 1)->latest()
            ->limit($num)
            ->get($columns);
    }

    public function getArticle($num = 6) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'category_id', 'name_vi as name', 'owner_vi as owner', 'description_vi as description', 'title_vi as title', 'location_vi as location', 'type', 'is_home', 'timeline'];
        } else {
            $columns = ['id', 'category_id', 'name_en as name', 'owner_en as owner', 'description_en as description', 'title_en as title', 'location_en as location', 'type', 'is_home', 'timeline'];
        }
        return $this->model->with(array('thumbnail' => function($query) {
            $query
                ->where('width', '=', 362)
                ->where('height', '=', 167)
                ->get();
        }))
            ->limit($num)
            ->get($columns);
    }

    public function getAllProject() {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'category_id', 'name_vi as name', 'owner_vi as owner', 'description_vi as description', 'title_vi as title', 'location_vi as location', 'type', 'is_home', 'timeline'];
        } else {
            $columns = ['id', 'category_id', 'name_en as name', 'owner_en as owner', 'description_en as description', 'title_en as title', 'location_en as location', 'type', 'is_home', 'timeline'];
        }
        return $this->model->with(array('thumbnail' => function($query) {
            $query
                ->where('width', '=', 362)
                ->where('height', '=', 167)
                ->get();
        }))
            ->get($columns);
    }

    public function getAllProjectByID($arrays_type) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'category_id', 'name_vi as name', 'owner_vi as owner', 'description_vi as description', 'title_vi as title', 'location_vi as location', 'type', 'is_home', 'timeline'];
        } else {
            $columns = ['id', 'category_id', 'name_en as name', 'owner_en as owner', 'description_en as description', 'title_en as title', 'location_en as location', 'type', 'is_home', 'timeline'];
        }

        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with(array('thumbnail' => function($query) {
            $query
                ->where('width', '=', 362)
                ->where('height', '=', 167)
                ->get();
        }))
            ->whereIn('category_id', $arrays_type)
            ->get($columns);
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByField($field, $value = null, $columns = ['*']) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'category_id', 'name_vi as name', 'owner_vi as owner', 'description_vi as description', 'title_vi as title', 'location_vi as location', 'type', 'is_home', 'timeline'];
        } else {
            $columns = ['id', 'category_id', 'name_en as name', 'owner_en as owner', 'description_en as description', 'title_en as title', 'location_en as location', 'type', 'is_home', 'timeline'];
        }
        return $this->model->with(array('category', 'thumbnail'))->where($field, '=', $value)->get($columns);
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }

    public function getAllByType($id)
    {
        $categories = $this->categoryRepository->findWhere([
            ['level','=',2],
            ['parent_id','=',$id]
        ]);
        $stack = array();
        foreach ($categories as $category) {
            array_push($stack, $category['id']);
        }
        $categories_id = array($stack[0], $stack[1], $stack[2], $stack[3], $stack[4]);
        return $this->model->with(array('category', 'thumbnail'))
            ->whereIn('category_id', $categories_id)
            ->get();
    }

    public function getAllByCategoryID($category_id)
    {
        return $this->model->with(array('category', 'thumbnail'))
            ->where('category_id', $category_id)
            ->get();
    }
}
