<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\BlogTag;

use App\Helpers\AppHelper;
use Prettus\Repository\Eloquent\BaseRepository;

class EloquentBlogTag extends BaseRepository implements BlogTagRepository {

    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\BlogTag";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function lastestPaginate($num = 10) {
        return $this->model->latest()->paginate($num);
    }

    public function getAll()
    {
        return $this->model->get();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByField($field, $value = null, $columns = ['*']) {
        if (AppHelper::isAdminPage()) {
            return $this->model
                ->with(array('tag'))
                ->where($field, '=', $value)
                ->get();
        } else {
            if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                $columns = ['id', 'tag_id', 'blog_id'];
            } else {
                $columns = ['id', 'tag_id', 'blog_id'];
            }
            return $this->model
                ->with(array('tag' => function($query) {
                    if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                        $nested_columns = ['id', 'name_vi', 'slug_vi'];
                    } else {
                        $nested_columns = ['id', 'name_en', 'slug_en'];
                    }
                    $query
                        ->select($nested_columns)
                        ->get();
                }))
                ->where($field, '=', $value)
                ->get($columns);
        }
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }

    public function findWhere(array $where, $columns = ['*']) {
        return parent::findWhere($where, $columns);
    }
}