<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:06
 */

namespace App\Repositories\Blog;


interface BlogRepository
{
    public function getAll();

    public function findSitemap();

    public function lastest($num = 10, $_case);

    public function create(array $attributes = []);

    public function delete($id);

    public function latests($num = 4);

    public function findByArticle($field, $value = null, $columns = ['*']);

    public function findByField($field, $value = null, $columns = ['*']);

    public function findById($field, $value = null, $columns = ['*']);

    public function findByTag($field, $value = null, $columns = ['*']);

    public function getRelatedPost($category_id, $article_id);

    public function update(array $attributes, $id);

    public function findWhere(array $where, $columns = ['*']);

    public function lastestPaginate($num = 10);

    public function search($where, $date, $num = 10);

    public function paginateBlogByUserId($num = 10, $user_id);

    public function getArticlesByCategoryId($categoryID, $_case);
}
