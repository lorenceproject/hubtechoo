<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\Blog;

use App\Helpers\AppHelper;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;

class EloquentBlog extends BaseRepository implements BlogRepository {

    use HasTimestamps;
    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\Blog";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function latests($num = 4) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'user_id', 'title_vi as title', 'slug_vi as slug', 'category_id', 'description_vi as description', 'date'];
        } else {
            $columns = ['id', 'user_id', 'title_en as title', 'slug_en as slug', 'category_id', 'description_en as description', 'date'];
        }
        return $this->model->with(array('thumbnail' => function($query) {
            $query
                ->where('created_at','=', $this->freshTimestamp())
                ->get();
        }))
            ->limit($num)
            ->get($columns);
    }

    public function getAll()
    {
        if(strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'user_id', 'slug_vi', 'title_vi', 'description_vi'];
        } else {
            $columns = ['id', 'user_id', 'slug_en', 'title_en', 'description_en'];
        }
        return $this->model
            ->select($columns)
            ->with(array('thumbnail', 'user', 'blogview'))
            ->where('status','=', '1')
            ->get();
    }

    public function findSitemap() {
        return $this->model->with(array('thumbnail'))
            ->where('status','=', '1')
            ->get();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function lastest($num = 4, $_case)
    {
        if(strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'user_id', 'slug_vi', 'title_vi', 'description_vi'];
        } else {
            $columns = ['id', 'user_id', 'slug_en', 'title_en', 'description_en'];
        }
        if (strcmp('newest', $_case) == 0) {
            return $this->model
                ->latest()
                ->select($columns)
                ->with(array('thumbnail', 'user', 'blogview'))
                ->limit($num)
                ->get();
        } else {
            return $this->model
                ->select($columns)
                ->with(array('thumbnail', 'user', 'blogview'))
                ->orderBy('view', 'desc')
                ->limit($num)
                ->get();
        }
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByArticle($field, $value = null, $columns = ['*']) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'user_id', 'slug_vi', 'title_vi as title', 'description_vi as description', 'category_id', 'content_vi as content', 'created_at', 'updated_at', 'count'];
            return $this->model
                ->select($columns)
                ->with(array('thumbnail' => function($query) {
                    $query
                        ->select('id','blog_id', 'thumbnail', 'width', 'height')
                        ->where('width', '=', 920)
                        ->where('height', '=', 614)
                        ->get();
                }, 'user' => function($query) {
                    $query
                        ->select('id','name_vi', 'avatar')
                        ->get();
                }, 'blogview' => function($query) {
                    $query
                        ->where('created_at','=', $this->freshTimestamp())
                        ->get();
                }, 'video', 'series' => function($query) {
                    $query
                        ->where('created_at','=', $this->freshTimestamp())
                        ->get();
                }))
                ->where($field, $value)
                ->get($columns);
        } else {
            $columns = ['id', 'user_id', 'slug_en', 'title_en as title', 'description_en as description', 'category_id', 'content_en as content', 'created_at', 'updated_at', 'count'];
            return $this->model
                ->select($columns)
                ->with(array('thumbnail' => function($query) {
                    $query
                        ->select('id','blog_id', 'thumbnail', 'width', 'height')
                        ->where('width', '=', 920)
                        ->where('height', '=', 614)
                        ->get();
                }, 'user' => function($query) {
                    $query
                        ->select('id','name_en', 'avatar')
                        ->get();
                }, 'blogview' => function($query) {
                    $query
                        ->where('created_at','=', $this->freshTimestamp())
                        ->get();
                }, 'video', 'series' => function($query) {
                    $query
                        ->where('created_at','=', $this->freshTimestamp())
                        ->get();
                }))
                ->where($field, $value)
                ->get();
        }
    }

    public function findByField($field, $value = null, $columns = ['*']) {

        if (AppHelper::isAdminPage()) {
            if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                $columns = ['id', 'user_id', 'category_id', 'title_vi', 'slug_vi', 'description_vi', 'status', 'is_editable', 'content_vi', 'lang', 'date'];
            } else {
                $columns = ['id', 'user_id', 'category_id', 'title_en', 'slug_en', 'description_en', 'status', 'is_editable', 'content_en', 'lang', 'date'];
            }
            return $this->model
                ->with(array('thumbnail', 'user', 'blogview', 'video'))
                ->where($field, $value)
                ->get($columns);
        } else {
            if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                $columns = ['id', 'user_id', 'slug_vi', 'title_vi', 'description_vi', 'category_id', 'content_vi', 'created_at', 'updated_at', 'count'];
                return $this->model
                    ->select($columns)
                    ->with(array('thumbnail' => function($query) {
                        $query
                            ->select('id','blog_id', 'thumbnail', 'width', 'height')
                            ->where('width', '=', 920)
                            ->where('height', '=', 614)
                            ->get();
                    }, 'user' => function($query) {
                        $query
                            ->select('id','name_vi', 'avatar')
                            ->get();
                    }, 'blogview' => function($query) {
                        $query
                            ->where('created_at','=', $this->freshTimestamp())
                            ->get();
                    }, 'video', 'series' => function($query) {
                        $query
                            ->where('created_at','=', $this->freshTimestamp())
                            ->get();
                    }))
                    ->where($field, $value)
                    ->get($columns);
            } else {
                $columns = ['id', 'user_id', 'slug_en', 'title_en', 'description_en', 'category_id', 'content_en', 'created_at', 'updated_at', 'count'];
                return $this->model
                    ->select($columns)
                    ->with(array('thumbnail' => function($query) {
                        $query
                            ->select('id','blog_id', 'thumbnail', 'width', 'height')
                            ->where('width', '=', 920)
                            ->where('height', '=', 614)
                            ->get();
                    }, 'user' => function($query) {
                        $query
                            ->select('id','name_en', 'avatar')
                            ->get();
                    }, 'blogview' => function($query) {
                        $query
                            ->where('created_at','=', $this->freshTimestamp())
                            ->get();
                    }, 'video', 'series' => function($query) {
                        $query
                            ->where('created_at','=', $this->freshTimestamp())
                            ->get();
                    }))
                    ->where($field, $value)
                    ->get();
            }
        }
    }

    public function findById($field, $value = null, $columns = ['*']) {
        return parent::findByField($field, $value, $columns);
    }

    public function findByTag($field, $value = null, $columns = ['*']) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'user_id', 'category_id', 'title_vi', 'slug_vi', 'description_vi'];
        } else {
            $columns = ['id', 'user_id', 'category_id', 'title_en', 'slug_en', 'description_en'];
        }
        return $this->model
            ->with(array('thumbnail', 'user',
                'blogview' => function($query) {
                    $query
                        ->where('created_at','=', $this->freshTimestamp())
                        ->get();
                },
                'video', 'series' => function($query) {
                    $query
                        ->where('created_at','=', $this->freshTimestamp())
                        ->get();
            }))
            ->where($field, $value)
            ->get($columns);
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }

    public function findWhere(array $where, $columns = ['*']) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'title_vi'];
        } else {
            $columns = ['id', 'title_en'];
        }
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($where);

        $model = $this
            ->model
            ->select($columns)
            ->get($columns);
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function lastestPaginate($num = 10) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'title_vi', 'description_vi', 'status', 'is_editable', 'date'];
            return $this->model->with(array('thumbnail' => function($query) {
                    $query
                        ->select('id','blog_id', 'thumbnail', 'width', 'height')
                        ->get();
                }))
                ->select($columns)
                ->where('user_id','=', Auth::user()->id)
                ->latest()->paginate($num);
        } else {
            $columns = ['id', 'title_en', 'description_en', 'status', 'is_editable', 'date'];
            return $this->model->with(array('thumbnail' => function($query) {
                    $query
                        ->select('id','blog_id', 'thumbnail', 'width', 'height')
                        ->get();
                }))
                ->select($columns)
                ->where('user_id','=', Auth::user()->id)
                ->latest()->paginate($num);
        }
    }

    public function search($where, $date, $num = 10) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'title_vi', 'description_vi', 'status', 'is_editable', 'date'];
            $this->applyCriteria();
            $this->applyScope();

            $this->applyConditions($where);
            $model = $this->model->with(array('thumbnail' => function($query) {
                $query
                    ->select('id','blog_id', 'thumbnail', 'width', 'height')
                    ->get();
            }))
                ->select($columns)
                ->where('user_id','=', Auth::user()->id)
                ->orderBy('created_at', $date)
                ->latest()->paginate($num);
            $this->resetModel();
            return $this->parserResult($model);
        } else {
            $columns = ['id', 'title_en', 'description_en', 'status', 'is_editable', 'date'];
            $this->applyCriteria();
            $this->applyScope();

            $this->applyConditions($where);
            $model = $this->model->with(array('thumbnail' => function($query) {
                $query
                    ->select('id','blog_id', 'thumbnail', 'width', 'height')
                    ->get();
            }))
                ->select($columns)
                ->where('user_id','=', Auth::user()->id)
                ->orderBy('created_at', $date)
                ->latest()->paginate($num);
            $this->resetModel();
            return $this->parserResult($model);
        }
    }

    public function paginateBlogByUserId($num = 10, $user_id) {
        return $this->model->with(array('thumbnail'))
            ->where('user_id','=',$user_id)
            ->latest()->paginate($num);
    }

    public function getRelatedPost($category_id, $article_id)
    {
        return $this->model->with(array('thumbnail', 'user', 'blogview'))
            ->where('category_id','=', $category_id)
            ->where('id','!=', $article_id)
            ->limit(3)
            ->get();
    }

    public function getArticlesByCategoryId($categoryID, $_case)
    {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'user_id', 'slug_vi', 'title_vi', 'description_vi', 'count'];
        } else {
            $columns = ['id', 'user_id', 'slug_en', 'title_en', 'description_en', 'count'];
        }
        if (strcmp('newests', $_case) == 0) {
            return $this->model
                ->latest()
                ->select($columns)
                ->where('status','=', '1')
                ->where('category_id','=', $categoryID)
                ->with(array('thumbnail', 'user', 'blogview'))
                ->limit(4)
                ->get();
        } else if (strcmp('featureds', $_case) == 0) {
            return $this->model
                ->select($columns)
                ->where('status','=', '1')
                ->where('category_id','=', $categoryID)
                ->with(array('thumbnail', 'user', 'blogview'))
                ->orderBy('view', 'desc')
                ->limit(4)
                ->get();
        } else {
            return $this->model
                ->select($columns)
                ->where('status','=', '1')
                ->where('category_id','=', $categoryID)
                ->with(array('thumbnail', 'user', 'blogview'))
                ->orderBy('view', 'desc')
                ->get();
        }
    }
}
