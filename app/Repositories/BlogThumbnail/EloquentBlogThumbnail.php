<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\BlogThumbnail;

use Prettus\Repository\Eloquent\BaseRepository;

class EloquentBlogThumbnail extends BaseRepository implements BlogThumbnailRepository {

    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\BlogThumbnail";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function lastestPaginate($num = 10, $blog_id) {
        return $this->model->with(array('blog'))->where('blog_id', '=', $blog_id)->latest()->paginate($num);
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByField($field, $value = null, $columns = ['*']) {
        return parent::findByField($field, $value, $columns);
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }
}