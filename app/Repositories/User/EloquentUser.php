<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\User;

use App\Helpers\AppHelper;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;

class EloquentUser extends BaseRepository implements UserRepository {

    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\User";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function getAll()
    {
        return $this->model->with(array('profile', 'thumbnail'))->get();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function lastestPaginate($num, $_case) {
        if (strcmp('user', $_case) == 0) {
            return $this->model
                ->where('type_of_user', '=' ,'internal')
                ->where('email', '!=' , env('APP_EMAIL'))
                ->latest()->paginate($num);
        } else {
            return $this->model
                ->where('type_of_user', '=' ,'external')
                ->where('email', '!=' , env('APP_EMAIL'))
                ->latest()->paginate($num);
        }
    }

    public function search($where, $date, $num, $_case) {
        if (strcmp('user', $_case) == 0) {
            $this->applyCriteria();
            $this->applyScope();

            $this->applyConditions($where);
            $model = $this->model
                ->where('type_of_user', '=' ,'internal')
                ->where('email', '!=' , env('APP_EMAIL'))
                ->latest()->paginate($num);
            $this->resetModel();
            return $this->parserResult($model);
        } else {
            $this->applyCriteria();
            $this->applyScope();

            $this->applyConditions($where);
            $model = $this->model
                ->where('type_of_user', '=' ,'external')
                ->where('email', '!=' , env('APP_EMAIL'))
                ->latest()->paginate($num);
            $this->resetModel();
            return $this->parserResult($model);
        }
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByField($field, $value = null, $columns = ['*']) {
        return parent::findByField($field, $value, $columns);
    }

    public function findByUser($field, $value = null, $columns = ['*']) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'name_vi', 'avatar'];
        } else {
            $columns = ['id', 'name_en', 'avatar'];
        }
        return $this->model
            ->where($field, $value)
            ->get($columns);
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }

    public function findWhere(array $where, $columns = ['*']) {
        return parent::findWhere($where, $columns);
    }
}
