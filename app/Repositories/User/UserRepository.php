<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:06
 */

namespace App\Repositories\User;


interface UserRepository
{
    public function getAll();

    public function lastestPaginate($num, $_case);

    public function search($where, $date, $num, $_case);

    public function create(array $attributes = []);

    public function delete($id);

    public function findByField($field, $value = null, $columns = ['*']);

    public function findByUser($field, $value = null, $columns = ['*']);

    public function update(array $attributes, $id);

    public function findWhere(array $where, $columns = ['*']);
}
