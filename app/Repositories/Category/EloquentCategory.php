<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\Category;

use App\Helpers\AppHelper;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;

class EloquentCategory extends BaseRepository implements CategoryRepository {

    use HasTimestamps;
    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\Category";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function lastestPaginate($num = 10) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'name_vi', 'description_vi', 'slug_vi', 'lang', 'status', 'is_editable', 'level', 'created_at', 'parent_id'];
        } else {
            $columns = ['id', 'name_en', 'description_en', 'slug_en', 'lang', 'status', 'is_editable', 'level', 'created_at', 'parent_id'];
        }
        return $this->model
            ->where('user_id','=', Auth::user()->id)
            ->latest()
            ->select($columns)
            ->paginate($num);
    }

    public function search($where, $date, $num = 10) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'name_vi', 'description_vi', 'slug_vi', 'lang', 'status', 'is_editable', 'level', 'created_at', 'parent_id'];
        } else {
            $columns = ['id', 'name_en', 'description_en', 'slug_en', 'lang', 'status', 'is_editable', 'level', 'created_at', 'parent_id'];
        }
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($where);
        $model = $this->model
            ->where('user_id','=', Auth::user()->id)
            ->orderBy('created_at', $date)
            ->latest()
            ->select($columns)
            ->paginate($num);
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getAll()
    {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'name_vi', 'level'];
        } else {
            $columns = ['id', 'name_en', 'level'];
        }
        if (AppHelper::isAdminPage()) {
            return $this->model->with(array('thumbnail' => function($query) {
                $query
                    ->select('id','category_id', 'thumbnail', 'width', 'height')
                    ->where('created_at', '=', $this->freshTimestamp())
                    ->get();
            }))
                ->select($columns)
                ->where('status','=', '1')
                ->where('level','=', '1')
                ->get();
        } else {
            return $this->model->with(array('thumbnail' => function($query) {
                $query
                    ->select('id','category_id', 'thumbnail', 'width', 'height')
                    ->where('width','=',355)
                    ->where('height','=',237)
                    ->get();
            }))
                ->select($columns)
                ->where('status','=', '1')
                ->where('level','=', '1')
                ->get();
        }
    }

    public function getAllWithoutLevel() {
        return $this->model->with(array('thumbnail' => function($query) {
            $query
                ->select('id','category_id', 'thumbnail', 'width', 'height')
                ->where('created_at', '=', $this->freshTimestamp())
                ->get();
        }))
            ->where('status','=', '1')
            ->where('level','=', '2')
            ->get();
    }

    public function findSitemap() {
        return $this->model->with(array('thumbnail'))
            ->where('status','=', '1')
            ->get();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByField($field, $value = null, $columns = ['*']) {
        return parent::findByField($field, $value, $columns);
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }

    public function findWhere(array $where, $columns = ['*']) {
        if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
            $columns = ['id', 'name_vi'];
        } else {
            $columns = ['id', 'name_en'];
        }
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($where);
        $model = $this->model
            ->select($columns)
            ->with(array('thumbnail'  => function($query) {
            $query
                ->select('id','category_id', 'thumbnail', 'width', 'height')
                ->where('created_at', '=', $this->freshTimestamp())
                ->get();
        }))->get($columns);

        $this->resetModel();
        return $this->parserResult($model);
    }

    public function lastest($num = 4, $_case)
    {
        if (strcmp('newest', $_case) == 0) {
            if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                $columns = ['id', 'name_vi', 'slug_vi', 'description_vi', 'updated_at'];
            } else {
                $columns = ['id', 'name_en', 'slug_en', 'description_en', 'updated_at'];
            }
            return $this->model
                ->select($columns)
                ->with(array('thumbnail' => function($query) {
                    $query
                        ->select('id','category_id', 'thumbnail', 'width', 'height')
                        ->where('width','=',208)
                        ->where('height','=',258)
                        ->get();
                }))
                ->orderBy('updated_at', 'desc')
                ->limit($num)
                ->get();
        } else {
            return $this->model
                ->with(array('thumbnail'))
                ->orderBy('updated_at', 'desc')
                ->limit($num)
                ->get();
        }
    }

    public function findChildCategory($level, $parent_id)
    {
        return self::findWhere([
            'level' => $level,
            'parent_id' => $parent_id,
        ], ['*']);
    }
}
