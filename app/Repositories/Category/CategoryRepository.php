<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:06
 */

namespace App\Repositories\Category;


interface CategoryRepository
{
    public function getAll();

    public function getAllWithoutLevel();

    public function findSitemap();

    public function create(array $attributes = []);

    public function lastestPaginate($num = 10);

    public function lastest($num = 10, $_case);

    public function delete($id);

    public function search($where, $date, $num = 10);

    public function findByField($field, $value = null, $columns = ['*']);

    public function update(array $attributes, $id);

    public function findWhere(array $where, $columns = ['*']);

    public function findChildCategory($level, $parent_id);
}
