<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\CategoryThumbnail;

use Prettus\Repository\Eloquent\BaseRepository;

class EloquentCategoryThumbnail extends BaseRepository implements CategoryThumbnailRepository {

    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\CategoryThumbnail";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function lastestPaginate($num = 10, $category_id) {
        return $this->model->with(array('category'))->where('category_id', '=', $category_id)->latest()->paginate($num);
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByField($field, $value = null, $columns = ['*']) {
        return parent::findByField($field, $value, $columns);
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }
}