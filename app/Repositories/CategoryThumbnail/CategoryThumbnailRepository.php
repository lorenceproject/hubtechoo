<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:06
 */

namespace App\Repositories\CategoryThumbnail;


interface CategoryThumbnailRepository
{
    public function getAll();

    public function lastestPaginate($num = 10, $blog_id);

    public function create(array $attributes = []);

    public function delete($id);

    public function findByField($field, $value = null, $columns = ['*']);

    public function update(array $attributes, $id);
}