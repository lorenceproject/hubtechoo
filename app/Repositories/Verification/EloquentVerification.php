<?php
/**
 * Created by PhpStorm.
 * User: lorence
 * Date: 31/12/2019
 * Time: 11:09
 */

namespace App\Repositories\Verification;

use Prettus\Repository\Eloquent\BaseRepository;

class EloquentVerification extends BaseRepository implements VerificationRepository {

    /* MODEL
     * @return string
     */
    public function model()
    {
        return "App\\Model\\Verification";
    }

    public function with($relations)
    {
        return parent::with($relations);
    }

    public function lastestPaginate($num = 10) {
        return $this->model->latest()->paginate($num);
    }

    public function getAll()
    {
        return $this->model->get();
    }

    public function create(array $attributes = [])
    {
        return parent::create($attributes);
    }

    public function delete($id) {
        return parent::delete($id);
    }

    public function findByField($field, $value = null, $columns = ['*']) {
        return $this->model->where($field, '=', $value)->get();
    }

    public function update(array $attributes, $id) {
        return parent::update($attributes, $id);
    }

    public function findWhere(array $where, $columns = ['*']) {
        return parent::findWhere($where, $columns);
    }
}