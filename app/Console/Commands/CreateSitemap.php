<?php

namespace App\Console\Commands;

use App\Repositories\Blog\BlogRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Tag\TagRepository;
use Carbon\Carbon;
use File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class CreateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export file XML automatically and submit to Google search console';

    /**
     * PageRepository
     */
    protected $categoryRepository;
    protected $blogRepository;
    protected $tagRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CategoryRepository $categoryRepository, BlogRepository $blogRepository, TagRepository $tagRepository)
    {
        parent::__construct();
        $this->blogRepository = $blogRepository;
        $this->tagRepository = $tagRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitemap = App::make('sitemap');

        // Common
        $sitemap->add(URL::to('/'), Carbon::now(), 1, 'daily');
        $sitemap->add(URL::to('/home'), Carbon::now(), 1, 'daily');
        $sitemap->add(URL::to('/categories'), Carbon::now(), 1, 'daily');
        $sitemap->add(URL::to('/author'), Carbon::now(), 1, 'daily');

        // Category
        $categories = $this->categoryRepository->findSitemap();
        foreach ($categories as $category) {
            $images = array();
            foreach ($category->thumbnail as $image) {
                $images[] = array(
                    'url' => $image->thumbnail
                );
            }
            $sitemap->add(URL::to('/').'/category/'.$category->slug_vi, $category->updated_at, 1, 'daily', $images);
            $sitemap->add(URL::to('/').'/category/'.$category->slug_en, $category->updated_at, 1, 'daily', $images);
        }

        // Blog
        $blogs = $this->blogRepository->findSitemap();
        foreach ($blogs as $blog) {
            $images = array();
            foreach ($blog->thumbnail as $image) {
                $images[] = array(
                    'url' => $image->thumbnail
                );
            }
            $sitemap->add(URL::to('/').'/post/'.$blog->slug_vi, $blog->updated_at, 1, 'daily', $images);
            $sitemap->add(URL::to('/').'/post/'.$blog->slug_en, $blog->updated_at, 1, 'daily', $images);
        }

        // Tag
        $tags = $this->tagRepository->findSitemap();
        foreach ($tags as $tag) {
            $sitemap->add(URL::to('/').'/tag/'.$tag->slug_vi, $tag->updated_at, 1, 'daily', null);
            $sitemap->add(URL::to('/').'/tag/'.$tag->slug_en, $tag->updated_at, 1, 'daily', null);
        }

        $sitemap->store('xml', 'sitemap');
        if (File::exists(public_path('sitemap.xml'))) {
            chmod(public_path('sitemap.xml'), 0777);
        }
        return true;
    }
}




