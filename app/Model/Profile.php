<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $table = "profiles";

    protected $fillable = [
        'id', 'user_id', 'name_en', 'name_vi', 'avatar', 'job_en', 'job_vi', 'status', 'is_editable', 'lang', 'address_en', 'address_vi', 'description_en', 'description_vi', 'phone_number', 'gender', 'date_of_birth', 'is_premium', 'date_to', 'date_from', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }
}
