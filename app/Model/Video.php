<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

class Video extends Model
{
    use Notifiable, LaravelVueDatatableTrait;

    protected $table = "videos";

    protected $fillable = [
        'id', 'blog_id','name_en', 'name_vi', 'description_en', 'user_id',
        'description_vi', 'href_en', 'href_vi', 'view', 'comments', 'like',
        'dislike', 'published', 'status', 'is_editable', 'lang', 'created_at', 'updated_at'
    ];

    protected $dataTableColumns = [
        'id' => [
            'searchable' => false,
        ],
        'blog_id' => [
            'searchable' => false,
        ],
        'user_id' => [
            'searchable' => false,
        ],
        'name_vi' => [
            'searchable' => true,
        ],
        'name_en' => [
            'searchable' => true,
        ],
        'href_vi' => [
            'searchable' => true,
        ],
        'href_en' => [
            'searchable' => true,
        ],
        'description_vi' => [
            'searchable' => false,
        ],
        'description_en' => [
            'searchable' => false,
        ],
        'view' => [
            'searchable' => true,
        ],
        'status' => [
            'searchable' => false,
        ],
        'is_editable' => [
            'searchable' => false,
        ],
        'comments' => [
            'searchable' => true,
        ],
        'published' => [
            'searchable' => true,
        ],
        'created_at' => [
            'searchable' => true,
        ]
    ];
}
