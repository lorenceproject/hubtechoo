<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    protected $table = "projects";

    protected $fillable = [
        'id', 'category_id', 'parent_id', 'name_vi', 'name_en', 'owner_vi', 'owner_en', 'description_vi' , 'description_en', 'title_vi' ,'title_en', 'type', 'is_home', 'location_vi' , 'location_en', 'status_vi', 'status_en', 'timeline', 'design_vi', 'design_en', 'construction_vi', 'construction_en', 'content_vi', 'content_en', 'lang', 'created_at', 'updated_at'
    ];

    public function category()
    {
        return $this->belongsTo('App\Model\Category', 'category_id', 'id');
    }

    public function thumbnail() {
        return $this->hasMany('App\Model\Thumbnail', 'project_id', 'id');
    }
}
