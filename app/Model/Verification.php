<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{

    protected $table = "verifications";

    protected $fillable = [
        'id', 'user_id', 'token', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }
}
