<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = "categories";

    protected $fillable = [
        'id', 'user_id', 'name_en', 'name_vi', 'description_en', 'description_vi', 'parent_id', 'level', 'status', 'is_editable', 'lang', 'slug_vi', 'slug_en', 'created_at', 'updated_at'
    ];

    public function thumbnail() {
        return $this->hasMany('App\Model\CategoryThumbnail', 'category_id', 'id');
    }
}
