<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

class Role extends Model
{
    use Notifiable, LaravelVueDatatableTrait;

    protected $table = "roles";

    protected $fillable = [
        'id', 'name', 'guard_name', 'created_at', 'updated_at'
    ];

    protected $dataTableColumns = [
        'id' => [
            'searchable' => false,
        ],
        'name' => [
            'searchable' => true,
        ],
        'guard_name' => [
            'searchable' => true,
        ],
        'created_at' => [
            'searchable' => true,
        ]
    ];
}
