<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{

    protected $table = "blog_tags";

    protected $fillable = [
        'id', 'tag_id', 'blog_id', 'user_id', 'created_at', 'updated_at'
    ];

    public function tag()
    {
        return $this->belongsTo('App\Model\Tag', 'tag_id', 'id');
    }
}
