<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Index extends Model
{

    protected $table = "indexes";

    protected $fillable = [
        'id', 'title_vi', 'title_en', 'href', 'level', 'status', 'is_editable', 'blog_id', 'created_at', 'updated_at'
    ];

    public function blog()
    {
        return $this->belongsTo('App\Model\Blog', 'blog_id', 'id');
    }
}
