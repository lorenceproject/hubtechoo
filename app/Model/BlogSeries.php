<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BlogSeries extends Model
{

    protected $table = "blog_series";

    protected $fillable = [
        'id', 'user_id', 'blog_id', 'series_id', 'level', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }

    public function blog()
    {
        return $this->belongsTo('App\Model\Blog', 'blog_id', 'id');
    }

    public function series()
    {
        return $this->belongsTo('App\Model\Series', 'series_id', 'id');
    }

}
