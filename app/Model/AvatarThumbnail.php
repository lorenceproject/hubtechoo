<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AvatarThumbnail extends Model
{

    protected $table = "avatar_thumbnails";

    protected $fillable = [
        'id', 'user_id', 'thumbnail', 'width', 'height', 'type', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }
}
