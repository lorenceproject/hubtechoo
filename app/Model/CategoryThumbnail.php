<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryThumbnail extends Model
{

    protected $table = "category_thumbnails";

    protected $fillable = [
        'id', 'category_id', 'thumbnail', 'width', 'height', 'type', 'created_at', 'updated_at'
    ];

    public function category()
    {
        return $this->belongsTo('App\Model\Category', 'category_id', 'id');
    }
}
