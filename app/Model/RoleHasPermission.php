<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class RoleHasPermission extends Model
{
    use Notifiable;

    protected $table = "role_has_permissions";

    protected $fillable = [
        'permission_id', 'role_id'
    ];
}
