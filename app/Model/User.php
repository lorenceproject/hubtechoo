<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'username', 'name_en', 'name_vi', 'email', 'avatar', 'email_verified_at', 'password', 'remember_token', 'expires_in', 'is_migration', 'type_of_user','status', 'is_editable', 'lang', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = array('allPermissions');

    public function getAllPermissionsAttribute()
    {
        $permissions = [];
        if (Auth::check()) {
            foreach (Permission::all() as $permission) {
                if (Auth::user()->hasPermissionTo($permission->name)) {
                    $permissions[] = $permission->name;
                }
            }
        }
        if (Auth::check()) {
            $temps = Auth::user()->getPermissionsViaRoles();
            foreach ($temps as $permission) {
                $permissions[] = $permission->name;
            }
        }
        return array_unique($permissions);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function role()
    {
        return $this->belongsTo('App\Model\Role', 'role_id', 'id');
    }

    public function profile() {
        return $this->hasMany('App\Model\Profile', 'user_id', 'id');
    }

    public function verification() {
        return $this->hasMany('App\Model\Verification', 'user_id', 'id');
    }

    public function reply() {
        return $this->hasMany('App\Model\Reply', 'user_id', 'id');
    }

    public function thumbnail() {
        return $this->hasMany('App\Model\AvatarThumbnail', 'user_id', 'id');
    }

    public function socialite() {
        return $this->hasOne('App\Model\Socialites', 'user_id', 'id');
    }
}
