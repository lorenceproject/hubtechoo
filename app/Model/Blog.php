<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $table = "blogs";
    protected $hidden = array();

    protected $fillable = [
        'id', 'user_id', 'title_en', 'title_vi', 'slug_vi', 'slug_en', 'category_id', 'description_en', 'description_vi', 'content_en', 'content_vi', 'status', 'is_editable', 'lang', 'view', 'count', 'date', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function series()
    {
        return $this->hasMany(BlogSeries::class, 'blog_id');
    }

    public function video() {
        return $this->hasOne(Video::class, 'blog_id');
    }

    public function thumbnail() {
        return $this->hasMany('App\Model\BlogThumbnail', 'blog_id', 'id');
    }

    public function blogview() {
        return $this->hasMany('App\Model\BlogView', 'blog_id', 'id');
    }
}
