<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $table = "tags";

    protected $fillable = [
        'id', 'user_id', 'name_en', 'name_vi', 'description_en', 'description_vi', 'status', 'is_editable', 'lang', 'slug_vi', 'slug_en', 'created_at', 'updated_at'
    ];
}
