<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Thumbnail extends Model
{

    protected $table = "thumbnails";

    protected $fillable = [
        'id', 'project_id', 'thumbnail', 'width', 'height', 'type', 'created_at', 'updated_at'
    ];

    public function project()
    {
        return $this->belongsTo('App\Model\Project', 'project_id', 'id');
    }
}
