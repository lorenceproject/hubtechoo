<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BlogThumbnail extends Model
{

    protected $table = "blog_thumbnails";

    protected $fillable = [
        'id', 'blog_id', 'thumbnail', 'width', 'height', 'type', 'created_at', 'updated_at'
    ];

    public function blog()
    {
        return $this->belongsTo('App\Model\Blog', 'blog_id', 'id');
    }
}
