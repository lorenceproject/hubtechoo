<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BlogView extends Model
{

    protected $table = "blog_views";

    protected $fillable = [
        'id', 'blog_id', 'slug', 'url', 'session_id', 'user_id', 'ip', 'agent', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }

    public function blog() {
        return $this->belongsTo('App\Model\Blog', 'blog_id', 'id');
    }
}
