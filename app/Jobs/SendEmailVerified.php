<?php

namespace App\Jobs;

use App\Model\Verification;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailVerified implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasTimestamps;

    protected $repository;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = $this->data['email'];
        $name = $this->data['name'] == null ? explode("@", $this->data['email'])[0] : $this->data['name'];
        $subject = $this->data['subject'];
        $token = md5($name).md5($name);

        Verification::create([
            'user_id' => $this->data['id'],
            'token' => $token,
            'created_at' => $this->freshTimestamp(),
            'updated_at' => $this->freshTimestamp()
        ]);

        Mail::send('auth.verify',
            [
                'name' => ucfirst($name),
                'token' => $token
            ], function ($mail) use ($email, $name, $subject) {
                $mail->to($email, $name)->subject($subject);
                $mail->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
            });
    }
}
