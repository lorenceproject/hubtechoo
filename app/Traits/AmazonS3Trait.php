<?php
/**
 * Created by PhpStorm.
 * User: vuongluis
 * Date: 2/21/2020
 * Time: 9:44 PM
 */

namespace App\Traits;


use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use File;
use Intervention\Image\Facades\Image;

trait AmazonS3Trait
{

    public function saveThumbnail(Request $request, $value) {
        $info = self::getInfo($value);
        if (preg_match(REGEX_EXTENSION_IMAGE, $info['filename']) == 0)
            $ext = $info['extension'];
        else $ext = PNG;
        $key = strtolower(str_random(24)) . "." . $ext;
        Storage::disk(S3)->put(THUMBNAIL . '/' . $key, fopen($request->file($value), 'r+'), 'public');
        $request->thumbnail = 'https://hubtechoo.s3-ap-southeast-1.amazonaws.com/'.THUMBNAIL. '/' . $key;
    }

    public function saveBlog(Request $request, $value) {
        $info = self::getInfo($value);
        if (preg_match(REGEX_EXTENSION_IMAGE, $info['filename']) == 0)
            $ext = $info['extension'];
        else $ext = PNG;
        $key = strtolower(str_random(24)) . "." . $ext;
        Storage::disk(S3)->put(BLOG . '/' . $key, fopen($request->file($value), 'r+'), 'public');
        $request->image = 'https://hubtechoo.s3-ap-southeast-1.amazonaws.com/'.BLOG. '/' . $key;
    }

    public function saveCategory(Request $request, $value) {
        $info = self::getInfo($value);
        if (preg_match(REGEX_EXTENSION_IMAGE, $info['filename']) == 0)
            $ext = $info['extension'];
        else $ext = PNG;
        $key = strtolower(str_random(24)) . "." . $ext;
        Storage::disk(S3)->put(CATEGORY . '/' . $key, fopen($request->file($value), 'r+'), 'public');
        $request->image = 'https://hubtechoo.s3-ap-southeast-1.amazonaws.com/'.CATEGORY. '/' . $key;
    }

    public function saveAvatar(Request $request, $backup) {
        $imageResize = Image::make($request->file('avatar')->getRealPath())->encode('webp', 90);
        $ratio = round($imageResize->width() / $imageResize->height());
        if (1 == $ratio && $imageResize->width() >= 512) {
            $imageResize->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();
            });
            $key = strtolower(str_random(24)).'.webp';
            $absolutePath = storage_path('app\avatar\\'.$key);
            $imageResize->save($absolutePath);
            Storage::disk(S3)->put(AVATAR . '/' . $key, file_get_contents($absolutePath), 'public');

            $request->avatar = 'https://hubtechoo.s3-ap-southeast-1.amazonaws.com/'.AVATAR. '/' . $key;

            if (File::exists(storage_path('app\avatar\\'.$key))) {
                unlink(storage_path('app\avatar\\'.$key));
            }
        } else {
            $request->avatar = $backup;
        }
    }

    public function getInfo($value) {
        return pathinfo($_FILES[$value]['name']);
    }
}
?>

<?php
    define('THUMBNAIL', 'hubtechoo/thumbnail');
    define('BLOG', 'hubtechoo/blog');
    define('CATEGORY', 'hubtechoo/category');
    define('AVATAR', 'hubtechoo/avatar');
    define('REGEX_EXTENSION_IMAGE', '/^.*picture.*$/');
    define('PNG', 'png');
    define('S3', 's3');
?>
