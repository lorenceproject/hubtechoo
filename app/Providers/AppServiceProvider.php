<?php

namespace App\Providers;

use App\Repositories\Blog\BlogRepository;
use App\Repositories\Blog\EloquentBlog;
use App\Repositories\BlogSeries\BlogSeriesRepository;
use App\Repositories\BlogSeries\EloquentBlogSeries;
use App\Repositories\BlogTag\BlogTagRepository;
use App\Repositories\BlogTag\EloquentBlogTag;
use App\Repositories\BlogThumbnail\BlogThumbnailRepository;
use App\Repositories\BlogThumbnail\EloquentBlogThumbnail;
use App\Repositories\BlogView\BlogViewRepository;
use App\Repositories\BlogView\EloquentBlogView;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Category\EloquentCategory;
use App\Repositories\CategoryThumbnail\CategoryThumbnailRepository;
use App\Repositories\CategoryThumbnail\EloquentCategoryThumbnail;
use App\Repositories\Contact\ContactRepository;
use App\Repositories\Contact\EloquentContact;
use App\Repositories\Index\EloquentIndex;
use App\Repositories\Index\IndexRepository;
use App\Repositories\Permission\EloquentPermission;
use App\Repositories\Permission\PermissionRepository;

use App\Repositories\Profile\EloquentProfile;
use App\Repositories\Profile\ProfileRepository;
use App\Repositories\Project\EloquentProject;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Role\EloquentRole;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Tag\EloquentTag;
use App\Repositories\Tag\TagRepository;
use App\Repositories\Thumbnail\EloquentThumbnail;
use App\Repositories\Thumbnail\ThumbnailRepository;
use App\Repositories\User\EloquentUser;
use App\Repositories\User\UserRepository;
use App\Repositories\Verification\EloquentVerification;
use App\Repositories\Verification\VerificationRepository;
use App\Repositories\Video\EloquentVideo;
use App\Repositories\Video\VideoRepository;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;

class AppServiceProvider extends ServiceProvider
{

    use HasTimestamps;

    protected $requestGuzzle;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setLanguage(substr(request()->server('HTTP_ACCEPT_LANGUAGE'), 0, 2));
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserRepository::class, EloquentUser::class);
        $this->app->singleton(RoleRepository::class, EloquentRole::class);
        $this->app->singleton(PermissionRepository::class, EloquentPermission::class);
        $this->app->singleton(ProfileRepository::class, EloquentProfile::class);
        $this->app->singleton(ThumbnailRepository::class, EloquentThumbnail::class);
        $this->app->singleton(CategoryRepository::class, EloquentCategory::class);
        $this->app->singleton(TagRepository::class, EloquentTag::class);
        $this->app->singleton(BlogTagRepository::class, EloquentBlogTag::class);
        $this->app->singleton(BlogRepository::class, EloquentBlog::class);
        $this->app->singleton(BlogThumbnailRepository::class, EloquentBlogThumbnail::class);
        $this->app->singleton(BlogViewRepository::class, EloquentBlogView::class);
        $this->app->singleton(CategoryThumbnailRepository::class, EloquentCategoryThumbnail::class);
        $this->app->singleton(VerificationRepository::class, EloquentVerification::class);
        $this->app->singleton(BlogSeriesRepository::class, EloquentBlogSeries::class);
        $this->app->singleton(IndexRepository::class, EloquentIndex::class);
        $this->app->singleton(VideoRepository::class, EloquentVideo::class);
        $this->app->singleton(ProjectRepository::class, EloquentProject::class);
        $this->app->singleton(ContactRepository::class, EloquentContact::class);
    }

    protected function setLanguage($lang) {
        if (strcmp('vi', $lang) == 0) {
            Session::put('locale', 'vi');
            App::setLocale('vi');
        } else {
            Session::put('locale', 'en');
            App::setLocale('en');
        }
    }
}
