<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer([
            'integrate.sliders'],
            'App\Composers\HomeComposer');

        view()->composer([
            'integrate.latests'],
            'App\Composers\LatestComposer');

        view()->composer([
            'integrate.posts', 'detail'],
            'App\Composers\PostComposer');

        view()->composer([
            'integrate.articles_row_one', 'integrate.articles_row_two'],
            'App\Composers\ArticleComposer');

        view()->composer([
            'integrate.project'],
            'App\Composers\AllProjectComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
