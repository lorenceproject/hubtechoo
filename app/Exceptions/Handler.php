<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Symfony\Component\ErrorHandler\Exception\FlattenException;

class Handler extends ExceptionHandler
{

    protected $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
    ];


    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request  $request
     * @param Exception $exception
     * @return JsonResponse|RedirectResponse
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof UnauthorizedHttpException) {
            if ($exception->getPrevious() == null) {
                return response()->json(
                    [
                        'error' => [
                            'code' => $exception->getStatusCode(),
                            'message' => 'Token is not provided'
                        ],
                        'data' => null,
                    ],
                    $exception->getStatusCode()
                );
            } else {
                switch (get_class($exception->getPrevious())) {
                    case TokenExpiredException::class:
                        return response()->json(
                            [
                                'error' => [
                                    'code' => $exception->getStatusCode(),
                                    'message' => 'Token has expired'
                                ],
                                'data' => null,
                            ],
                            $exception->getStatusCode()
                        );
                    case TokenInvalidException::class:
                    case TokenBlacklistedException::class:
                        return response()->json(
                            [
                                'error' => [
                                    'code' => $exception->getStatusCode(),
                                    'message' => 'Token is invalid'
                                ],
                                'data' => null,
                            ],
                            $exception->getStatusCode()
                        );
                    default:
                        return response()->json(
                            [
                                'error' => [
                                    'code' => $exception->getStatusCode(),
                                    'message' => 'Token is not provided'
                                ],
                                'data' => null,
                            ],
                            $exception->getStatusCode()
                        );
                }
            }
        } else if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json(
                [
                    'error' => [
                        'code' => $exception->getStatusCode(),
                        'message' => 'Token has expired'
                    ],
                    'data' => null,
                ],
                $exception->getStatusCode()
            );

        } else if ($exception instanceof NotFoundHttpException) {
            return redirect()->to('/admin/authenticate/login');
        } else if (FlattenException::create($exception)->getStatusCode() == 500) {
            return redirect()->to('/admin/authenticate/login');
        }
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        $token = session()->get('token');
        switch (!isset($token)) {
            default:
                $login = 'login';
                break;
        }
        return redirect()->guest(route($login));
    }
}
