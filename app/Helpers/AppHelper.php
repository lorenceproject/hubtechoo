<?php

/**
 * Created by PhpStorm.
 * User: vuongluis
 * Date: 6/7/2021
 * Time: 8:09 PM
 */
namespace App\Helpers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class AppHelper
{
    private const COLLOCATION_AUDIO = 1;

    public static function isAdminPage(): bool
    {
        return null != Auth::user() && strcmp('internal', Auth::user()->type_of_user) == 0;
    }

    public static function getLanguageRequest() {
        if (0 === strcmp("", request()->header('locale'))) {
            return Lang::locale();
        }
        return request()->header('locale');
    }

    public static function getHost()
    {
        return env('APP_URL', 'https://template.com');
    }

    public static function getCollocationAudio(): int
    {
        return self::COLLOCATION_AUDIO;
    }

}
