<?php
/**
 * Created by PhpStorm.
 * User: vuongluis
 * Date: 6/8/2021
 * Time: 12:51 PM
 */
namespace App\Helpers;

use App\Repositories\Blog\BlogRepository;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;
use Mobile_Detect;

class MetaHelper
{
    const DEFAULT_THUMBNAIL_URL = '/images/leearchitects.png';
    const APP_ID = 655262891823850;
    const THEME_COLOR = '#FFFFFF';
    const TWITTER_APP_NAME_IPHONE = 'leearchitects';
    const TWITTER_APP_ID_IPHONE = '';
    const TWITTER_SITE = '@leearchitects';
    const TWITTER_APP_URL_IPHONE = '';
    const APP_STORE_ID = '';
    const ANDROID_PACKAGE = 'com.leearchitects.reader';
    const TWITTER_TITLE = '';
    const TWITTER_DESCRIPTION = '';
    const TWITTER_IMAGE_SRC = self::DEFAULT_THUMBNAIL_URL;
    const AL_ADNROID_URL = '';
    const AL_IOS_URL = '';
    const TWITTER_APP_NAME_ANDROID = '';
    const AL_WEB_URL = '';
    const ARTICLE_PUBLISHED_TIME = '';
    const ARTICLE_AUTHOR = '';
    const OG_IMAGE_SECURE = self::DEFAULT_THUMBNAIL_URL;
    const OG_SITE_NAME = '';
    const AUTHOR = '';
    const PARSELY_POST_ID = '';
    const REFERRER = 'unsafe-url';
    const ROBOTS = 'index,follow,max-image-preview:large';
    const CANONICAL = '';
    const KEYWORDS = '';
    const TYPE = 'website';
    const ITEMPROP_NAME = '';
    const ITEMPROP_DESCRIPTION = '';
    const ITEMPROP_IMAGE = self::DEFAULT_THUMBNAIL_URL;
    const GOOGLE_SITE_VERIFICATION = '7ZscPV56E_nMkTvygcPwyZpSevrHm6noKy96NVd3D7s';
    const FB_PAGES = '102516155013260';
    const ICON = '/images/favicon.ico';

    public function __construct(BlogRepository $blogRepository) {
        $this->blogRepository = $blogRepository;
    }

    public static function defaultMeta()
    {
        return [
            'title' => Lang::get('strings.home.title'),
            'description' => Lang::get('strings.home.description'),
            'thumbnail' => url(self::DEFAULT_THUMBNAIL_URL),
            'url' => AppHelper::getHost(),
            'itemprop_name' => self::ITEMPROP_NAME,
            'itemprop_description' => self::ITEMPROP_DESCRIPTION,
            'itemprop_image' => self::ITEMPROP_IMAGE,
            'type' => self::TYPE,
            'app_id' => self::APP_ID,
            'theme_color' => self::THEME_COLOR,
            'twitter_app_name_iphone' => self::TWITTER_APP_NAME_IPHONE,
            'twitter_app_id_iphone' => self::TWITTER_APP_ID_IPHONE,
            'twitter_site' => self::TWITTER_SITE,
            'twitter_app_url_iphone' => self::TWITTER_APP_URL_IPHONE,
            'app_store_id' => self::APP_STORE_ID,
            'android_package' => self::ANDROID_PACKAGE,
            'twitter_title' => self::TWITTER_TITLE,
            'twitter_description' => self::TWITTER_DESCRIPTION,
            'twitter_image_src' => self::TWITTER_IMAGE_SRC,
            'al_android_url' => self::AL_ADNROID_URL,
            'al_ios_url' => self::AL_IOS_URL,
            'twitter_app_name_android' => self::TWITTER_APP_NAME_ANDROID,
            'al_web_url' => self::AL_WEB_URL,
            'article_published_time' => self::ARTICLE_PUBLISHED_TIME,
            'article_author' => self::ARTICLE_AUTHOR,
            'og_image_secure' => self::OG_IMAGE_SECURE,
            'og_site_name' => self::OG_SITE_NAME,
            'fb_pages' => self::FB_PAGES,
            'author' => self::AUTHOR,
            'robots' => self::ROBOTS,
            'referrer' => self::REFERRER,
            'parsely_post_id' => self::PARSELY_POST_ID,
            'icon' => self::ICON,
            'canonical' => self::CANONICAL,
            'keywords' => self::KEYWORDS,
            'google_site_verification' => self::GOOGLE_SITE_VERIFICATION
        ];
    }

    public static function fetchArticleMeta()
    {

        if (!UrlMeta::isCurrentUrlNeededMeta()) {
            return self::defaultMeta();
        } else {
            return self::fetchRegularMeta();
        }
    }

    private static function fetchRegularMeta()
    {
        $currentRoutingSegments = UrlMeta::currentUrlSegments();
        $paramSegment = end($currentRoutingSegments);

        $blogRepository = \App::make(BlogRepository::class);
        $categoryRepository = \App::make(CategoryRepository::class);

        $url = url()->current();

        $data = null;
        $data['url'] = $url;
        $data['app_id'] = self::APP_ID;
        $data['itemprop_name'] = Lang::get('strings.home.title');
        $data['itemprop_description'] = Lang::get('strings.home.description');
        $data['twitter_title'] = Lang::get('strings.home.title');
        $data['twitter_description'] = Lang::get('strings.home.description');


        if (UrlMeta::isCurrentUrlRequestPages()) {
            if (strcmp(Constants::CATEGORIES, $paramSegment) == 0) {
                $data['title'] = Lang::get('strings.categories.title');
                $data['description'] = Lang::get('strings.categories.description');
                $data['thumbnail'] = URL::to('/').'/images/og_2.jpg';
                $data['itemprop_name'] = Lang::get('strings.categories.title');
                $data['itemprop_description'] = Lang::get('strings.categories.description');

                $data['twitter_title'] = Lang::get('strings.categories.title');
                $data['twitter_description'] = Lang::get('strings.categories.description');

            } else if (strcmp(Constants::AUTHORS, $paramSegment) == 0) {
                $data['title'] = Lang::get('strings.authors.title');
                $data['description'] = Lang::get('strings.authors.description');
                $data['thumbnail'] = URL::to('/').'/images/og_2.jpg';
                $data['itemprop_name'] = Lang::get('strings.authors.title');
                $data['itemprop_description'] = Lang::get('strings.authors.description');

                $data['twitter_title'] = Lang::get('strings.authors.title');
                $data['twitter_description'] = Lang::get('strings.authors.description');
            }
        } else if (UrlMeta::isCurrentUrlRequestBlogDetail()) {
            $detect = new Mobile_Detect();
            $og_image = URL::to('/').'/images/og_2.jpg';
            $twitter_image_src = URL::to('/').'/images/twitter_1.jpg';
            if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                $blog = $blogRepository->findByField('slug_vi', $paramSegment);
                if (!is_null($blog) && sizeof($blog) > 0 && !is_null($blog[0]['thumbnail']) && sizeof($blog[0]['thumbnail']) > 0) {
                    foreach($blog[0]['thumbnail'] as $thumbnail) {
                        if (($thumbnail->width == 600 || $thumbnail->height == 315) && ($detect->isMobile())) {
                            $og_image = $thumbnail->thumbnail;
                        } else if($thumbnail->width == 1200 && $thumbnail->height == 630) {
                            $og_image = $thumbnail->thumbnail;
                        } else if ($thumbnail->width == 1200 && $thumbnail->height == 800) {
                            $twitter_image_src = $thumbnail->thumbnail;
                        }
                    }
                }
                if (!is_null($blog) && sizeof($blog) > 0) {
                    $data['title'] = $blog[0]['title_vi'];
                    $data['description'] = $blog[0]['description_vi'];
                    $data['article_published_time'] = $blog[0]->created_at->format('d M Y');
                    $data['article_author'] = $blog[0]['user']->name_vi;
                    $data['author'] = $blog[0]['user']->name_vi;
                    $data['parsely_post_id'] = $blog[0]['id'];
                    $data['og_site_name'] = $blog[0]['title_vi'];
                    $data['twitter_title'] = $blog[0]['title_vi'];
                    $data['twitter_description'] = $blog[0]['description_vi'];
                }
            } else {
                $blog = $blogRepository->findByField('slug_en', $paramSegment);
                if (!is_null($blog) && sizeof($blog) > 0 && !is_null($blog[0]['thumbnail']) && sizeof($blog[0]['thumbnail']) > 0) {
                    foreach($blog[0]['thumbnail'] as $thumbnail) {
                        if (($thumbnail->width == 600 || $thumbnail->height == 315) && ($detect->isMobile())) {
                            $og_image = $thumbnail->thumbnail;
                        } else if($thumbnail->width == 1200 && $thumbnail->height == 630) {
                            $og_image = $thumbnail->thumbnail;
                        } else if ($thumbnail->width == 1200 && $thumbnail->height == 800) {
                            $twitter_image_src = $thumbnail->thumbnail;
                        }
                    }
                }
                if (!is_null($blog) && sizeof($blog) > 0) {
                    $data['title'] = $blog[0]['title_en'];
                    $data['description'] = $blog[0]['description_en'];
                    $data['article_published_time'] = $blog[0]->created_at->format('M d Y');
                    $data['article_author'] = $blog[0]['user']->name_en;
                    $data['author'] = $blog[0]['user']->name_en;
                    $data['parsely_post_id'] = $blog[0]['id'];
                    $data['og_site_name'] = $blog[0]['title_en'];
                    $data['twitter_title'] = $blog[0]['title_en'];
                    $data['twitter_description'] = $blog[0]['description_en'];
                }
            }
            $data['twitter_app_url_iphone'] = URL::to('/').'/post/'.$paramSegment;
            $data['al_android_url'] = URL::to('/').'/post/'.$paramSegment;
            $data['al_ios_url'] = URL::to('/').'/post/'.$paramSegment;
            $data['al_web_url'] = URL::to('/').'/post/'.$paramSegment;
            $data['type'] = 'article';
            $data['og_image_secure'] = $og_image;
            $data['og_url'] = URL::to('/').'/post/'.$paramSegment;
            $data['twitter_image_src'] = $twitter_image_src;
        } else if (UrlMeta::isCurrentUrlRequestCategoryDetail()) {
            $detect = new Mobile_Detect();
            $og_image = URL::to('/').'/images/og_2.jpg';
            $twitter_image_src = URL::to('/').'/images/twitter_1.jpg';
            if (strcmp('vi', AppHelper::getLanguageRequest()) == 0) {
                $category = $categoryRepository->findByField('slug_vi', $paramSegment);
                if (!is_null($category) && sizeof($category) > 0 && !is_null($category[0]['thumbnail']) && sizeof($category[0]['thumbnail']) > 0) {
                    foreach($category[0]['thumbnail'] as $thumbnail) {
                        if (($thumbnail->width == 600 || $thumbnail->height == 315) && ($detect->isMobile())) {
                            $og_image = $thumbnail->thumbnail;
                        } else if($thumbnail->width == 1200 && $thumbnail->height == 630) {
                            $og_image = $thumbnail->thumbnail;
                        } else if ($thumbnail->width == 1200 && $thumbnail->height == 800) {
                            $twitter_image_src = $thumbnail->thumbnail;
                        }
                    }
                }
                if (!is_null($category) && sizeof($category) > 0) {
                    $data['title'] = $category[0]['name_vi'];
                    $data['description'] = $category[0]['description_vi'];
                }
            } else {
                $category = $categoryRepository->findByField('slug_en', $paramSegment);
//                if (!is_null($category) && sizeof($category) > 0 && !is_null($category[0]['thumbnail']) && sizeof($category[0]['thumbnail']) > 0) {
//                    foreach($category[0]['thumbnail'] as $thumbnail) {
//                        if (($thumbnail->width == 600 || $thumbnail->height == 315) && ($detect->isMobile())) {
//                            $og_image = $thumbnail->thumbnail;
//                        } else if($thumbnail->width == 1200 && $thumbnail->height == 630) {
//                            $og_image = $thumbnail->thumbnail;
//                        } else if ($thumbnail->width == 1200 && $thumbnail->height == 800) {
//                            $twitter_image_src = $thumbnail->thumbnail;
//                        }
//                    }
//                }
                if (!is_null($category) && sizeof($category) > 0) {
                    $data['title'] = $category[0]['name_en'];
                    $data['description'] = $category[0]['description_en'];
                }
            }
        }

        return array_merge(self::defaultMeta(), $data);
    }
}
