<?php
/**
 * Created by PhpStorm.
 * User: vuongluis
 * Date: 6/8/2021
 * Time: 12:48 PM
 */

namespace App\Helpers;


class UrlMeta
{

    const OMIT_ROUTES = [
        'storage',
        'assets',
        'fonts',
        'api'
    ];

    public static function currentUrlSegments()
    {
        return request()->segments();
    }

    public static function isCurrentUrlNeededMeta()
    {
        $currentSegments = self::currentUrlSegments();

        if (
            !isset($currentSegments) ||
            $currentSegments == '' ||
            (gettype($currentSegments) === "array" && sizeof($currentSegments) === 0)
        ) {
            return false;
        } else {
            $headSegment = $currentSegments[0];
            return !in_array($headSegment, self::OMIT_ROUTES);
        }
    }

    /**
     * For categories details
     * @return bool
     */
    public static function isCurrentUrlRequestCategoryDetail()
    {
        $regularRoutes = self::getFrontCategoriesRouteDictionary();
        $currentSegments = self::currentUrlSegments();
        $mainSegment = array_slice($currentSegments, 0, 1);

        return in_array(
                $mainSegment,
                $regularRoutes
            ) && !(is_null($currentSegments[0])) && $currentSegments[0] !== '';
    }

    private static function getFrontCategoriesRouteDictionary()
    {
        return [
            'categoryMeta' => ['category'],
        ];
    }

    /**
     * For blog details
     * @return bool
     */
    public static function isCurrentUrlRequestBlogDetail()
    {
        $regularRoutes = self::getFrontBlogsRouteDictionary();
        $currentSegments = self::currentUrlSegments();
        $mainSegment = array_slice($currentSegments, 0, 1);

        return in_array(
                $mainSegment,
                $regularRoutes
            ) && !(is_null($currentSegments[0])) && $currentSegments[0] !== '';
    }

    private static function getFrontBlogsRouteDictionary()
    {
        return [
            'postMeta' => ['post'],
        ];
    }

    /** For pages:
     * /categories
     * /authors
     * /...
     */
    public static function isCurrentUrlRequestPages() {
        return in_array(self::currentUrlSegments(), self::getFrontPagesRouteDictionary());
    }

    private static function getFrontPagesRouteDictionary()
    {
        return [
            'categories' => [Constants::CATEGORIES],
            'authors' => [Constants::AUTHORS]
        ];
    }

}
