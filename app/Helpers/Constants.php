<?php
/**
 * Created by PhpStorm.
 * User: vuongluis
 * Date: 6/8/2021
 * Time: 2:15 PM
 */

namespace App\Helpers;


class Constants
{
    const CATEGORIES = 'categories';
    const AUTHORS = 'authors';

    const BLOG = 'blog';
    const CATEGORY = 'category';
    const TAG = 'tag';
    const USER = 'user';

    const LANGUAGE_VI = "vi";
    const LANGUAGE_EN = "en";
}
