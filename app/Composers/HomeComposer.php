<?php
namespace App\Composers;

use App\Repositories\Project\ProjectRepository;
use Illuminate\View\View;

/**
 * Created by PhpStorm.
 * User: vuongluis
 * Date: 3/16/2020
 * Time: 11:05 AM
 */
class HomeComposer
{

    protected $repository;

    public function __construct(ProjectRepository $repository)
    {
        $this->repository = $repository;
    }

    public function compose(View $view)
    {
        $view->with('sliders', $this->repository->findWhere([
            ['is_home','=', 1],
        ]));
    }

}
