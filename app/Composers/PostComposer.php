<?php
namespace App\Composers;

use App\Repositories\Blog\BlogRepository;
use Illuminate\View\View;

/**
 * Created by PhpStorm.
 * User: vuongluis
 * Date: 3/16/2020
 * Time: 11:05 AM
 */
class PostComposer
{

    protected $repository;

    public function __construct(BlogRepository $repository)
    {
        $this->repository = $repository;
    }

    public function compose(View $view)
    {
        $view->with('posts', $this->repository->latests(3));
    }

}
