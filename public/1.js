webpackJsonp([1],{

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-d1e83074\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/views/layouts/partials/front-office/Footer.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", { staticClass: "site-footer" }, [
      _c(
        "div",
        {
          staticClass: "footer-top",
          staticStyle: { "background-image": "url(images/pattern/pt15.png)" }
        },
        [
          _c("div", { staticClass: "container" }, [
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                {
                  staticClass:
                    "col-xl-3 col-lg-3 col-md-6 col-5 col-sm-6 footer-col-4 col-12"
                },
                [
                  _c("div", { staticClass: "widget widget_about border-0" }, [
                    _c("h5", { staticClass: "footer-title" }, [
                      _vm._v("About Us")
                    ]),
                    _vm._v(" "),
                    _c("p", { staticClass: "mm-t5" }, [
                      _vm._v(
                        "Contrary to popular belief, Lorem simply random text. It has roots in a piece of classical Latin literature."
                      )
                    ]),
                    _vm._v(" "),
                    _c("ul", { staticClass: "contact-info-bx" }, [
                      _c("li", [
                        _c("i", { staticClass: "las la-map-marker" }),
                        _c("strong", [_vm._v("Address")]),
                        _vm._v(" 20 , New York 10010 ")
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("i", { staticClass: "las la-phone-volume" }),
                        _c("strong", [_vm._v("Phone")]),
                        _vm._v(" 0800-123456")
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("i", { staticClass: "las la-envelope-open" }),
                        _c("strong", [_vm._v("Email")]),
                        _vm._v(" info@example.com")
                      ])
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "col-xl-3 col-lg-4 col-md-6 col-7 col-sm-6 footer-col-4 col-12"
                },
                [
                  _c(
                    "div",
                    { staticClass: "widget border-0 recent-posts-entry" },
                    [
                      _c("h5", { staticClass: "footer-title" }, [
                        _vm._v("Latest Post")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "widget-post-bx" }, [
                        _c("div", { staticClass: "widget-post clearfix" }, [
                          _c("div", { staticClass: "dlab-post-media" }, [
                            _c("img", {
                              attrs: {
                                src: "images/blog/recent-blog/pic1.jpg",
                                width: "200",
                                height: "143",
                                alt: ""
                              }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "dlab-post-info" }, [
                            _c("div", { staticClass: "dlab-post-header" }, [
                              _c("h6", { staticClass: "post-title" }, [
                                _c(
                                  "a",
                                  { attrs: { href: "blog-single.html" } },
                                  [
                                    _vm._v(
                                      "Helping you and your house become better."
                                    )
                                  ]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "dlab-post-meta" }, [
                              _c("ul", [
                                _c("li", { staticClass: "post-date" }, [
                                  _c("i", { staticClass: "la la-clock" }),
                                  _vm._v(" "),
                                  _c("strong", [_vm._v("01 June")]),
                                  _vm._v(" "),
                                  _c("span", [_vm._v(" 2020")])
                                ])
                              ])
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "widget-post clearfix" }, [
                          _c("div", { staticClass: "dlab-post-media" }, [
                            _c("img", {
                              attrs: {
                                src: "images/blog/recent-blog/pic2.jpg",
                                width: "200",
                                height: "160",
                                alt: ""
                              }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "dlab-post-info" }, [
                            _c("div", { staticClass: "dlab-post-header" }, [
                              _c("h6", { staticClass: "post-title" }, [
                                _c(
                                  "a",
                                  { attrs: { href: "blog-single.html" } },
                                  [_vm._v("Creating quality urban lifestyles.")]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "dlab-post-meta" }, [
                              _c("ul", [
                                _c("li", { staticClass: "post-date" }, [
                                  _c("i", { staticClass: "la la-clock" }),
                                  _vm._v(" "),
                                  _c("strong", [_vm._v("01 June")]),
                                  _vm._v(" "),
                                  _c("span", [_vm._v(" 2020")])
                                ])
                              ])
                            ])
                          ])
                        ])
                      ])
                    ]
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "col-xl-3 col-lg-2 col-md-6 col-sm-6 footer-col-4 col-12"
                },
                [
                  _c(
                    "div",
                    { staticClass: "widget widget_services border-0" },
                    [
                      _c("h5", { staticClass: "footer-title" }, [
                        _vm._v("Usefull Link")
                      ]),
                      _vm._v(" "),
                      _c("ul", { staticClass: "mm-t10" }, [
                        _c("li", [
                          _c("a", { attrs: { href: "javascript:void(0);" } }, [
                            _vm._v("About Us")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("a", { attrs: { href: "javascript:void(0);" } }, [
                            _vm._v("Blog")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("a", { attrs: { href: "javascript:void(0);" } }, [
                            _vm._v("Services")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("a", { attrs: { href: "javascript:void(0);" } }, [
                            _vm._v("Privacy Policy")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("a", { attrs: { href: "javascript:void(0);" } }, [
                            _vm._v("Projects ")
                          ])
                        ])
                      ])
                    ]
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "col-xl-3 col-lg-3 col-md-6 col-sm-6 footer-col-4 col-12"
                },
                [
                  _c("div", { staticClass: "widget" }, [
                    _c("h5", { staticClass: "footer-title" }, [
                      _vm._v("Opening Hours")
                    ]),
                    _vm._v(" "),
                    _c("ul", { staticClass: "thsn-timelist-list mm-t5" }, [
                      _c("li", [
                        _c("span", { staticClass: "thsn-timelist-li-title" }, [
                          _vm._v("Mon – Tue")
                        ]),
                        _c("span", { staticClass: "thsn-timelist-li-value" }, [
                          _vm._v("10:00 – 18:00")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("span", { staticClass: "thsn-timelist-li-title" }, [
                          _vm._v("Wed – Thur")
                        ]),
                        _c("span", { staticClass: "thsn-timelist-li-value" }, [
                          _vm._v("10:00 – 17:00")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("span", { staticClass: "thsn-timelist-li-title" }, [
                          _vm._v("Fri – Sat")
                        ]),
                        _c("span", { staticClass: "thsn-timelist-li-value" }, [
                          _vm._v("10:00 – 12:30")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("span", { staticClass: "thsn-timelist-li-title" }, [
                          _vm._v("Saturday")
                        ]),
                        _c("span", { staticClass: "thsn-timelist-li-value" }, [
                          _vm._v("10:00 – 12:30")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("span", { staticClass: "thsn-timelist-li-title" }, [
                          _vm._v("Sunday")
                        ]),
                        _c("span", { staticClass: "thsn-timelist-li-value" }, [
                          _vm._v("Closed")
                        ])
                      ])
                    ])
                  ])
                ]
              )
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "footer-bottom" }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6 col-sm-6 text-left " }, [
              _c("span", [_vm._v("Copyright © 2020 DexignZone")])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6 col-sm-6 text-right " }, [
              _c("div", { staticClass: "widget-link " }, [
                _c("ul", [
                  _c("li", [
                    _c("a", { attrs: { href: "javascript:void(0);" } }, [
                      _vm._v(" About")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "javascript:void(0);" } }, [
                      _vm._v(" Help Desk")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "javascript:void(0);" } }, [
                      _vm._v(" Privacy Policy")
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-d1e83074", module.exports)
  }
}

/***/ }),

/***/ "./resources/assets/js/views/layouts/partials/front-office/Footer.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-d1e83074\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/views/layouts/partials/front-office/Footer.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/views/layouts/partials/front-office/Footer.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d1e83074", Component.options)
  } else {
    hotAPI.reload("data-v-d1e83074", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});
//# sourceMappingURL=1.js.map