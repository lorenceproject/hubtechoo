let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .vue();

mix.js('resources/assets/js/app-front.js', 'public/js')
    .react()
    .sass('resources/assets/sass/app-front.scss', 'public/css');

if (!mix.inProduction()) {
    mix.webpackConfig({
        devtool: 'source-map'
    }).sourceMaps()
} else {
    mix.version()
}

mix.autoload({
    'jquery': ['$', 'window.jQuery', 'jQuery'],
    'moment': ['moment','window.moment'],
});
