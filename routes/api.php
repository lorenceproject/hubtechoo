<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {

    Route::post('login', [
        'as' => 'login',
        'uses' => 'AuthController@authenticate'
    ]);

    Route::post('flogin', [
        'as' => 'flogin',
        'uses' => 'AuthController@fauthenticate'
    ]);

    Route::post('register', [
        'as' => 'register',
        'uses' => 'AuthController@register'
    ]);

    Route::post('fregister', [
        'as' => 'fregister',
        'uses' => 'AuthController@fregister'
    ]);

    Route::get('logout', [
        'as' => 'logout',
        'uses' => 'AuthController@logout'
    ]);

    Route::get('flogout', [
        'as' => 'logout',
        'uses' => 'AuthController@flogout'
    ]);

    Route::get('check', [
        'as' => 'check',
        'uses' => 'AuthController@check'
    ]);

    Route::get('avatar', [
        'as' => 'avatar',
        'uses' => 'AuthController@avatar'
    ]);
});

Route::group(['middleware' => ['jwt.auth']], function() {

    Route::resource('user', 'API\V1\UserController');
    Route::resource('avatar', 'API\V1\AvatarController');
    Route::resource('profile', 'API\V1\ProfileController');
    Route::resource('blog', 'API\V1\BlogController');
    Route::resource('category', 'API\V1\CategoryController');
    Route::resource('tag', 'API\V1\TagController');
    Route::resource('blog-image', 'API\V1\BlogImageController');
    Route::resource('category-image', 'API\V1\CategoryImageController');
    Route::resource('role', 'API\V1\RoleController');
    Route::resource('permission', 'API\V1\PermissionController');
    Route::resource('project', 'API\V1\ProjectController');
    Route::resource('contact', 'API\V1\ContactController');
    Route::resource('image', 'API\V1\ImageController');

    Route::post('ximage', array(
        'as' => 'image blog',
        'uses' => 'API\V1\ImageController@blog'
    ));

    Route::post('timage', array(
        'as' => 'image series',
        'uses' => 'API\V1\ImageController@series'
    ));

    Route::post('zimage', array(
        'as' => 'image series',
        'uses' => 'API\V1\ImageController@series'
    ));

    Route::get('extension/blogs', [
        'as' => 'check',
        'uses' => 'API\V1\BlogController@videos'
    ]);

    Route::post('apermission', array(
        'as' => 'role assign to permission',
        'uses' => 'API\V1\PermissionController@roleassigntopermission'
    ));

    Route::post('auser', array(
        'as' => 'model assign to role',
        'uses' => 'API\V1\UserController@modelassigntorole'
    ));

    Route::post('buser', array(
        'as' => 'model assign to permission',
        'uses' => 'API\V1\UserController@modelassigntopermission'
    ));

    /** MODULE SEARCH */
    Route::post('search', array(
        'as' => 'search everything',
        'uses' => 'API\V1\Search\SearchController@search'
    ));

});

/** Using validateUserLogged to validate error comes from HTTPS */

