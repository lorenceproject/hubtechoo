<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/email/verify/{token}', [
    'as' => 'send-verified-email',
    'uses' => 'AuthController@verify'
]);

Route::group(['middleware'=>'language'],function ()
{

    Route::get('locale/{locale}', function ($locale) {
        Session::put('locale',$locale);
        return $_SERVER['HTTP_REFERER'];
    });

    Route::get('locales/{locale}', function ($locale) {
        Session::put('locale',$locale);
        return redirect()->back();
    });

    Route::get('/admin/{vue?}', function (Request $request) {
        if ($request->is('admin')) {
            return view('app-front');
        }
        return view('app');
    })->where('vue', '[\/\w\.-]*')->name('admin');

    Route::get('/portal-admin', function () {
        return view('app');
    })->where('vue', '[\/\w\.-]*')->name('portal-admin');

    Route::get('{path?}', 'Front\FrontController')->where('path', '[a-zA-Z0-9-/]+');

    Route::post('social/{provider}', 'AuthController@social');
});
