<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'Model' => App\Model\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_ID','655262891823850'),
        'client_secret' => env('FACEBOOK_SECRET','e7c74a8a57c2eb846e3e1660657b2faf'),
        'redirect' => env('FACEBOOK_URL','http://leearchitects.com/auth/facebook/callback'),
    ],

    'google' => [
        'client_id' => env('GOOGLE_ID', '619159056904-68hsv2eo30bef10mtas91as1fr4g3tr4.apps.googleusercontent.com'),
        'client_secret' => env('GOOGLE_SECRET', 'sv_VmMZEM9EIzq0mcEt4uDhz'),
        'redirect' => env('GOOGLE_URL', 'https://leearchitects.com/auth/google/callback'),
    ],

    'twitter' => [
        'client_id'     => env('TWITTER_ID'),
        'client_secret' => env('TWITTER_SECRET'),
        'redirect'      => env('TWITTER_URL'),
    ],

];
