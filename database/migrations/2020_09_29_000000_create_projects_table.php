<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'projects';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->tableName))
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('category_id');
                $table->unsignedBigInteger('user_id');
                $table->string('name', 255)->nullable();
                $table->string('owner', 255)->nullable();
                $table->longText('description')->nullable();
                $table->longText('title')->nullable();
                $table->string('type', 128);
                $table->tinyInteger('is_home')->nullable();
                $table->string('location', 255)->nullable();
                $table->string('status', 255)->nullable();
                $table->date('date')->nullable();
                $table->longText('design')->nullable();
                $table->longText('construction')->nullable();
                $table->longText('content')->nullable();
                $table->timestamps();

                $table->foreign('category_id')
                    ->references('id')->on('categories');

                $table->foreign('user_id')
                    ->references('id')->on('users');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //return true;
        Schema::dropIfExists('projects');
    }
}
