import React, { Component } from 'react';
import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom";
import Team from "./frontend/components/Layout/components/Content/Module/Team";
import Blog from "./frontend/components/Layout/components/Content/Module/Blog";

class RouterFront extends React.Component {
    render () {
        return (
            <BrowserRouter>
                <Routes>
                    <Route path="/team" element={<Team/>}/>
                    <Route path="/blog" element={<Blog/>}/>
                </Routes>
            </BrowserRouter>
        )
    }
}

export default RouterFront;
