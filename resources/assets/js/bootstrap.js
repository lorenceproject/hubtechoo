import VueRouter from 'vue-router'
import Vuelidate from 'vuelidate'
import Vue from 'vue';

import Ls from './backend/services/ls'

window._ = require('lodash');
window.Popper = require('popper.js').default;


import moment from 'moment';
import { Form, HasError, AlertError } from 'vform';

window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
});

import DataTable from 'laravel-vue-datatable';
Vue.use(DataTable);

import swal from 'sweetalert2';
window.swal = swal;

import infiniteScroll from 'vue-infinite-scroll';
Vue.use(infiniteScroll)

import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload);

import Permissions from './backend/mixins/Permissions.vue';
Vue.mixin(Permissions);

import VueMindmap from 'vue-mindmap'
import 'vue-mindmap/dist/vue-mindmap.css'
Vue.config.productionTip = false
Vue.use(VueMindmap)

import VueFullscreen from 'vue-fullscreen'
Vue.use(VueFullscreen)

import VueYouTubeEmbed from 'vue-youtube-embed'
Vue.use(VueYouTubeEmbed);

import SweetAlertIcons from 'vue-sweetalert-icons';
Vue.use(SweetAlertIcons);

import vMultiselectListbox from 'vue-multiselect-listbox'
Vue.component('v-multiselect-listbox', vMultiselectListbox);

const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
window.toast = toast;

Vue.filter('ellipseShortText', function (text) {
    let temp = 160;
    if (text.length > 160) {
        for (let i = 160; i < text.length; i++) {
            if ('' === text.charAt(i)) {
                temp = i;
                break;
            }
        }
    }
    if (text.length < 160) {
        return text;
    }
    return text.substr(0, temp)+ '...';
});

Vue.filter('slugText', function (text) {
    let trimText = $.trim(text);
    return global.slug(trimText);
});

Vue.filter('upText', function (text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
});

Vue.filter('typeOfUser', function (text) {
    if ('en' == Ls.get('locale')) {
        if ('internal' == text) return "Internal";
        else return "External";
    } else {
        if ('internal' == text) return "Nội Bộ";
        else return "Vãng Lai";
    }
});

Vue.filter('myDate', function (created) {
    return moment(created).lang(localStorage.getItem('locale')).format('YYYY');
});

Vue.filter('fullDate', function (created) {
    return moment(created).lang(localStorage.getItem('locale')).format('DD MMMM YYYY');
});

Vue.filter("formatNumber", function (value) {
    return require("numeral")(value).format("0,0");
});

Vue.filter('ellipseText', function (text) {
    let temp = 142;
    if (text != null && text.length > 142) {
        for (let i = 142; i < text.length; i++) {
            if ('' === text.charAt(i)) {
                temp = i;
                break;
            }
        }
    }
    return text != null ? text.substr(0, temp)+ '...' : '';
});

Vue.filter('formatSize', function (size) {
    if (size > 1024 * 1024 * 1024 * 1024) {
        return (size / 1024 / 1024 / 1024 / 1024).toFixed(2) + ' TB'
    } else if (size > 1024 * 1024 * 1024) {
        return (size / 1024 / 1024 / 1024).toFixed(2) + ' GB'
    } else if (size > 1024 * 1024) {
        return (size / 1024 / 1024).toFixed(2) + ' MB'
    } else if (size > 1024) {
        return (size / 1024).toFixed(2) + ' KB'
    }
    return size.toString() + ' B'
});

Vue.component('pagination', require('laravel-vue-pagination'));


window.Fire = new Vue();

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap');
    require('admin-lte');
    global.slug = require('vietnamese-slug');
    global.notie = require('notie');
    global.toastr = require('toastr');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

global.axios = require('axios');

global.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};

global.axios.interceptors.request.use(function (config) {
    const AUTH_TOKEN = Ls.get('auth.token');
    if (AUTH_TOKEN) {
        config.headers.common['Authorization'] = `Bearer ${AUTH_TOKEN}`
    }
    return config
}, function (error) {
    return Promise.reject(error)
});

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

Vue.use(Vuelidate);
Vue.use(VueRouter);
