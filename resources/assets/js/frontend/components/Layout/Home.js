import React, { Component } from 'react';
import Content from './components/Content/Content'
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";

class Home extends React.Component {
    render () {
        return (
            <>
                <Header/>
                <Content/>
                <Footer/>
            </>
        )
    }
}

export default Home;
