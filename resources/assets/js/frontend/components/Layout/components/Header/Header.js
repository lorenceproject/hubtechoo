import React, { Component } from 'react';
import TopBar from './TopBar/TopBar';
import NavBar from './NavBar/NavBar';

class Header extends React.Component {
    render () {
        return (
            <header className="site-header mo-left header">
                <TopBar/>
                <NavBar/>
            </header>
        )
    }
}

export default Header;