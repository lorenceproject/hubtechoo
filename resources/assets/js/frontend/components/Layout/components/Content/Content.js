import React, { Component } from 'react';
import RouterFront from "../../../../../router-front";
import Project from "./Module/Project";

class Content extends React.Component {
    render () {
        return (
            <div className="page-content bg-white">
                <Project/>
                <div className="content-block">
                    <RouterFront/>
                </div>
            </div>
        )
    }
}

export default Content;
