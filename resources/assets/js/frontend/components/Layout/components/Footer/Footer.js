import React, { Component } from 'react';
import Top from './Top/Top';
import Bottom from './Bottom/Bottom';

class Footer extends React.Component {
    render () {
        return (
            <footer className="site-footer">
                <Top/>
                <Bottom/>
            </footer>
        )
    }
}

export default Footer;