import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import Home from "../components/Layout/Home";

export default class LayoutFront extends Component {
    render() {
        return (
            <Home/>
        );
    }
}

ReactDOM.render(<LayoutFront />, document.getElementById('app'));
