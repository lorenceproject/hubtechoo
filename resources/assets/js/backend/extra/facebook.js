/**
 * Created by vuongluis on 3/28/2021.
 */
window.fbAsyncInit = function() {
    FB.init({
        appId      : '655262891823850',
        cookie     : true,
        xfbml      : true,
        version    : 'v9.0'
    });
};
(function(d, s, id){
    let js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.getRegistrations()
        .then(function(registrations) {
            for(let registration of registrations) {
                registration.unregister()
            }})
        .catch(function(err) {
            console.log('Service Worker registration failed: ', err);
        });
}