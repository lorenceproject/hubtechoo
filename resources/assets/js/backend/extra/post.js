export const externalJs = () => {
    return new Promise(() => {
        let lastScrollTop = 0;
        window.addEventListener('scroll', function() {
            if (null == document.getElementById("relativePost")) return;
            if (null == document.getElementById("shareID")) return;
            if (null == document.getElementById("table_of_content")) return;

            let st = window.pageYOffset || document.documentElement.scrollTop;

            if ((st > lastScrollTop) && (window.innerHeight + window.scrollY) >= (document.body.offsetHeight - document.getElementById("relativePost").offsetHeight)) {
                setTimeout( function() {
                    Object.assign(document.getElementById("relativePost").style, {
                        display: "block"
                    });
                    Object.assign(document.getElementById("shareID").style, {
                        display: "none"
                    });
                    Object.assign(document.getElementById("table_of_content").style, {
                        display: "none"
                    });
                }, 0);
            }
            if ((st <= lastScrollTop) && (window.innerHeight + window.scrollY) < (document.body.offsetHeight - document.getElementById("relativePost").offsetHeight)) {
                setTimeout( function() {
                    Object.assign(document.getElementById("shareID").style, {
                        display: "block"
                    });
                    Object.assign(document.getElementById("table_of_content").style, {
                        display: "block"
                    });
                }, 0);
            }
            lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
        }, false);
        document.addEventListener("DOMContentLoaded", function () {
            let e = "dmca-badge";
            let t = "refurl";
            let n = document.querySelectorAll('a.'+e);
            if (n[0].getAttribute("href").indexOf("refurl") < 0) {
                for (let r = 0; r < n.length; r++) {
                    let i = n[r];
                    i.href = i.href + (i.href.indexOf("?") === -1 ? "?" : "&") + t + "=" + document.location
                }
            }
        }, false)
    });
};
