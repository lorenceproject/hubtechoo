/**
 * I'm trying to custom For EventBus for the website
 * @type {string}
 */
const DIALOG_SIGNAL_SUCCESS = "open_dialog_success"
const DIALOG_SIGNAL_ERROR = "open_dialog_error"

export default {
    DIALOG_SIGNAL_SUCCESS: DIALOG_SIGNAL_SUCCESS,
    DIALOG_SIGNAL_ERROR: DIALOG_SIGNAL_ERROR
}
