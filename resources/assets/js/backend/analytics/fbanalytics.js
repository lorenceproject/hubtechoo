/**
 * URL: https://developers.facebook.com/docs/analytics/send_data/events
 * Observable: https://business.facebook.com/events_manager2/list/app/655262891823850/overview?business_id=204371614407350
 * */
export default {
    /** Page */
    async open_home(lang) {
        let params = {};
        params['lang'] = lang;
        params['device'] = navigator.userAgent;
        if (typeof (FB) != 'undefined' && window.fbAsyncInit.hasRun) {
            FB.AppEvents.logEvent('open_home', null, params);
        }
    },
    async open_author(name) {
        let params = {};
        params['name'] = name;
        if (typeof (FB) != 'undefined' && window.fbAsyncInit.hasRun) {
            FB.AppEvents.logEvent('open_author', null, params);
        }
    },
    async open_category(lang) {
        let params = {};
        params['lang'] = lang;
        if (typeof (FB) != 'undefined' && window.fbAsyncInit.hasRun) {
            FB.AppEvents.logEvent('open_category', null, params);
        }
    },
    async open_tag(lang) {
        let params = {};
        params['lang'] = lang;
        if (typeof (FB) != 'undefined' && window.fbAsyncInit.hasRun) {
            FB.AppEvents.logEvent('open_tag', null, params);
        }
    },
    async sign_in_facebook(token, avatar) {
        let params = {};
        params['token'] = token;
        params['avatar'] = avatar;
        if (typeof (FB) != 'undefined' && window.fbAsyncInit.hasRun) {
            FB.AppEvents.logEvent('sign_in_facebook', null, params);
        }
    },
    async open_article(id, name, description) {
        let params = {};
        params['id'] = id;
        params['name'] = name;
        params['description'] = description;
        if (typeof (FB) != 'undefined' && window.fbAsyncInit.hasRun) {
            FB.AppEvents.logEvent('open_article', null, params);
        }
    },
    async share_facebook(id, name, description) {
        let params = {};
        params['id'] = id;
        params['name'] = name;
        params['description'] = description;
        if (typeof (FB) != 'undefined' && window.fbAsyncInit.hasRun) {
            FB.AppEvents.logEvent('share_facebook', null, params);
        }
    },
    async share_linkedin(id, name, description) {
        let params = {};
        params['id'] = id;
        params['name'] = name;
        params['description'] = description;
        if (typeof (FB) != 'undefined' && window.fbAsyncInit.hasRun) {
            FB.AppEvents.logEvent('share_linkedin', null, params);
        }
    },
    async share_twitter(id, name, description) {
        let params = {};
        params['id'] = id;
        params['name'] = name;
        params['description'] = description;
        if (typeof (FB) != 'undefined' && window.fbAsyncInit.hasRun) {
            FB.AppEvents.logEvent('share_twitter', null, params);
        }
    }
}
