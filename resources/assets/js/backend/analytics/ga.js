export default {
    async trackPage(GA, page, title, href) {
        GA.page({
            page: page,
            title: title,
            location: href
        })
    },
    /** Event tracking */
    async trackEvent(GA, category, action, label, value) {
        GA.event({
            eventCategory: category,
            eventAction: action,
            eventLabel: label,
            eventValue: value
        });
    }
}
