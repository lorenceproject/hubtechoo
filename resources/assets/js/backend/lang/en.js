const messages = {
  "page" : {
	   "login" : {
			"title": "Login",
			"button": "Login",
			"placeholder" : {
				"email" : "Enter your Email",
				"password": "Enter your password"
			},
			"error" : {
				"email": {
					"required": "This field cannot be blank.",
					"format": "You have entered an invalid format."
				},
				"password": {
					"required": "This field cannot be blank.",
					"length": "Please use at least 6 characters."
				}
			}
	   }
  },
  "sidebar": {
	  "navigation" : {
		  "dashboard" : "Dashboard",
		  "management" : "Management",
		  "project" : "Project",
		  "post" : "Post",
		  "account": "Account",
		  "config" : "Setup"
	  },
	  "sub_navigation" : {
		  "permission" : "Permission",
		  "admin" : "For Admins",
		  "guest" : "For Users",
          "website": "For Websites",
		  "all": "Collection",
		  "new": "Add New",
          "category": "Category",
		  "tag" : "Tags",
		  "information": "Information",
		  "contact": "Contact"
	  }
  },
  "breadcrumb" : {
	  "dashboard" : "Dashboard",
	  "dashboard_detail" : {
		  "list" : "",
		  "label": "Dencentralization",
		  "module" : {
			  "role" : "For Role",
			  "permission" : "For Permission",
			  "permissionToRole": "Grant Role To Permission",
			  "roleToUser": "Grant User To Role",
			  "permissionToUser": "Grant User To Permission"
		  },
		  "table": {
			  "id" : "Index",
			  "name" : {
				  "role" : "Role",
				  "permission" : "Permission"
			  },
			  "guard_name": "Guard Name",
			  "date_creation": "Date Creation",
			  "manipulation" : "Manipulation"
		  }
	  },
	  "management" : {
		  "user" : "For Admins",
		  "client" : "For Users",
		  "table" : {
			  "name" : "Full Name",
			  "all" : "All",
			  "email" : "Email",
			  "type_of_user": "Account Type",
			  "language" : "Language",
			  "date" : "Date Creation",
			  "manipulation" : "Manipulation"
		  },
		  "placeholder" : {
			  "name" : "Full Name",
			  "username" : "Username",
			  "email" : "Email",
			  "password" : "Password",
			  "status" : "Status",
			  "is_editable" : "Editable"
		  }
	  },
	  "project": {
	      "website" : "Website",
	      "table" : {
			  "all": "All",
			  "index": "Index",
			  "name" : "Project Name",
			  "thumbnail": "Thumbnail",
			  "description": "Description",
			  "initial_cost": "Initial Cost",
			  "view": "View",
			  "status": "Status",
			  "is_editable": "Is Editable",
			  "date": "Date Creation",
			  "multiple_languages": "Switch Language",
			  "manipulation": "Manipulation"
		  },
		  "placeholder" : {
			   "name" : "Enter project name",
			   "thumbnail" : "Choose the fit the size 500x357 for the thumbnail of the website",
			   "initial_cost": "Enter initial cost"
		  }
	  },
	  "post" : {
		  "post": "Post",
		  "image": "Image",
		  "table" : {
				"id" : "Index",
				"name": "Article Name",
				"thumbnail": "Thumbnail",
				"language" : "Language",
				"description": "Description",
				"content": "Content",
				"status": "Status",
				"category": "Category",
				"is_editable": "Is Editable",
				"date": "Date Creation",
				"tag": "Tag / Key Word",
				"slug": "Slug(SEO)",
				"multiple_languages": "Switch Language",
				"manipulation": "Manipulation"
		  },
		  "placeholder" : {
			  "name" : "Enter project name",
			  "category": "Select the category",
			  "tag": "Search key word\/tag"
		  }
	  },
	  "category" : {
		  "category" : "Category",
		  "image": "Image",
		  "table" : {
				"id" : "Index",
				"name": "Category Name",
				"slug": "Slug(SEO)",
				"status": "Status",
				"is_editable": "Is Editable",
				"description": "Description",
				"language" : "Language",
				"level": "Category Hierarchy",
				"date" : "Date Creation",
				"thumbnail": "Thumbnail",
				"multiple_languages": "Switch Language",
				"manipulation": "Manipulation"
		  },
		  "placeholder" : {
			  "name" : "Enter category name",
		  }
	  },
	  "tag": {
		"tag" : "Tag",
		"table" : {
			"id" : "Index",
			"name" : "Tag Name",
			"description": "Description",
			"language" : "Language",
			"date" : "Date Creation",
			"status": "Status",
			"is_editable": "Is Editable",
			"slug": "Slug(SEO)",
			"multiple_languages": "Switch Language",
			"manipulation": "Manipulation"
		}
	  },
	  "thumbnail" : {
		  "table" : {
			"id": "Index",
			"thumbnail": "Image",
			"name": "Image Name",
			"size": "Size (KB)",
			"ratio": "Aspect Ratio",
			"status": "Status",
			"design": "Direct Design",
			"manipulation": "Manipulation"
		  }
      },
	  "profile": {
		  "information": "Information",
		  "contact": "Contact",
		  "field": {
			  "name": "Full Name",
			  "avatar": "Photo Picture",
			  "phone_number": "Phone Number",
			  "position": "Position",
			  "date_of_birth": "Date of Birth",
			  "gender" : "Gender",
			  "address" : "Address",
			  "description" : "Description",
			  "male": "Male",
			  "female": "Female",
			  "username": "Username",
			  "email" :"Email",
			  "status": "Status",
			  "is_editable": "Is Editable",
			  "multiple_languages": "Switch Language"
		  }
	  },
	  "contact": {
			"contact" : "Contact",
			"table" : {
				"id": "Index",
				"name": "Contact Name",
				"email": "Email",
				"messasge": "Message",
				"manipulation": "Manipulation"
			}
	  }
  },
  "control" : {
	  "previous" : "Previous",
	  "next" : "Next"
  },
  "filter" : {
	  "search" : {
		  "role" : "Enter the role name",
		  "permission" : "Enter the permission name",
		  "user" : "Enter the Admin name",
		  "client" : "Enter the User name",
		  "post" : "Enter the Post name",
		  "category": "Enter the Category name",
		  "tag": "Enter the Tag name",
		  "contact": "Enter the Contact name"
	  },
	  "sort" : {
		  "newest" : "Newest",
		  "oldest" : "Oldest"
	  },
      "status" : {
		  "view" : "View",
		  "hide" : "Hide"
	  }
  },
  "button" : {
	  "new" : "New",
	  "close" : "Close",
	  "update": "Update"
  },
  "lang" : {
	  "vi" : "Vietnamese",
	  "en" : "English"
  },
  "dialog" : {
	  "label" : {
		  "title" : "Are you sure?",
		  "text" : "You will not be able to undo this!"
	  },
	  "button" : {
		  "cancel" : "Cancel",
		  "delete" : "Delete"
	  }
  },
  "file": {
	  "type" : {
		  "image" : "Picture",
		  "folder" : "Folder"
	  },
	  "dialog": {
		  "action": "Action",
		  "upload" : "Upload",
		  "retry" : "Retry",
		  "stop": "Stop",
          "remove": "Remove"
	  },
	  "label": {
		  "or" : "or",
		  "drop_upload_file" : "Drops files anywhere to upload",
		  "choose_file" : "Choose your file"
	  },
	  "size": {
	      "920_770" : "Home Slider",
          "1304_868": "Thumbnail",
          "362_167": "Small Thumbnail",
          "1920_326": "Banner"
	  },
	  "add" : "Add Item",
      "upload" : "Execute Upload",
  },
  "noun" : {
	  "form_1" : "s",
	  "form_2": "es"
  },
  "common" : {
	  "lang_vi" : "Vietnamese",
	  "lang_en" : "English"
  }
};
export default messages
