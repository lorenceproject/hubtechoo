const messages = {
  "page" : {
	   "login" : {
			"title": "Đăng Nhập",
			"button": "Đăng Nhập",
			"placeholder" : {
				"email": "Nhập địa chỉ hộp thư",
				"password": "Nhập mật khẩu"
			},
			"error" : {
				"email": {
					"required": "Trường này không được để trống.",
					"format": "Bạn đã nhập một định dạng không hợp lệ."
				},
				"password": {
					"required": "Trường này không được để trống.",
					"length": "Vui lòng sử dụng ít nhất 6 ký tự."
				}
			}
	   }		   
  },	  
  "sidebar": {
	  "navigation" : {
		  "dashboard" : "Trang Chủ",
		  "management" : "Quản Trị",
		  "project" : "Dự Án",
		  "post" : "Bài Viết",
		  "account": "Tài Khoản",
		  "config" : "Thiết Lập"
	  },
	  "sub_navigation" : {
		  "permission" : "Cấp Quyền",
		  "admin" : "Quản Trị Viên",
		  "guest" : "Người Dùng",
	      "website": "Websites",
		  "all": "Danh Sách",
          "new": "Tạo Mới",
		  "category": "Chủ Đề",
		  "tag" : "Từ Khóa",
		  "information": "Thông Tin",
		  "contact": "Liên Hệ",
	      "logout": "Đăng Xuất"		  
	  }
  },
  "breadcrumb" : {
	  "dashboard" : "Trang Chủ",
	  "dashboard_detail" : {
		  "list" : "",
		  "label": "Phân @uyền",
		  "module" : {
			  "role" : "Vai Trò",
			  "permission" : "Quyền",
			  "permissionToRole": "Cấp Quyền Cho Vai Trò",
			  "roleToUser": "Cấp Vai Trò Cho Người Dùng",
			  "permissionToUser": "Cấp Quyền Cho Người Dùng"
		  },
		  "table": {
			  "id" : "STT",
			  "name" : {
				  "role" : "Vai Trò",
				  "permission" : "Quyền"
			  },
			  "guard_name": "Phạm Vi",
			  "date_creation": "Ngày Tạo",
			  "manipulation" : "Thao Tác"
		  }
	  },
	  "management" : {
		  "user" : "Quản Trị Viên",
		  "client" : "Người Dùng",
		  "table" : {
			  "name" : "Họ và Tên",
			  "all" : "Tất Cả",
			  "email" : "Hộp Thư",
			  "type_of_user": "Tài Khoản",
			  "language" : "Ngôn Ngữ",
			  "date" : "Ngày Tạo",
			  "manipulation" : "Thao Tác"
		  },
		  "placeholder" : {
			  "name" : "Họ và Tên",
			  "username" : "Tên Tài Khoản",
			  "email" : "Hộp Thư",
			  "password" : "Mật Khẩu",
			  "status" : "Trạng Thái",
			  "is_editable" : "Chỉnh Sửa"
		  }
	  },
	  "project": {
	      "website" : "Website",
		  "table" : {
			  "all": "Tất Cả",
			  "index": "STT",
			  "name" : "Tên Dự Án",
			  "thumbnail": "Ảnh Dự Án",
			  "description": "Mô Tả Dự Án",
			  "initial_cost": "Chi Phí Ban Đầu",
			  "view": "Lượt Xem",
			  "status": "Trạng Thái",
			  "is_editable": "Chỉnh Sửa",
			  "date": "Ngày Tạo",
			  "multiple_languages": "Chuyển Đổi Ngôn Ngữ",
			  "manipulation": "Thao Tác"
		  },
		  "placeholder" : {
			   "name" : "Nhập tên dự án",
			   "thumbnail" : "Chọn kích thước vừa khít 500x357 cho ảnh thumbnail của website",
			   "initial_cost": "Nhập chi phí ban đầu",
		
		  }
	  },
	  "post" : {
		  "post": "Bài Viết",
		  "image": "Ảnh",
		  "table" : {
				"id" : "STT",
				"name": "Tên Bài Viết",
				"thumbnail": "Ảnh Bài Viết",
				"language" : "Ngôn Ngữ",
				"description": "Mô Tả Bài Viết",
				"content": "Nội Dung Bài Viết",
				"status": "Trạng Thái",
				"category": "Chủ Đề",
				"is_editable": "Chỉnh Sửa",
				"date": "Ngày Tạo",
				"tag": "Thẻ / Từ Khóa",
				"slug": "Phần URL sau \/(SEO)",
				"multiple_languages": "Chuyển Đổi Ngôn Ngữ",
				"manipulation": "Thao Tác"
		  },
		  "placeholder" : {
			  "name" : "Nhập tiêu đề bài viết",
			  "category": "Chọn chủ đề",
			  "tag": "Nhập từ khóa\/thẻ"
		  }
	  },
	  "category" : {
		  "category" : "Chủ Đề",
		  "image": "Ảnh",
		  "table" : {
				"id" : "STT",
				"name": "Tên Danh Mục",
				"slug": "Phần URL sau \/(SEO)",
				"status": "Trạng Thái",
				"is_editable": "Chỉnh Sửa",
				"description": "Mô Tả",
				"language" : "Ngôn Ngữ",
				"level": "Phân Cấp Danh Mục",
				"date" : "Ngày Tạo",
				"thumbnail": "Ảnh Danh Mục",
				"multiple_languages": "Chuyển Đổi Ngôn Ngữ",
				"manipulation": "Thao Tác"
		  },
		  "placeholder" : {
			  "name" : "Nhập tên danh mục",
		  }
	  },
	  "tag": {
		  "tag" : "Từ Khóa",
		  "table" : {
			"id" : "STT",
			"name" : "Thẻ/Từ Khóa",
			"description": "Mô Tả",
			"language" : "Ngôn Ngữ",
			"date" : "Ngày Tạo",
			"status": "Trạng Thái",
			"is_editable": "Chỉnh Sửa",
			"slug": "Phần URL sau \/(SEO)",
			"multiple_languages": "Chuyển Đổi Ngôn Ngữ",
			"manipulation": "Thao Tác"
		  }			  
	  },
	  "thumbnail" : {
		  "table" : {
			"id": "STT",
			"thumbnail": "Hình Ảnh",
			"name": "Tên Hình Ảnh",
			"size": "Kích Thước (KB)",
			"ratio": "Tỷ lệ Khung Ảnh",
			"design": "Hướng Thiết Kế",
			"status": "Trạng Thái",
			"manipulation": "Thao Tác"
		  }			  
      },
	  "profile": {
		  "information": "Thông Tin",
		  "contact": "Liên Hệ",
		  "field": {
			  "name": "Họ và Tên",
			  "avatar": "Ảnh Đại Diện",
			  "phone_number": "Số Điện Thoại",
			  "position": "Vị trí",
			  "date_of_birth": "Sinh Nhật",
			  "gender" : "Giới Tính",
			  "address" : "Địa Chỉ",
			  "description" : "Mô Tả",
			  "male": "Nam",
			  "female": "Nữ",
			  "username": "Tên Đăng Nhập",
			  "email": "Địa Chỉ Hộp Thư",
			  "status": "Trạng Thái",
		      "is_editable": "Chỉnh Sửa",
			  "multiple_languages": "Chuyển Đổi Ngôn Ngữ"
		  }
	  },
	  "contact": {
			"contact" : "Liên Hệ",
			"table" : {
				"id": "STT",
				"name": "Tên Liên Hệ",
				"email": "Hộp Thư Điện Tử",
				"messasge": "Thông Điệp",
				"manipulation": "Thao Tác"
			}
	  }
  },
  "control" : {
	  "previous" : "Trước",
	  "next" : "Sau"
  },
  "filter" : {
	  "search" : {
		  "role" : "Nhập tên vài trò",
		  "permission" : "Nhập tên quyền",
		  "user" : "Nhập tên Quản Trị Viên",
		  "client" : "Nhập tên Người Dùng",
		  "post" : "Nhập tên bài viết",
		  "category": "Nhập chủ đề",
		  "tag": "Nhập thẻ/từ khóa",
		  "contact": "Nhập tên liên hệ"
	  },
	  "sort" : {
		  "newest" : "Mới Nhất",
		  "oldest" : "Cũ Nhất"
	  },
	  "status" : {
		  "view" : "Xem",
		  "hide" : "Ẩn"
	  }
  },
  "button" : {
	  "new" : "Tạo Mới",
	  "close" : "Đóng",
	  "update": "Cập Nhật"
  },
  "lang" : {
	  "vi" : "Tiếng Việt",
	  "en" : "Tiếng Anh"
  },
  "dialog" : {
	  "label" : {
		  "title" : "Bạn có chắc không?",
		  "text" : "Bạn sẽ không thể hoàn tác điều này!"
	  },
	  "button" : {
		  "cancel" : "Huỷ Bỏ",
		  "delete" : "Xóa"
	  }
  },
  "file": {
	  "type" : {
		  "image" : "Ảnh",
		  "folder" : "Thư Mục"
	  },
	  "dialog": {
		  "action": "Hành Động",
		  "upload" : "Tải Lên",
		  "retry" : "Thử Lại",
		  "stop": "Dừng lại",
          "remove": "Xóa Bỏ" 		  
	  },
	  "label": {
		  "or" : "hoặc là",
		  "drop_upload_file" : "Thả tệp ở bất cứ đâu để tải lên",
		  "choose_file" : "Chọn tệp tin"
	  },
	  "size": {
	      "920_770" : "Slider Trang Chủ",
          "1304_868": "Hình Ảnh",
          "362_167": "Hình Ảnh Thu Nhỏ",
          "1920_326": "Banner" 		  
	  },
	  "add" : "Thêm Tập Tin",
      "upload" : "Thực Hiện Tải Lên",	  
  },
  "noun" : {
	  "form_1" : "",
	  "form_2": ""
  },
  "common" : {
	  "lang_vi" : "Tiếng Việt",
	  "lang_en" : "Tiếng Anh"
  }
};
export default messages
