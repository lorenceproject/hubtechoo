import Vue from 'vue';
import VueRouter from 'vue-router'
import AuthService from './backend/services/auth'
import Ls from './backend/services/ls';

/*
 |--------------------------------------------------------------------------
 | Admin Views
 |--------------------------------------------------------------------------|
 */

// Layouts
import LayoutBasic from './backend/views/layouts/LayoutBasic.vue'
import LayoutLogin from './backend/views/layouts/LayoutLogin.vue'

// Auth
import Login from './backend/views/auth/Login.vue'
import Register from './backend/views/auth/Register.vue'

// Dashboard
import Basic from './backend/views/admin/dashboard/Basic.vue'

// User
import User from './backend/views/admin/management/User.vue'

// Category
import Category from './backend/views/admin/blog/Category.vue'

// Tag
import Tag from './backend/views/admin/blog/Tag.vue'

// Profile
import Profile from './backend/views/admin/profile/Profile.vue'
import Contact from './backend/views/admin/profile/Contact.vue'

// Client
import Client from './backend/views/admin/management/Client.vue'

// Blog
import Blog from './backend/views/admin/blog/Blog.vue'
import CreateBlog from './backend/views/admin/blog/BlogCreate.vue'
import EditBlog from './backend/views/admin/blog/BlogEdit.vue'
import ImageBlog from './backend/views/admin/blog/BlogImage.vue'
import ImageCategory from './backend/views/admin/blog/CategoryImage.vue'

// Module // Security
import Decentralization from './backend/views/module/security/Decentralization.vue'

// Project
import Project from './backend/views/admin/project/Project.vue'

import ProjectCreate from './backend/views/admin/project/ProjectCreate.vue'
import ProjectEdit from './backend/views/admin/project/ProjectEdit.vue'
import ProjectImage from './backend/views/admin/project/ProjectImage.vue'

// Error
import NotFoundPage from './backend/views/errors/404.vue'

window.Vue = Vue;
Vue.use(VueRouter);

const routes = [

    /*
     |--------------------------------------------------------------------------
     | Layout Routes for DEMO  Layout
     |--------------------------------------------------------------------------|
     */

    {
        path: '/portal-admin',
        component: LayoutBasic,
        redirect: 'admin/dashboard/basic'
    },
    /*
     |--------------------------------------------------------------------------
     | Admin Backend Routes
     |--------------------------------------------------------------------------|
     */
    {
        path: '/admin',
        component: LayoutBasic,
        meta: {requiresAuth: true},
        children: [
            {
                path: 'dashboard/basic',
                component: Basic,
            },
            {
                path: 'dashboard/users',
                component: User,
            },
            {
                path: 'dashboard/security',
                component: Decentralization,
            },
            {
                path: 'dashboard/profile',
                component: Profile,
            },
            {
                path: 'dashboard/contact',
                component: Contact,
            },
            {
                path: 'dashboard/clients',
                component: Client,
            },
            {
                path: 'blog',
                component: Blog,
            },
            {
                path: 'xblog/create',
                component: CreateBlog,
            },
            {
                path: 'blogs/:id',
                component: EditBlog,
            },
            {
                path: 'blog/image/:id',
                component: ImageBlog,
            },
            {
                path: 'category/image/:id',
                component: ImageCategory,
            },
            {
                path: 'yblog/category',
                component: Category,
            },
            {
                path: 'yblog/tag',
                component: Tag,
            },
            {
                path: 'project',
                component: Project,
            },
            {
                path: 'projects/:id',
                component: ProjectEdit,
            },
            {
                path: 'project/create',
                component: ProjectCreate,
                props: (route) => ({ category_id: 4 })
            },
            {
                path: 'project/image/:id',
                component: ProjectImage,
            }
        ]
    },

    /*
     |--------------------------------------------------------------------------
     | Auth & Registration Routes
     |--------------------------------------------------------------------------|
     */

    {
        path: '/admin/authenticate',
        component: LayoutLogin,
        children: [
            {
                path: 'login',
                component: Login,
                name: 'login'
            },
            {
                path: 'register',
                component: Register,
                name: 'register'
            }
        ]
    },

    {path: '*', component: NotFoundPage}
];

const router = new VueRouter({
    routes,
    mode: 'history',
    linkActiveClass: 'active'
});

router.beforeEach((to, from, next) => {
    axios.defaults.headers.common['locale'] = Ls.get('locale');
    if (to.matched.some(m => m.meta.requiresAuth)) {
        return AuthService.check().then(authenticated => {
            if (!authenticated) {
                return next({path: '/admin/authenticate/login'})
            }

            return next()
        })
    }

    if (to.path === '/admin/authenticate/login' || to.path === '/admin/authenticate/register') {
        if (Ls.get('auth.token') === null) {
            return next();
        } else {
            return AuthService.check().then(authenticated => {
                if (!authenticated) {
                    Ls.remove('auth.token');
                    return next();
                } else {
                    return next({path: '/admin/dashboard/basic'})
                }
            });
        }
    }
    return next();
});

export default router
