<?php
/**
 * Created by PhpStorm.
 * User: vuongluis
 * Date: 6/8/2021
 * Time: 1:26 PM
 */
use App\Helpers\MetaHelper;
use App\Helpers\UrlMeta;

$neededMeta = UrlMeta::isCurrentUrlNeededMeta();
$defaultMeta = MetaHelper::defaultMeta();

if ($neededMeta) {
    $hdData = MetaHelper::fetchArticleMeta();
} else {
    $hdData = $defaultMeta;
}
?>
    <meta charset="utf-8">
    <meta name="locale" content="{{ App::getLocale() }}"/>
    <title>{{ $hdData['title'] }}</title>
    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="{{ $hdData['itemprop_name'] }}">
    <meta itemprop="description" content="{{ $hdData['itemprop_description'] }}">
    <meta itemprop="image" content="{{ $hdData['itemprop_image'] }}">
    <!-- Common -->
    <meta name="title" content="{{ $hdData['title'] }}">
    <meta name="description" content="{{ $hdData['description'] }}">
    <meta name="theme-color" content="{{ $hdData['theme_color'] }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Twitter tag -->
    <meta name="twitter:app:name:iphone" content="{{ $hdData['twitter_app_name_iphone'] }}">
    <meta name="twitter:app:id:iphone" content="{{ $hdData['twitter_app_id_iphone'] }}">
    <meta name="twitter:site" content="{{ $hdData['twitter_site'] }}">
    <meta name="twitter:app:url:iphone" content="{{ $hdData['twitter_app_url_iphone'] }}">
    <meta property="twitter:title" content="{{ $hdData['twitter_title'] }}">
    <meta property="twitter:description" content="{{ $hdData['twitter_description'] }}">
    <meta name="twitter:image:src" content="{{ $hdData['twitter_image_src'] }}">
    <meta name="twitter:card" content="summary_large_image">
    <!-- Mobile tag -->
    <meta property="al:ios:app_name" content="{{ $hdData['twitter_app_name_iphone'] }}">
    <meta property="al:ios:app_store_id" content="{{ $hdData['app_store_id'] }}">
    <meta property="al:android:package" content="{{ $hdData['android_package'] }}">
    <meta property="al:android:url" content="{{ $hdData['al_android_url'] }}">
    <meta property="al:ios:url" content="{{ $hdData['al_ios_url'] }}">
    <meta property="al:android:app_name" content="{{ $hdData['twitter_app_name_android'] }}">
    <meta property="al:web:url" content="{{ $hdData['al_web_url'] }}">
    <!-- Article tag -->
    <meta property="article:published_time" content="{{ $hdData['article_published_time'] }}">
    <meta property="article:author" content="{{ $hdData['article_author'] }}">
    <!-- Facebook tag -->
    <meta property="og:image" content="{{$hdData['thumbnail']}}">
    <meta property="og:image:width" content="720" />
    <meta property="og:image:height" content="480" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:secure" content="{{ $hdData['og_image_secure'] }}">
    <meta property="og:title" content="{{ $hdData['title'] }}">
    <meta property="og:description" content="{{ $hdData['description'] }}">
    <meta property="og:url" content="{{ $hdData['url'] }}">
    <meta property="og:type" content="{{ $hdData['type'] }}">
    <meta property="fb:app_id" content="{{ $hdData['app_id'] }}">
    <meta property="og:site_name" content="{{ $hdData['og_site_name'] }}">
    <meta property="fb:pages" content="{{ $hdData['fb_pages'] }}" />
    <!-- Extension -->
    <meta name="author" content="{{ $hdData['author'] }}">
    <meta name="robots" content="{{ $hdData['robots'] }}">
    <meta name="referrer" content="{{ $hdData['referrer'] }}">
    <meta name="parsely-post-id" content="{{ $hdData['parsely_post_id'] }}">
    <link rel="icon" href="{{ asset($hdData['icon']) }}">
    <link rel="canonical" href="{{ $hdData['canonical'] }}"/>
    <meta name="keywords" content="{{ $hdData['keywords'] }}">
    <meta name="google-site-verification" content="{{ $hdData['google_site_verification'] }}" />
