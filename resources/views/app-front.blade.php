<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />
    <meta name="description" content="EduZone - Education Collage & School HTML5 Template" />
    <meta property="og:title" content="EduZone - Education Collage & School HTML5 Template" />
    <meta property="og:description" content="EduZone - Education Collage & School HTML5 Template" />
    <meta property="og:image" content="" />
    <meta name="format-detection" content="telephone=no">

    <link rel="icon" href="{{ asset('/images/favicon.ico') }}" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/images/favicon.png') }}" />

    <title>Get Your Business To Scale Up Quickly.</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('/css/app-front.css') }}" rel="stylesheet">
    <script src="{{ asset('/js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('/js/respond.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/plugins.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/style.css') }}">
    <link class="skin" rel="stylesheet" type="text/css" href="{{ asset('/css/skin/skin-1.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/templete.css') }}">
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Merriweather:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap');
    </style>

    <link rel="stylesheet" type="text/css" href="{{ asset('/plugins/revolution/revolution/css/revolution.min.css') }}">
</head>
<body id="bg">
    <div id="app" class="page-wraper"></div>
    <script src="{{ asset('/js/app-front.js') }}"></script>
    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/plugins/owl-carousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('/js/dz.carousel.js') }}"></script>
    <script src="{{ asset('/plugins/revolution/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
</body>
</html>
