
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="{{ App::getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="locale" content="{{ App::getLocale() }}"/>
  <meta name="robots" content="index, follow"/>
  <meta name="description" content="{!! trans('strings.description') !!}">
  <meta name="author" content="lorence">
  <link rel="icon" href="{{ asset('/images/favicon.ico') }}" type="image/x-icon" />
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/images/favicon.png') }}" />
  <title>{!! trans('strings.home.title') !!}</title>
  <link rel="stylesheet" href="{{ asset('/css/all.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/app.css?v='.time().'') }} }}">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">
  <transition name="fade" mode="out-in">
      <keep-alive>
          <router-view></router-view>
      </keep-alive>
  </transition>
  <loading
          :active.sync="isLoading"
          :is-full-page="indicatorOptions.isFullPage"
          :opacity="indicatorOptions.opacity"
          :color="indicatorOptions.color"
          :loader="indicatorOptions.loader">
  </loading>
</div>
<script src="{{ asset('/js/app.js?v='.time().'') }}"></script>
</body>
</html>
